<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">

    <title>MPI Construction </title>

    <?php include 'include/link.php';?>

</head>
<body>

    <?php include 'include/header.php';?>


<section class="gd_section">


        
        <!--theme1-COMMON_WRAPPER_1-Wrapper open-->
        <div class="gd_wrpr_full structurebox">
            
        <div class="gd_wrpr_inner gd_overlay gd_overflow_hidden">
            <div class="gd_wrpr_full gd_wrpr_inner gd_bg_img">
                <!-- 1366x550 manipulation -->
               <img src="images/banner/common_banner_bg_02.jpg" alt="" title="">
            </div>
        </div>
        <div class="gd_wrpr_outer">
            <div class="gd_wrpr_full primary_font  gd_txt_clr_white gd_height_full gd_element_hcenter gd_flex_column gd_justify_content_center gd_m_l_0_lg gd_p_15 gd_align_items_center gd_p_tb_100 gd_p_lr_50 gd_p_tb_50_xl gd_p_tb_35_md gd_p_tb_25_xs  gd_p_lr_15_lg black_overlay">
                <h1 class="gd_wrpr_full gd_txt_align_left gd_txt_size_60 color_secondory gd_line_height_60 gd_txt_size_50_lg gd_line_height_50_lg gd_txt_size_35_xs gd_line_height_35_xs text_amin_1 gd_txt_weight_700 gd_txt_align_center gd_p_tb_100 gd_p_tb_50_xl gd_p_tb_35_md gd_p_tb_25_xs">
                     MEET THE TEAM
                </h1>
            </div>
            <div class="gd_overlay gd_wrpr_full gd_height_full line_animation_wrpr style_04">
                <div class="line_01"></div>
                <div class="line_02"></div>
                <div class="line_03"></div>
                <div class="line_04"></div>
                <div class="line_05"></div>
                <div class="line_06"></div>
                <div class="dote_01"></div>
                <div class="circle_01" id="scene"><div class="inner" data-depth="0.5"></div></div>
                <div class="dote_02"></div>
            </div>
        </div>
    
        </div>
    </section>

    <section class="gd_section">
        <div class="gd_wrpr_outer gd_p_tb_70 gd_p_lr_50 gd_p_tb_50_xl gd_p_lr_25_xl gd_p_tb_20_lg gd_p_lr_15_lg">
            
            <!--theme1-COMMON_WRAPPER_1-Wrapper open-->
            <div class="gd_wrpr_full structurebox">
                <div class="gd_wrpr_outer gd_wrpr_full">
                        

                        <div class="gd_wrpr_6 gd_m_b_15 gd_wrpr_12_sm">
                            <div class="gd_wrpr_inner">
                                
                                <div class="gd_wrpr_full gd_border_clr_02 common_line_btm gd_p_5 gd_wrpr_inner">
                                    <div class="gd_border_5 gd_wrpr_full gd_p_15 gd_txt_align_center">
                                        
                                            
                                            <div class="gd_wrpr_full gd_p_tb_30 gd_element_hcenter gd_border_clr_03 gd_p_20_md">
                                                <img src="images/team/01.png" alt="" class="gd_border_radius_50per gd_border_10">
                                            </div>
                                            <div class="gd_para gd_txt_size_16 gd_line_height_25 gd_txt_size_14_lg gd_line_height_20_lg txt_clr_0 primary_font gd_txt_align_left gd_section_4 gd_m_b_15 gd_wrpr_full gd_m_b_15_lg gd_txt_size_13_lg gd_line_height_18_lg text_clr_0 gd_txt_weight_700 gd_txt_align_center">
                                                INDI JOHAL: Managing Director
                                            </div>
                                            <div class="gd_para gd_txt_size_16 gd_line_height_25 gd_txt_size_14_lg gd_line_height_20_lg txt_clr_0 primary_font gd_txt_align_left gd_section_4 gd_m_b_15 gd_wrpr_full gd_m_b_15_lg gd_txt_size_13_lg gd_line_height_18_lg text_clr_0 gd_txt_align_center">
                                                A British citizen, born and brought up in London, with over 25 years of experience in managing a wide range of real estate developments. Indi relocated to Oman in 2009 where he has since overseen a variety of developments with a value upwards of RO30m.  Responsible for the overall management of the company and generally driving the company forward, analysing business operations, trends, costs, revenue & financial commitments. He is focused on ensuring client satisfaction.
                                            </div>




                                    </div>
                                </div>


                            </div>
                        </div>



                        <div class="gd_wrpr_6 gd_m_b_15 gd_wrpr_12_sm">
                            <div class="gd_wrpr_inner">
                                
                                <div class="gd_wrpr_full gd_border_clr_02 common_line_btm gd_p_5 gd_wrpr_inner">
                                    <div class="gd_border_5 gd_wrpr_full gd_p_15 gd_txt_align_center">
                                        
                                            
                                            <div class="gd_wrpr_full gd_p_tb_30 gd_element_hcenter gd_border_clr_03 gd_p_20_md">
                                                <img src="images/team/02.png" alt="" class="gd_border_radius_50per gd_border_10">
                                            </div>
                                            <div class="gd_para gd_txt_size_16 gd_line_height_25 gd_txt_size_14_lg gd_line_height_20_lg txt_clr_0 primary_font gd_txt_align_left gd_section_4 gd_m_b_15 gd_wrpr_full gd_m_b_15_lg gd_txt_size_13_lg gd_line_height_18_lg text_clr_0 gd_txt_weight_700 gd_txt_align_center">
                                                ALOK SRIVASTAVA: General Manager
                                            </div>
                                            <div class="gd_para gd_txt_size_16 gd_line_height_25 gd_txt_size_14_lg gd_line_height_20_lg txt_clr_0 primary_font gd_txt_align_left gd_section_4 gd_m_b_15 gd_wrpr_full gd_m_b_15_lg gd_txt_size_13_lg gd_line_height_18_lg text_clr_0 gd_txt_align_center">
                                                Graduate Civil Engineer with around 22 years of experience in the areas of Business Development, Operation management, project planning, site and construction management, tendering, project coordination & execution. He has overseen a wide variety of jobs ranging from Residential & commercial buildings to Industrial buildings. 
                                            </div>




                                    </div>
                                </div>


                            </div>
                        </div>



                        <div class="gd_wrpr_6 gd_m_b_15 gd_wrpr_12_sm">
                            <div class="gd_wrpr_inner">
                                
                                <div class="gd_wrpr_full gd_border_clr_02 common_line_btm gd_p_5 gd_wrpr_inner">
                                    <div class="gd_border_5 gd_wrpr_full gd_p_15 gd_txt_align_center">
                                        
                                            
                                            <div class="gd_wrpr_full gd_p_tb_30 gd_element_hcenter gd_border_clr_03 gd_p_20_md">
                                                <img src="images/team/03.png" alt="" class="gd_border_radius_50per gd_border_10">
                                            </div>
                                            <div class="gd_para gd_txt_size_16 gd_line_height_25 gd_txt_size_14_lg gd_line_height_20_lg txt_clr_0 primary_font gd_txt_align_left gd_section_4 gd_m_b_15 gd_wrpr_full gd_m_b_15_lg gd_txt_size_13_lg gd_line_height_18_lg text_clr_0 gd_txt_weight_700 gd_txt_align_center">
                                                ARUN SRIPATHI: Chief Operating Officer
                                            </div>
                                            <div class="gd_para gd_txt_size_16 gd_line_height_25 gd_txt_size_14_lg gd_line_height_20_lg txt_clr_0 primary_font gd_txt_align_left gd_section_4 gd_m_b_15 gd_wrpr_full gd_m_b_15_lg gd_txt_size_13_lg gd_line_height_18_lg text_clr_0 gd_txt_align_center">
                                               Graduate Civil Engineer with Masters in Business Administration. A seasoned leader in Construction, Project and Financial Management with over 30 years of international experience. Instrumental in developing a high energy team, customer engagement and development of Omani talent.
                                            </div>




                                    </div>
                                </div>


                            </div>
                        </div>


                        <div class="gd_wrpr_6 gd_m_b_15 gd_wrpr_12_sm">
                            <div class="gd_wrpr_inner">
                                
                                <div class="gd_wrpr_full gd_border_clr_02 common_line_btm gd_p_5 gd_wrpr_inner">
                                    <div class="gd_border_5 gd_wrpr_full gd_p_15 gd_txt_align_center">
                                        
                                            
                                            <div class="gd_wrpr_full gd_p_tb_30 gd_element_hcenter gd_border_clr_03 gd_p_20_md">
                                                <img src="images/team/04.png" alt="" class="gd_border_radius_50per gd_border_10">
                                            </div>
                                            <div class="gd_para gd_txt_size_16 gd_line_height_25 gd_txt_size_14_lg gd_line_height_20_lg txt_clr_0 primary_font gd_txt_align_left gd_section_4 gd_m_b_15 gd_wrpr_full gd_m_b_15_lg gd_txt_size_13_lg gd_line_height_18_lg text_clr_0 gd_txt_weight_700 gd_txt_align_center">
                                               INSAR ALI: Chief Financial Officer
                                            </div>
                                            <div class="gd_para gd_txt_size_16 gd_line_height_25 gd_txt_size_14_lg gd_line_height_20_lg txt_clr_0 primary_font gd_txt_align_left gd_section_4 gd_m_b_15 gd_wrpr_full gd_m_b_15_lg gd_txt_size_13_lg gd_line_height_18_lg text_clr_0 gd_txt_align_center">
                                              Chartered Accountant with over ten years of experience with KPMG. He has built a reputation over the years for his transparency, honesty, integrity and overall professionalism.
                                            </div>




                                    </div>
                                </div>


                            </div>
                        </div>


                        <div class="gd_wrpr_6 gd_m_b_15 gd_wrpr_12_sm">
                            <div class="gd_wrpr_inner">
                                
                                <div class="gd_wrpr_full gd_border_clr_02 common_line_btm gd_p_5 gd_wrpr_inner">
                                    <div class="gd_border_5 gd_wrpr_full gd_p_15 gd_txt_align_center">
                                        
                                            
                                            <div class="gd_wrpr_full gd_p_tb_30 gd_element_hcenter gd_border_clr_03 gd_p_20_md">
                                                <img src="images/team/05.png" alt="" class="gd_border_radius_50per gd_border_10">
                                            </div>
                                            <div class="gd_para gd_txt_size_16 gd_line_height_25 gd_txt_size_14_lg gd_line_height_20_lg txt_clr_0 primary_font gd_txt_align_left gd_section_4 gd_m_b_15 gd_wrpr_full gd_m_b_15_lg gd_txt_size_13_lg gd_line_height_18_lg text_clr_0 gd_txt_weight_700 gd_txt_align_center">
                                               AMIR FATEMIAN: Project Director
                                            </div>
                                            <div class="gd_para gd_txt_size_16 gd_line_height_25 gd_txt_size_14_lg gd_line_height_20_lg txt_clr_0 primary_font gd_txt_align_left gd_section_4 gd_m_b_15 gd_wrpr_full gd_m_b_15_lg gd_txt_size_13_lg gd_line_height_18_lg text_clr_0 gd_txt_align_center">
                                              MSc in Civil Engineering with over 28 years of experience in international civil and industrial mega projects, with highly reputable international contractors and consulting engineers.
                                            </div>




                                    </div>
                                </div>


                            </div>
                        </div>


                        <div class="gd_wrpr_6 gd_m_b_15 gd_wrpr_12_sm">
                            <div class="gd_wrpr_inner">
                                
                                <div class="gd_wrpr_full gd_border_clr_02 common_line_btm gd_p_5 gd_wrpr_inner">
                                    <div class="gd_border_5 gd_wrpr_full gd_p_15 gd_txt_align_center">
                                        
                                            
                                            <div class="gd_wrpr_full gd_p_tb_30 gd_element_hcenter gd_border_clr_03 gd_p_20_md">
                                                <img src="images/team/06.png" alt="" class="gd_border_radius_50per gd_border_10">
                                            </div>
                                            <div class="gd_para gd_txt_size_16 gd_line_height_25 gd_txt_size_14_lg gd_line_height_20_lg txt_clr_0 primary_font gd_txt_align_left gd_section_4 gd_m_b_15 gd_wrpr_full gd_m_b_15_lg gd_txt_size_13_lg gd_line_height_18_lg text_clr_0 gd_txt_weight_700 gd_txt_align_center">
                                               AVILAN DASWATTE: Commercial Manager
                                            </div>
                                            <div class="gd_para gd_txt_size_16 gd_line_height_25 gd_txt_size_14_lg gd_line_height_20_lg txt_clr_0 primary_font gd_txt_align_left gd_section_4 gd_m_b_15 gd_wrpr_full gd_m_b_15_lg gd_txt_size_13_lg gd_line_height_18_lg text_clr_0 gd_txt_align_center">
                                              Graduate Civil Engineer with a Diploma in Quantity Surveying & Building Economics and Construction Site Supervision. He has over 20 years of experience in Oman, Maldives and Sri Lanka.
                                            </div>




                                    </div>
                                </div>


                            </div>
                        </div>



                        <div class="gd_wrpr_6 gd_m_b_15 gd_wrpr_12_sm">
                            <div class="gd_wrpr_inner">
                                
                                <div class="gd_wrpr_full gd_border_clr_02 common_line_btm gd_p_5 gd_wrpr_inner">
                                    <div class="gd_border_5 gd_wrpr_full gd_p_15 gd_txt_align_center">
                                        
                                            
                                            <div class="gd_wrpr_full gd_p_tb_30 gd_element_hcenter gd_border_clr_03 gd_p_20_md">
                                                <img src="images/team/07.png" alt="" class="gd_border_radius_50per gd_border_10">
                                            </div>
                                            <div class="gd_para gd_txt_size_16 gd_line_height_25 gd_txt_size_14_lg gd_line_height_20_lg txt_clr_0 primary_font gd_txt_align_left gd_section_4 gd_m_b_15 gd_wrpr_full gd_m_b_15_lg gd_txt_size_13_lg gd_line_height_18_lg text_clr_0 gd_txt_weight_700 gd_txt_align_center">
                                              MANOJ WIJERATHNE: HSE-In-Charge
                                            </div>
                                            <div class="gd_para gd_txt_size_16 gd_line_height_25 gd_txt_size_14_lg gd_line_height_20_lg txt_clr_0 primary_font gd_txt_align_left gd_section_4 gd_m_b_15 gd_wrpr_full gd_m_b_15_lg gd_txt_size_13_lg gd_line_height_18_lg text_clr_0 gd_txt_align_center">
                                              NEBOSH, IOSH AND IASP certified. He has over 20 years of intensive experience coordinating and managing HSE in a vast variety of projects throughout Sri Lanka, Saudi Arabia, Oman and Maldives.
                                            </div>




                                    </div>
                                </div>


                            </div>
                        </div>











                </div>
            </div>
            

        </div>        
    </section>




    








    <?php /* /;?>
    
    <?php include 'include/banner_new.php'; ?>

    <?php include 'include/what.php'; ?>

    <?php include 'include/virtual_design.php';?>

    <?php include 'include/what_new.php';?>

    <?php include 'include/our_acheivement.php';?>
    
    <?php /* */;?>
    



    <?php include 'include/footer.php';?>

    <?php include 'include/script.php';?>

</body>
</html>