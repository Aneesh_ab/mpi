<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">

    <title>MPI Construction </title>

    <?php include 'include/link.php';?>

</head>
<body>

    <?php include 'include/header.php';?>

    
    <?php include 'include/banner_new.php'; ?>

    <?php include 'include/what.php'; ?>

    <?php include 'include/virtual_design.php';?>

    <?php include 'include/what_new.php';?>


    <?php include 'include/our_acheivement.php';?>

    

    <?php include 'include/footer.php';?>

    <?php include 'include/script.php';?>

</body>
</html>