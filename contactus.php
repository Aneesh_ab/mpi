<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">

    <title>MPI Construction </title>

    <?php include 'include/link.php';?>

</head>
<body>

    <?php include 'include/header.php';?>



        <section class="gd_section">
        <div class="gd_wrpr_inner gd_overlay gd_overflow_hidden">
            <div class="gd_wrpr_full gd_wrpr_inner gd_bg_img">
                <img src="images/banner/common_banner_bg_01.png" alt="" title="">
            </div>
        </div>
        <div class="gd_wrpr_outer">



            <div class="gd_wrpr_full primary_font  gd_txt_clr_white gd_height_full gd_element_hcenter gd_flex_column gd_justify_content_center gd_m_l_0_lg gd_p_15 gd_align_items_center gd_p_tb_100 gd_p_lr_50 gd_p_tb_50_xl gd_p_tb_35_md gd_p_tb_25_xs  gd_p_lr_15_lg black_overlay">
                <div class="gd_wrpr_full gd_txt_align_left gd_txt_size_60 color_secondory gd_line_height_60 gd_txt_size_50_lg gd_line_height_50_lg gd_txt_size_35_xs gd_line_height_35_xs text_amin_1 gd_txt_weight_700 gd_txt_align_center gd_p_tb_100 gd_p_tb_50_xl gd_p_tb_35_md gd_p_tb_25_xs">
                     CONTACT US
                </div>
            </div>




            <div class="gd_overlay gd_wrpr_full gd_height_full line_animation_wrpr style_04">
                <div class="line_01"></div>
                <div class="line_02"></div>
                <div class="line_03"></div>
                <div class="line_04"></div>
                <div class="line_05"></div>
                <div class="line_06"></div>
                <div class="dote_01"></div>
                <div class="circle_01" id="scene"><div class="inner" data-depth="0.5"></div></div>
                <div class="dote_02"></div>
            </div>
        </div>
    </section>




        <section class="gd_section">
            <div class="gd_wrpr_outer coustom_zindex_01">





                <div class="gd_wrpr_7 gd_wrpr_8_lg  gd_wrpr_12_md">
                    <div class="gd_wrpr_inner gd_wrpr_full ">
                        

                        <div class="gd_wrpr_full structurebox">
                            <div class="gd_wrpr_outer gd_wrpr_full">


                        <div class="gd_wrpr_6 gd_p_r_5 gd_p_r_0_xs gd_wrpr_12_xs">
                            <div class="gd_wrpr_inner gd_p_lr_40 gd_p_tb_60 gd_p_lr_15_lg shadow_style_03  gd_p_lr_25_xl gd_p_tb_30_xl gd_flex_column gd_p_tb_15_md gd_border_r_1_xs border_clr_gray_01 gd_flex_column gd_p_tb_40_xs gd_p_lr_25_xs gd_border_b_1_xs">
                                <div class="gd_wrpr_full gd_txt_size_40 gd_line_height_40 clr_red gd_txt_align_center gd_m_b_30 gd_line_height_30_lg gd_txt_size_30_lg">
                                    <span class="gd_icon_location"></span>
                                </div>
                                <div class="gd_wrpr_full gd_txt_size_18 gd_line_height_18 primary_font gd_txt_weight_700 txt_clr_gray gd_txt_align_center gd_m_b_30 gd_txt_size_14_xl gd_line_height_14_xl gd_m_b_20_xl gd_m_b_15_lg gd_m_b_10_xs gd_txt_size_13_xs gd_line_height_13_xs">
                                    ADDRESS
                                </div>
                                <div class="gd_wrpr_full gd_txt_size_14 gd_line_height_21 primary_font gd_txt_weight_700 txt_clr_gray_02 gd_txt_align_center gd_p_lr_30 gd_txt_size_13_lg gd_line_height_18_lg gd_p_lr_30_lg gd_p_lr_0_xs gd_txt_size_12_xs gd_line_height_16_xs gd_txt_weight_400_xs">
                                    7th Floor, Super Plaza Building,
                                    340, Way 4805, Azaiba,
                                    Muscat, Sultanate of Oman
                                </div>
                            </div>
                        </div>
                        <div class="gd_wrpr_6 gd_p_r_5 gd_p_r_0_xs gd_p_r_0_md gd_wrpr_12_xs">
                            <div class="gd_wrpr_inner gd_p_lr_40 gd_p_tb_60 gd_p_lr_15_lg shadow_style_03 gd_p_lr_25_xl gd_p_tb_30_xl gd_flex_column gd_p_tb_15_md gd_flex_column gd_p_tb_40_xs gd_p_lr_25_xs">
                                <div class="gd_wrpr_full gd_txt_size_40 gd_line_height_40 clr_red gd_txt_align_center gd_m_b_30 gd_line_height_30_lg gd_txt_size_30_lg">
                                    <span class="gd_icon_location"></span>
                                </div>
                                <div class="gd_wrpr_full gd_txt_size_18 gd_line_height_18 primary_font gd_txt_weight_700 txt_clr_gray gd_txt_align_center gd_m_b_30 gd_txt_size_14_xl gd_line_height_14_xl gd_m_b_20_xl gd_m_b_15_lg gd_m_b_10_xs gd_txt_size_13_xs gd_line_height_13_xs">
                                   POSTAL ADDRESS
                                </div>
                                <div class="gd_wrpr_full gd_txt_size_14 gd_line_height_21 primary_font gd_txt_weight_700 txt_clr_gray_02 gd_txt_align_center gd_p_lr_30 gd_txt_size_13_lg gd_line_height_18_lg gd_p_lr_30_lg gd_p_lr_0_xs gd_txt_size_12_xs gd_line_height_16_xs gd_txt_weight_400_xs">
                                    PO Box #120,
                                    PO Code # 102,
                                    Qurum, Muscat, Sultanate of Oman.

                                </div>
                            </div>
                        </div>
                    
                                
                            </div>
                        </div>


                    </div>
                </div>






                <div class="gd_wrpr_5 gd_wrpr_4_lg gd_wrpr_12_md shadow_style_03 gd_overflow_hidden banner_container gd_overflow_hidden">
                    
                    <div class="gd_wrpr_inner gd_wrpr_full">
                        

                        <div class="gd_wrpr_full structurebox">

                            


                    
                    <div class="gd_wrpr_full gd_wrpr_inner gd_overlay gd_bg_img">
                        <img class="banner_bg" src="images/contact/01.jpg">
                    </div>
                    <div class="gd_wrpr_inner bg_color_fade_02 gd_p_lr_50 gd_p_tb_60 gd_element_vcenter gd_p_lr_15_lg gd_p_tb_30_xl">
                        <div class="gd_wrpr_full gd_txt_size_14 gd_line_height_14 primary_font gd_txt_weight_700 gd_m_b_30 gd_m_b_20_lg gd_txt_size_13_lg gd_line_height_13_lg">
                            <div class="gd_wrpr_full gd_m_b_25 gd_txt_align_center_md">Telephone:</div>
                            <div class="gd_txt_size_14 gd_line_height_22_lg primary_font gd_flex_row txt_clr_gray_02 gd_wrpr_full gd_justify_content_center_md gd_align_items_center_md">
                                <a href="tel:+968 2412 8989" class="gd_wrpr_auto gd_display_table_md  gd_float_none_md gd_txt_align_center_md secondary_clr_hover txt_clr_gray_02">
                                    +968 2412 8989 
                                </a> 

                                <span>/</span>
                                <a href="tel:+968 2412 8833" class="gd_wrpr_auto gd_display_table_md  gd_float_none_md gd_txt_align_center_md secondary_clr_hover txt_clr_gray_02">
                                    2412 8833
                                </a>
                                <!-- <a href="tel:+91 8113 091 091" class="gl_link_fullwidth"></a>
                                <div class="gd_wrpr_auto gd_display_table_md  gd_float_none_md gd_txt_align_center_md secondary_clr_hover">
                                    +91 8113 091 091
                                </div> -->
                            </div>
                            
                        </div>
                        <div class="gd_wrpr_full gd_txt_size_14 gd_line_height_14 primary_font gd_txt_weight_700 gd_txt_size_13_lg gd_line_height_13_lg">
                            <div class="gd_wrpr_full gd_m_b_25 gd_txt_align_center_md">Email:</div>
                            <div class="gd_wrpr_full gd_txt_size_14 gd_line_height_22_lg primary_font">
                                <a href="mailto:info@mpioman.com" class="gd_wrpr_full gd_display_table_md  gd_float_none_md gd_txt_align_center_md secondary_clr_hover txt_clr_gray_02 gd_m_b_10">
                                    einfo@mpioman.com [general]
                                </a>

                                <a href="mailto:tendering@mpioman.com" class="gd_wrpr_full gd_display_table_md  gd_float_none_md gd_txt_align_center_md secondary_clr_hover txt_clr_gray_02 gd_m_b_10">
                                   tendering@mpioman.com [tendering]
                                </a>
                                <!-- <a href="mailto:enquiry@hailstonein.com" class="gl_link_fullwidth"></a>
                                <div class="gd_wrpr_auto gd_display_table_md  gd_float_none_md gd_txt_align_center_md secondary_clr_hover">
                                    enquiry@hailstonein.com
                                </div> -->
                            </div>
                        </div>
                    </div>
                


                        
                    </div>


                    </div>

                </div>
            </section>




            <section class="gd_wrpr_full">
                <div class="gd_wrpr_outer gd_wrpr_full primary_bg_clr gd_p_lr_100 gd_p_tb_25 gd_p_15_lg">
                    
                    <div class="gd_wrpr_full structurebox widget_3">
                        <div class="gd_wrpr_outer gd_wrpr_full ">

                            <div class="gd_wrpr_4 gd_wrpr_12_sm gd_m_b_20_sm">
                                <div class="gd_wrpr_iner">
                                    
                                    <div class="gd_wrpr_full gd_txt_size_16 gd_line_height_16 txt_clr_gray_03 gd_m_b_10 primary_font gd_txt_weight_700 gd_txt_size_14_lg gd_line_height_16_lg gd_txt_align_center_sm"> GPS Coordinates: </div>
                                    <div class="gd_wrpr_full gd_txt_size_24 gd_line_height_24 gd_txt_weight_700 txt_clr_gray_03 primary_font gd_txt_size_20_xl gd_line_height_20_xl gd_txt_size_18_lg gd_line_height_18_md gd_txt_align_center_sm">
                                            23°35’24.0N 58°21’35.0E
                                    </div>
                                </div>
                            </div>


                            <div class="gd_wrpr_4 gd_wrpr_12_sm gd_m_b_20_sm">
                                <div class="gd_wrpr_iner">
                                    
                                    <div class="gd_wrpr_full gd_txt_size_16 gd_line_height_16 txt_clr_gray_03 gd_m_b_10 primary_font gd_txt_weight_700 gd_txt_size_14_lg gd_line_height_16_lg gd_txt_align_center_sm">Plus Code:</div>
                                    <div class="gd_wrpr_full gd_txt_size_24 gd_line_height_24 gd_txt_weight_700 txt_clr_gray_03 primary_font gd_txt_size_20_xl gd_line_height_20_xl gd_txt_size_18_lg gd_line_height_18_md gd_txt_align_center_sm">
                                            H9R5+2V Muscat
                                    </div>
                                </div>
                            </div>


                            <div class="gd_wrpr_4 gd_wrpr_12_sm gd_m_b_20_sm">
                                <div class="gd_wrpr_iner">
                                    
                                    <div class="gd_wrpr_full gd_txt_size_16 gd_line_height_16 txt_clr_gray_03 gd_m_b_10 primary_font gd_txt_weight_700 gd_txt_size_14_lg gd_line_height_16_lg gd_txt_align_center_sm">LinkedIn:</div>
                                    <div class="gd_wrpr_full gd_txt_size_24 gd_line_height_24 gd_txt_weight_700 txt_clr_gray_03 primary_font gd_txt_size_20_xl gd_line_height_20_xl gd_txt_size_18_lg gd_line_height_18_md gd_txt_align_center_sm">
                                            <a href="http://tiny.cc/mpioman" class="gd_link gd_txt_align_center_sm gd_wrpr_full_sm" target="_blank">
                                                http://tiny.cc/mpioman
                                            </a>
                                    </div>
                                </div>

                            </div>




                            
                        </div>
                    </div>    

                </div>
            </section>






            <section class="gd_section">
                <div class="gd_wrpr_outer gd_p_lr_50 bg_clr_white_fade gd_p_tb_80 gd_p_lr_15_lg gd_p_tb_15_lg ">





                    <div class="gd_wrpr_9 gd_wrpr_12_lg gd_m_auto">


                        <div class="gd_wrpr_full structurebox widget_2">
                            <div class="gd_wrpr_full  gd_txt_size_24 gd_line_height_24 txt_clr_blue_01 forth_font gd_txt_size_22_lg gd_line_height_22_lg">
                            
                            <div class="gd_para gd_txt_size_16 gd_line_height_25 gd_txt_size_14_lg gd_line_height_20_lg txt_clr_0 primary_font gd_txt_align_left gd_section_4 gd_m_b_30 gd_wrpr_full gd_m_b_15_lg gd_txt_size_13_lg gd_line_height_18_lg text_clr_0 description_1">Have a question or an enquiry? Please fill out the form below and we will be in contact with you shortly</div>
                        </div>
                        <div class="gd_wrpr_full gd_wrpr_outer custom_floating_label">
                            <div class="gd_wrpr_6 gd_wrpr_12_xs  gd_m_b_20 gd_mb_15_lg gd_p_r_20 gd_p_r_15_lg gd_p_r_0_xs ">
                                <div class="gd_form_group ">
                                    <div class="gd_form_group gd_form_group_floating">
                                        <input type="text" class="gd_input gd_border_0 gd_txt_size_13 gd_line_height_13 gd_border_radius_5 gd_p_tb_25 gd_p_lr_15 gd_bg_clr_white" placeholder=" ">
                                        <label for="" class="gd_label gd_p_l_15 gd_txt_size_13 gd_line_height_13 primary_font txt_clr_gray">First Name</label>
                                        <label for="" class="error">Required</label>
                                    </div>
                                </div>
                            </div>
                            <div class="gd_wrpr_6 gd_wrpr_12_xs  gd_m_b_20 gd_mb_15_lg gd_p_r_20 gd_p_r_15_lg gd_p_r_0_xs ">
                                <div class="gd_form_group">
                                    <div class="gd_form_group gd_form_group_floating">
                                        <input type="text" class="gd_input gd_border_0 gd_txt_size_13 gd_line_height_13 gd_border_radius_5 gd_p_tb_25 gd_p_lr_15 gd_bg_clr_white" placeholder=" ">
                                        <label for="" class="gd_label gd_p_l_15 gd_txt_size_13 gd_line_height_13 primary_font txt_clr_gray">Last Name</label>
                                        <label for="" class="error">Required</label>
                                    </div>
                                </div>
                            </div>
                            <div class="gd_wrpr_6 gd_wrpr_12_xs  gd_m_b_20 gd_mb_15_lg gd_p_r_20 gd_p_r_15_lg gd_p_r_0_xs ">
                                <div class="gd_form_group">
                                    <div class="gd_form_group gd_form_group_floating">
                                        <input type="text" class="gd_input gd_border_0 gd_txt_size_13 gd_line_height_13 gd_border_radius_5 gd_p_tb_25 gd_p_lr_15 gd_bg_clr_white" placeholder=" ">
                                        <label for="" class="gd_label gd_p_l_15 gd_txt_size_13 gd_line_height_13 primary_font txt_clr_gray">Phone Number</label>
                                        <label for="" class="error">Required</label>
                                    </div>
                                </div>
                            </div>
                            <div class="gd_wrpr_6 gd_wrpr_12_xs  gd_m_b_20 gd_mb_15_lg gd_p_r_20 gd_p_r_15_lg gd_p_r_0_xs ">
                                <div class="gd_form_group">
                                    <div class="gd_form_group gd_form_group_floating">
                                        <input type="text" class="gd_input gd_border_0 gd_txt_size_13 gd_line_height_13 gd_border_radius_5 gd_p_tb_25 gd_p_lr_15 gd_bg_clr_white" placeholder=" ">
                                        <label for="" class="gd_label gd_p_l_15 gd_txt_size_13 gd_line_height_13 primary_font txt_clr_gray">Email</label>
                                        <label for="" class="error">Required</label>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="gd_wrpr_12  gd_m_b_20 gd_mb_15_lg gd_p_r_20 gd_p_r_15_lg gd_p_r_0_xs ">
                                    <div class="gd_form_group gd_form_group_floating gd_wrpr_inner">
                                        <textarea type="text" class="gd_input gd_height_full gd_border_0 gd_txt_size_13 gd_line_height_13 gd_border_radius_5 gd_p_tb_25 gd_p_lr_15 gd_wrpr_inner gd_bg_clr_white" placeholder=" "></textarea>
                                        <label for="" class="gd_label gd_p_l_15 gd_txt_size_13 gd_line_height_13 primary_font txt_clr_gray gd_align_items_start">Message</label>
                                        <label for="" class="error">Required</label>
                                </div>
                            </div>
                            <div class="gd_wrpr_12 gd_m_b_20 gd_mb_15_lg gd_p_r_20 gd_p_r_15_lg gd_p_r_0_xs ">
                                    <div class="gd_wrpr_full">
                            <button type="submit" class="gd_btn gd_btn_flat gd_txt_size_12 gd_border_radius_5 gd_p_tb_10 bg_clr_dark_02 secondary_bg_hover gd_txt_clr_white">SUBMIT</button>
                        </div>
                            </div>

                        </div>


                            
                        </div>
                        
                        
                    </div>
                </div>
            </section>


            <section class="gd_section">
                   
                    <iframe width="100%" height="500" id="gmap_canvas" src="https://maps.google.com/maps?q=Sultan%20Qaboos%20St&t=&z=15&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" class="gd_wrpr_full"></iframe>
            </section>
                <?php include 'include/footer.php';?>
            <?php include 'include/script.php';?>
        </body>
    </html>