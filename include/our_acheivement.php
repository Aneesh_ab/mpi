

<!-- flex_wrapr_none -->

<section class="gd_section gd_wrpr_outer gd_overflow_hidden">
   
    <div class="gd_wrpr_4 gd_wrpr_12_sm" >
        <div class="gd_wrpr_full gd_wrpr_inner gd_position_absolute gd_overflow_hidden primary_font text_clr_white_light gd_bg_clr_white">
            <div class="gd_wrpr_auto gd_position_absolute gd_txt_weight_400 bg_font_text1 gd_zindex_100 gd_display_table rellax_action gd_txt_weight_700 clr_dark_fade" >
                OUR<br>
                ACHIE       
            </div>
        </div>
        <div class="gd_wrpr_inner gd_p_50 gd_flex_column gd_align_items_start gd_p_25_xl gd_p_lr_15_lg">
            
            <div class="gd_wrpr_full gd_txt_align_left gd_m_b_35 gd_txt_size_45 gd_txt_size_40_xl gd_line_height_40_xl gd_line_height_45 gd_txt_size_30_lg gd_line_height_30_lg gd_txt_size_20_md gd_line_height_20_md primary_font gd_txt_weight_700 gd_m_b_15_lg text_clr_0">
                OUR ACHIEVEMENTS
            </div>
            
            <div class="owl-carousel gd_slider gd_slider_control_hide" data-slide-nav="false" data-slide-dots="false" data-slide-autoplay="true" data-slide-autoplaytimeout="4000" data-slide-drag="false" data-slide-tdrag="false" data-slide-animateOut="fadeOut" data-slide-animateOut="fadeIn">

            <div class="item gd_wrpr_full gd_img gd_m_b_30 gd_m_b_25_xl gd_m_b_20_lg gd_m_b_15_xs box_hover_effect active">
                <img src="images/index/achivements.jpg" alt="">
            </div>
             <div class="item gd_wrpr_full gd_img gd_m_b_30 gd_m_b_25_xl gd_m_b_20_lg gd_m_b_15_xs box_hover_effect active">
                <img src="images/index/achivements1.jpg" alt="">
            </div>

            </div>

            <!-- <div class="gd_para gd_para gd_txt_size_14 gd_line_height_22 gd_txt_weight_400 gd_txt_size_13_lg gd_line_height_18_lg  gd_m_b_30 gd_m_b_20_lg gd_m_b_15_sm primary_font">
                COSIDICI NATIONAL AWARDS is made up of a global pool of highly dedicated professionals working closely together to ensure our customer’s success. People with outstanding expertise, commitment and drive to make profitable advancements, reach new heights in service and deliver real results. 
            </div> -->

            <!-- <a href="javascript:void(0)" class="gd_link primary_font gd_txt_size_11 gd_line_height_11 text_clr_0 btn_style_01 secondary_bg_clr gd_p_tb_10 gd_p_lr_20 primary_bg_hover">VIEW MORE</a> -->           

        </div>

    </div>

    <div class="gd_wrpr_8 gd_wrpr_12_sm">
        <!-- <div class="gd_wrpr_full gd_wrpr_inner gd_position_absolute gd_overflow_hidden gd_zindex_100 primary_font text_clr_7_light">
            <div class="gd_wrpr_auto gd_position_absolute gd_txt_weight_400 bg_font_text gd_display_table gd_left_100n rellax_action" data-rellax-speed="-4">
                <img src="images/index/testimonial_bg.jpg" alt="">
            </div>
        </div> -->

        <!-- primary_bg_clr_light -->

        <div class="gd_wrpr_outer gd_p_50 gd_align_items_start  gd_p_25_xl gd_p_lr_15_lg gd_bg_clr_white">
            <div class="gd_wrpr_full gd_txt_align_left gd_m_b_35 gd_txt_size_45 gd_line_height_45 gd_txt_size_40_xl gd_line_height_40_xl gd_txt_size_30_lg gd_line_height_30_lg gd_txt_size_20_md gd_line_height_20_md primary_font gd_txt_weight_700 gd_m_b_15_lg text_clr_0 ">
                TESTIMONIAL
            </div>
            <div class="gd_wrpr_12 gd_wrpr_outer gd_flex_column gd_m_b_40 gd_m_b_30_xl">
                <div class="gd_wrpr_outer gd_p_tb_35 gd_border_b_1 gd_p_tb_25_xl gd_p_tb_20_lg gd_p_tb_15_xs gd_flex_column_xs  border_clr_white_off">
                    <!-- <div class="custom_img_circle_shape gd_border_radius_50per gd_m_b_15_xs">
                        <img src="images/index/testimoanial1.png" alt="">
                    </div> -->
                    <div class="gd_wrpr gd_p_l_30 gd_p_tb_5 gd_p_l_25_xl gd_wrpr_12_xs gd_p_0_xs flex_tip">
                        <div class="gd_wrpr_full gd_para gd_txt_size_16 gd_line_height_25 gd_txt_size_14_lg gd_line_height_20_lg gd_txt_weight_400 text_clr_0 gd_m_b_25 gd_m_b_20_xl gd_m_b_15_lg gd_m_b_10_sm  primary_font">
                            Global pool of highly dedicated professionals working closely together to ensure our customer’s success. People with outstanding expertise, commitment and drive to make profitable advancements, reach new heights in service and deliver real results. 
                        </div>
                        <div class="gd_wrpr_full gd_txt_size_14 gd_line_height_22 gd_txt_weight_700 gd_txt_size_13_lg gd_line_height_20_lg text_clr_0  primary_font">
                            Catherine Grace – CEO Digitalcraft
                        </div>
                    </div>
                </div>
                <div class="gd_wrpr_outer gd_p_tb_35 gd_border_b_1 gd_p_tb_25_xl gd_p_tb_20_lg gd_p_tb_15_xs gd_flex_column_xs gd_element_center_xs border_clr_white_off">
                    <!-- <div class="custom_img_circle_shape gd_border_radius_50per gd_m_b_15_xs">
                        <img src="images/index/testimoanial2.png" alt="">
                    </div> -->
                    <div class="gd_wrpr gd_p_l_30 gd_p_tb_5 gd_p_l_25_xl gd_wrpr_12_xs gd_p_0_xs flex_tip">
                        <div class="gd_wrpr_full gd_para gd_txt_size_16 gd_line_height_25 gd_txt_size_14_lg gd_line_height_20_lg gd_txt_weight_400 text_clr_0 gd_m_b_25 gd_m_b_20_xl gd_m_b_15_lg gd_m_b_10_sm  primary_font">
                            Global pool of highly dedicated professionals working closely together to ensure our customer’s success. People with outstanding expertise, commitment and drive to make profitable advancements, reach new heights in service and deliver real results. 
                        </div>
                        <div class="gd_wrpr_full gd_txt_size_14 gd_line_height_22 gd_txt_weight_700 gd_txt_size_13_lg gd_line_height_20_lg text_clr_0  primary_font">
                            John Mathue - CEO Digitalcraft
                        </div>
                    </div>
                </div>

            </div>
            <a href="javascript:void(0)" class="gd_link primary_font gd_txt_size_12 gd_line_height_12 gd_txt_weight_700 text_clr_0 btn_style_01 gd_p_tb_10 gd_p_lr_20 ">VIEW ALL</a>
            <!-- <a href="javascript:void(0)" class="gd_link gd_wrpr_auto gd_txt_size_11 gd_txt_weight_400 gd_p_tb_10 gd_p_lr_20 bg_clr_dark_02  gd_display_table_xs gd_float_none_xs gd_m_auto_xs  secondary_bg_hover">VIEW MORE</a> -->
        </div>    
    </div>


</section>