<section class="gd_section">
	<div class="gd_wrpr_outer">
		<div class="gd_wrpr_6 gd_wrpr_12_sm ">
			<div class="gd_wrpr_inner gd_bg_clr_white gd_p_l_50 gd_p_r_100 gd_p_t_80 gd_p_b_100 txt_clr_0   gd_p_15_lg gd_flex_column gd_overflow_hidden">
				<div class="gd_wrpr_full gd_txt_align_left gd_m_b_25 gd_txt_size_50 gd_line_height_50 gd_txt_size_30_lg gd_line_height_30_lg gd_txt_size_20_md gd_line_height_20_md primary_font gd_txt_weight_700 gd_m_b_15_lg">LGS</div>
				<div class="gd_txt_size_16 gd_line_height_25 gd_txt_size_14_lg gd_line_height_20_lg txt_clr_0 primary_font gd_txt_align_left gd_section_4 gd_m_b_30 gd_wrpr_full gd_m_b_15_lg gd_txt_size_13_lg gd_line_height_18_lg text_clr_0">
					In cognizance of the demand for speed, sustainability and environmentally friendly options, MPI offers its expertise in the design, supply, manufacture and installation of Light weight galvanized structural steel systems (LGS) for Industrial, Commercial and Residential projects.
				</div>
				<div class="bg_txt_parallax_common rellax_action clr_dark_fade" data-rellax-speed="-4">LGS</div>
				<!-- <div class="gd_link primary_font gd_txt_size_11 gd_line_height_11 txt_clr_0 btn_style_01 secondary_bg_clr gd_p_tb_10 gd_p_lr_20 gd_position_absolute gd_bottom_15n gd_position_relative_lg gd_bottom_0_lg primary_bg_hover">VIEW MORE</div> -->
                <a href="javascript:void(0)" class="gd_link primary_font gd_txt_size_11 gd_line_height_11 text_clr_0 btn_style_01 secondary_bg_clr gd_p_tb_10 gd_p_lr_20 primary_bg_hover">VIEW MORE</a>
			</div>
		</div>
		<div class="gd_wrpr_6 gd_wrpr_12_sm  ">
                <div class="gd_wrpr_full gd_p_20 hd_p_15_sm gd_bg_clr_white">
                    <div class="owl-carousel gd_slider" 
                    data-slide-margin="20" 
                    data-slide-loop="true"
                    data-slide-nav="true" 
                    data-slide-dots="false" 
                    data-ext-class="gd_m_b_50">
                      <div class="item gd_wrpr_full spcial_lines_anim">
                           <img class="" src="images/what_to_do_slider/01.jpg" alt="">
                      </div>
                       <div class="item gd_wrpr_full spcial_lines_anim">
                           <img class="" src="images/what_to_do_slider/02.jpg" alt="">
                      </div>
                       <div class="item gd_wrpr_full spcial_lines_anim">
                           <img class="" src="images/what_to_do_slider/01.jpg" alt="">
                      </div>
                       <div class="item gd_wrpr_full spcial_lines_anim">
                           <img class="" src="images/what_to_do_slider/02.jpg" alt="">
                      </div>
                      
                    </div>
                    <div class="gd_slider_control_wrpr gd_slider_control_b gd_slider_control_c gd_justify_content_start btn_syle_01 gd_justify_content_center_xs">
                        <div class="gd_slider_control gd_slider_control_left gd_m_0">
                            <span class="gd_icon_arrow2_left_t"></span>
                        </div>
                        <div class="gd_slider_control gd_slider_control_right gd_m_0">
                            <span class="gd_icon_arrow2_right_t"></span>
                        </div>
                    </div>
                     
                </div>
            </div>
	</div>
</section>