<!-- banner -->

<div class="gd_section">
	<div class="gd_widget overlay_bg">
		<div class="gd_wrpr_full gd_wrpr_outer full_video">
		<!-- <video autoplay muted loop class="myVideo">
  <source src="video/08-VC.mp4" type="video/mp4">
</video> -->
		




		
		
		<div class="gd_overlay gd_widget_inner slider_style_01">
			<!-- data-slide-autoplaytimeout="5000" data-slide-autoplay="true" -->
				<div class="owl-carousel gd_slider gd_slider_control_hide" data-slide-nav="false" data-slide-dots="false" data-slide-autoplay="true" data-slide-autoplaytimeout="6000" data-slide-drag="false" data-slide-tdrag="false" data-slide-animateOut="fadeOut" data-slide-animateOut="fadeIn">
					<div class="item gd_wrpr_full gd_height_full">
						<div class="gd_overlay gd_wrpr_full background_image gd_bg_img">
							<img src="images/what_to_do_slider/rgb/01.jpg">
						</div>
						<div class="gd_wrpr_full primary_font  gd_txt_clr_white gd_height_full gd_element_hcenter gd_flex_column gd_wrpr_offset_2  gd_m_l_0_lg gd_p_15">
							<div class="gd_wrpr_full gd_txt_align_left gd_txt_size_70 color_secondory gd_line_height_70 gd_txt_size_50_lg gd_line_height_50_lg gd_txt_size_35_xs gd_line_height_35_xs gd_m_b_10_lg text_amin_1 gd_txt_weight_700">WELCOME TO MPI</div>
							<div class="gd_wrpr_full gd_txt_align_left gd_m_b_35 gd_txt_size_40 gd_line_height_40 gd_txt_weight_700 gd_txt_size_25_lg gd_line_height_25_lg text_amin_2 gd_m_b_15_xs text_color_new1">Innovation Integrity Reliability
							</div>
							<div class="gd_txt_size_14 gd_line_height_20 gd_txt_clr_white primary_font gd_txt_align_left gd_section_4 gd_m_b_15 gd_section_3_lg gd_section_2_md gd_wrpr_full_xs text_amin_3 gd_p_r_20_xs">
								Focused on delivering value at every step of the construction process 
 
							</div>
							<a href="javascript:void(0)" class="gd_link primary_font gd_txt_size_11 gd_line_height_11 text_clr_0 btn_style_01 secondary_bg_clr gd_p_tb_10 gd_p_lr_20 text_amin_4 gd_txt_size_14_xs gd_line_height_14_xs">VIEW SERVICES</a>
						</div>
						<div class="gd_overlay gd_wrpr_full gd_height_full line_animation_wrpr style_01                                                                                                                                                                                                                                                                                            ">
							<div class="line_01"></div>
							<div class="line_02"></div>
							<div class="line_03"></div>
							<div class="line_04"></div>
							<div class="line_05"></div>
							<div class="line_06"></div>
							<div class="dote_01"></div>
							<div class="circle_01" id="scene"><div class="inner" data-depth="0.5"></div></div>
							<div class="dote_02"></div>
						</div>
					</div>




					<div class="item gd_wrpr_full gd_height_full">
						<div class="gd_overlay gd_wrpr_full background_image gd_bg_img">
							<img src="images/what_to_do_slider/rgb/02.jpg">
						</div>
						<div class="gd_wrpr_full primary_font  gd_txt_clr_white gd_height_full gd_element_hcenter gd_flex_column gd_wrpr_offset_2  gd_m_l_0_lg gd_p_15">
							<div class="gd_wrpr_full gd_txt_align_left gd_txt_size_70 color_secondory gd_line_height_70 gd_txt_size_50_lg gd_line_height_50_lg gd_txt_size_35_xs gd_line_height_35_xs gd_m_b_10_lg text_amin_1 gd_txt_weight_700">WELCOME TO MPI</div>
							<div class="gd_wrpr_full gd_txt_align_left gd_m_b_35 gd_txt_size_40 gd_line_height_40 gd_txt_weight_700 gd_txt_size_25_lg gd_line_height_25_lg text_amin_2 gd_m_b_15_xs text_color_new1">Innovation Integrity Reliability
							</div>
							<div class="gd_txt_size_14 gd_line_height_20 gd_txt_clr_white primary_font gd_txt_align_left gd_section_4 gd_m_b_15 gd_section_3_lg gd_section_2_md gd_wrpr_full_xs text_amin_3 gd_p_r_20_xs">
								Focused on delivering value at every step of the construction process 
 
							</div>
							<a href="javascript:void(0)" class="gd_link primary_font gd_txt_size_11 gd_line_height_11 text_clr_0 btn_style_01 secondary_bg_clr gd_p_tb_10 gd_p_lr_20 text_amin_4 gd_txt_size_14_xs gd_line_height_14_xs">VIEW SERVICES</a>
						</div>
						<div class="gd_overlay gd_wrpr_full gd_height_full line_animation_wrpr style_02                                                                                                                                                                                                                                                                                            ">
							<div class="line_01"></div>
							<div class="line_02"></div>
							<div class="line_03"></div>
							<div class="line_04"></div>
							<div class="line_05"></div>
							<div class="line_06"></div>
							<div class="dote_01"></div>
							<div class="circle_01" id="scene"><div class="inner" data-depth="0.5"></div></div>
							<div class="dote_02"></div>
						</div>
					</div>




					<div class="item gd_wrpr_full gd_height_full">
						<div class="gd_overlay gd_wrpr_full background_image gd_bg_img">
							<img src="images/what_to_do_slider/rgb/03.jpg">
						</div>
						<div class="gd_wrpr_full primary_font  gd_txt_clr_white gd_height_full gd_element_hcenter gd_flex_column gd_wrpr_offset_2  gd_m_l_0_lg gd_p_15">
							<div class="gd_wrpr_full gd_txt_align_left gd_txt_size_70 color_secondory gd_line_height_70 gd_txt_size_50_lg gd_line_height_50_lg gd_txt_size_35_xs gd_line_height_35_xs gd_m_b_10_lg text_amin_1 gd_txt_weight_700">WELCOME TO MPI</div>
							<div class="gd_wrpr_full gd_txt_align_left gd_m_b_35 gd_txt_size_40 gd_line_height_40 gd_txt_weight_700 gd_txt_size_25_lg gd_line_height_25_lg text_amin_2 gd_m_b_15_xs text_color_new1">Innovation Integrity Reliability
							</div>
							<div class="gd_txt_size_14 gd_line_height_20 gd_txt_clr_white primary_font gd_txt_align_left gd_section_4 gd_m_b_15 gd_section_3_lg gd_section_2_md gd_wrpr_full_xs text_amin_3 gd_p_r_20_xs">
								Focused on delivering value at every step of the construction process 
 
							</div>
							<a href="javascript:void(0)" class="gd_link primary_font gd_txt_size_11 gd_line_height_11 text_clr_0 btn_style_01 secondary_bg_clr gd_p_tb_10 gd_p_lr_20 text_amin_4 gd_txt_size_14_xs gd_line_height_14_xs">VIEW SERVICES</a>
						</div>
						<div class="gd_overlay gd_wrpr_full gd_height_full line_animation_wrpr style_03                                                                                                                                                                                                                                                                                            ">
							<div class="line_01"></div>
							<div class="line_02"></div>
							<div class="line_03"></div>
							<div class="line_04"></div>
							<div class="line_05"></div>
							<div class="line_06"></div>
							<div class="dote_01"></div>
							<div class="circle_01" id="scene"><div class="inner" data-depth="0.5"></div></div>
							<div class="dote_02"></div>
						</div>
					</div>
					<div class="item gd_wrpr_full gd_height_full">
						<div class="gd_overlay gd_wrpr_full background_image gd_bg_img">
							<img src="images/what_to_do_slider/rgb/04.jpg">
						</div>
						<div class="gd_wrpr_full primary_font  gd_txt_clr_white gd_height_full gd_element_hcenter gd_flex_column gd_wrpr_offset_2  gd_m_l_0_lg gd_p_15">
							<div class="gd_wrpr_full gd_txt_align_left gd_txt_size_70 color_secondory gd_line_height_70 gd_txt_size_50_lg gd_line_height_50_lg gd_txt_size_35_xs gd_line_height_35_xs gd_m_b_10_lg text_amin_1 gd_txt_weight_700">WELCOME TO MPI</div>
							<div class="gd_wrpr_full gd_txt_align_left gd_m_b_35 gd_txt_size_40 gd_line_height_40 gd_txt_weight_700 gd_txt_size_25_lg gd_line_height_25_lg text_amin_2 gd_m_b_15_xs text_color_new1">Innovation Integrity Reliability
							</div>
							<div class="gd_txt_size_14 gd_line_height_20 gd_txt_clr_white primary_font gd_txt_align_left gd_section_4 gd_m_b_15 gd_section_3_lg gd_section_2_md gd_wrpr_full_xs text_amin_3 gd_p_r_20_xs">
								Focused on delivering value at every step of the construction process 
 
							</div>
							<a href="javascript:void(0)" class="gd_link primary_font gd_txt_size_11 gd_line_height_11 text_clr_0 btn_style_01 secondary_bg_clr gd_p_tb_10 gd_p_lr_20 text_amin_4 gd_txt_size_14_xs gd_line_height_14_xs">VIEW SERVICES</a>
						</div>
						<div class="gd_overlay gd_wrpr_full gd_height_full line_animation_wrpr style_01                                                                                                                                                                                                                                                                                            ">
							<div class="line_01"></div>
							<div class="line_02"></div>
							<div class="line_03"></div>
							<div class="line_04"></div>
							<div class="line_05"></div>
							<div class="line_06"></div>
							<div class="dote_01"></div>
							<div class="circle_01" id="scene"><div class="inner" data-depth="0.5"></div></div>
							<div class="dote_02"></div>
						</div>
					</div>
					
				</div>


				<?php include 'include/socialmedia.php'; ?>




		</div>



		</div>
	</div>
</div>



<!-- banner end -->

