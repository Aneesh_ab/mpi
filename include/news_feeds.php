<section class="gd_section">
    <div class="gd_wrpr_outer gd_p_lr_50  gd_p_tb_60 gd_p_lr_25_xl gd_p_lr_15_lg gd_p_tb_45_xl gd_p_tb_25_lg bg_clr_white_fade gd_overflow_hidden">
        <div class="gd_wrpr_full gd_wrpr_inner gd_position_absolute gd_overflow_hidden secondary_font text_clr_7_light">
        <div class="gd_wrpr_auto gd_position_absolute gd_txt_weight_400 bg_font_text gd_display_table gd_left_100n rellax_action clr_white_fade" data-rellax-speed="-4">
            NEWS FEEDS      
        </div>
        </div>
        <div class="gd_wrpr_full gd_wrpr_inner gd_element_vcenter gd_m_b_40  gd_m_b_25_lg gd_m_b_20_sm gd_m_b_15_xs">
            <div class="gd_wrpr gd_txt_size_32 gd_line_height_32 gd_txt_weight_400 gd_txt_size_30_lg gd_line_height_30_lg gd_txt_size_25_sm gd_line_height_25_sm gd_txt_size_22_xs gd_line_height_22_xs secondary_font text_clr_7">
                NEWS FEED
            </div>
            <a href="javascript:void(0)" class="gd_link gd_wrpr_auto gd_txt_size_11 gd_txt_weight_400 gd_p_tb_10 gd_p_lr_20 bg_clr_dark_02 gd_txt_clr_white gd_display_table_xs gd_float_none_xs gd_m_auto_xs  secondary_bg_hover">VIEW ALL</a>
        </div>
        <div class="gd_wrpr_full">
            <div class="gd_wrpr_outer gd_m_lr_10n">

                <div class="gd_wrpr_6 gd_p_lr_10 gd_wrpr_12_sm gd_m_tb_10_sm">
                    <div class="gd_wrpr_inner gd_boxshadow gd_flex_column gd_bg_clr_white gd_align_items_start">
                        <div class="gd_wrpr_full">
                            <img src="images/index/news2.jpg" alt="" class="gd_wrpr_full">
                        </div>
                        <div class="gd_wrpr_outer gd_p_lr_25 gd_m_r_55 gd_p_tb_35 gd_m_t_35n gd_bg_clr_white gd_flex_column gd_p_lr_15_lg gd_p_tb_25_lg gd_m_t_25n_lg gd_m_r_30_lg gd_m_r_15_xs flex_tip">
                            <div class="gd_wrpr_full gd_txt_size_16 gd_line_height_16 gd_txt_weight_500 gd_m_b_15 gd_txt_size_14_lg gd_line_height_14_lg text_clr_2 primary_font">
                                Ensure our customer’s success. People with outstanding expertise,
                            </div>
                            <div class="gd_wrpr_full gd_txt_size_13 gd_line_height_13 gd_txt_weight_400 gd_m_b_20 gd_txt_size_12_lg gd_line_height_12_lg gd_m_b_15_lg text_clr_3 primary_font">
                                05:00 - 18 February, 2020
                            </div>
                            <div class="gd_para gd_txt_size_14 gd_line_height_22 gd_txt_weight_500 gd_txt_size_13_lg gd_line_height_18_lg gd_txt_size_12_xs text_clr_7 primary_font news_para">
                                Global pool of highly dedicated professionals working closely together to ensure our customer’s success. People with outstanding expertise, commitment and drive to make profitable advancements, reach new heights in service and deliver real results. 
                            </div>                            
                        </div>
                        <div class="gd_wrpr_auto gd_element_center gd_p_lr_25 gd_p_tb_10 gd_txt_size_12 gd_line_height_12 gd_txt_clr_white gd_txt_size_11_lg gd_line_height_11_lg bg_clr_dark_02 primary_bg_hover">
                            <a href="javascript:void(0)" class="gl_link_fullwidth"></a>
                            <div class="dot_shape gd_bg_clr_white gd_m_r_5"></div>
                            <span>EXPLORE NEWS</span>
                        </div>
                    </div>
                </div>
                <div class="gd_wrpr_6 gd_p_lr_10 gd_wrpr_12_sm gd_m_tb_10_sm">
                    <div class="gd_wrpr_inner gd_boxshadow gd_flex_column gd_bg_clr_white gd_align_items_start">
                        <div class="gd_wrpr_full">
                            <img src="images/index/news1.jpg" alt="" class="gd_wrpr_full">
                        </div>
                        <div class="gd_wrpr_outer gd_p_lr_25 gd_m_r_55 gd_p_tb_35 gd_m_t_35n gd_bg_clr_white gd_flex_column gd_p_lr_15_lg gd_p_tb_25_lg gd_m_t_25n_lg gd_m_r_30_lg gd_m_r_15_xs flex_tip">
                            <div class="gd_wrpr_full gd_txt_size_16 gd_line_height_16 gd_txt_weight_500 gd_m_b_15 gd_txt_size_14_lg gd_line_height_14_lg text_clr_2 primary_font">
                                Ensure our customer’s success. People with outstanding expertise,
                            </div>
                            <div class="gd_wrpr_full gd_txt_size_13 gd_line_height_13 gd_txt_weight_400 gd_m_b_20 gd_txt_size_12_lg gd_line_height_12_lg gd_m_b_15_lg text_clr_3 primary_font">
                                05:00 - 18 February, 2020
                            </div>
                            <div class="gd_para gd_txt_size_14 gd_line_height_22 gd_txt_weight_500 gd_txt_size_13_lg gd_line_height_18_lg gd_txt_size_12_xs text_clr_7 primary_font news_para">
                                Global pool of highly dedicated professionals working closely together to ensure our customer’s success. People with outstanding expertise, commitment and drive to make profitable advancements, reach new heights in service and deliver real results. 
                            </div>                            
                        </div>
                        <div class="gd_wrpr_auto gd_element_center gd_p_lr_25 gd_p_tb_10 gd_txt_size_12 gd_line_height_12 gd_txt_clr_white gd_txt_size_11_lg gd_line_height_11_lg bg_clr_dark_02 primary_bg_hover">
                            <a href="javascript:void(0)" class="gl_link_fullwidth"></a>
                            <div class="dot_shape gd_bg_clr_white gd_m_r_5"></div>
                            <span>EXPLORE NEWS</span>
                        </div>
                    </div>
                </div>
                


                <?php /* /;?><?php /* /;?>
                <div class="gd_wrpr_6 gd_p_lr_10 gd_wrpr_12_sm gd_m_tb_10_sm">
                    <div class="gd_wrpr_inner gd_boxshadow gd_flex_column">
                        <div class="gd_wrpr_full">
                            <img src="images/index/news2.jpg" alt="" class="gd_wrpr_full">
                        </div>
                        <div class="gd_wrpr_full gd_bg_clr_white gd_display_flex gd_flex_column primary_font flex_tip">
                            <div class="gd_wrpr_outer gd_p_lr_25 gd_m_r_55 gd_p_tb_35 gd_m_t_35n gd_bg_clr_white gd_flex_column gd_p_lr_15_lg gd_p_tb_25_lg gd_m_t_25n_lg gd_m_r_30_lg gd_m_r_15_xs flex_tip">
                                <div class="gd_wrpr_full gd_txt_size_16 gd_line_height_16 gd_txt_weight_500 gd_m_b_15 gd_txt_size_14_lg gd_line_height_14_lg text_clr_2">
                                    Ensure our customer’s success. People with outstanding expertise,
                                </div>
                                <div class="gd_wrpr_full gd_txt_size_13 gd_line_height_13 gd_txt_weight_400 gd_m_b_20 gd_txt_size_12_lg gd_line_height_12_lg gd_m_b_15_lg text_clr_3">
                                    05:00 - 18 February, 2020
                                </div>
                                <div class="gd_para gd_txt_size_14 gd_line_height_22 gd_txt_weight_500 gd_txt_size_13_lg gd_line_height_18_lg text_clr_7 news_para">
                                    Global pool of highly dedicated professionals working closely together to ensure our customer’s success. People with outstanding expertise, commitment and drive to make profitable advancements, reach new heights in service and deliver real results. 
                                </div>                            
                            </div>
                            <div class="gd_wrpr_outer">
                                <div class="gd_wrpr_auto gd_element_center gd_p_lr_25 gd_p_tb_10 gd_txt_size_12 gd_line_height_12 gd_txt_clr_white gd_txt_size_11_lg gd_line_height_11_lg bg_clr_dark_02 primary_bg_hover">
                                    <a href="expos.php" class="gl_link_fullwidth"></a>
                                    <div class="dot_shape gd_bg_clr_white gd_m_r_5"></div>
                                    <span>EXPLORE NEWS</span>
                                </div>
                                <!-- <a href="expos.php" class="gd_link"><span class="dot_shape"></span>EXPLRE NEWS</a> -->
                            </div>                           
                        </div>
                    </div>
                </div>
                <?php /* /;?><?php /* */;?>


            </div>
        </div>
    </div>
</section>