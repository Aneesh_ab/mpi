<section class="gd_section gd_wrpr_outer">
    <div class="gd_wrpr_4 gd_wrpr_5_xl gd_wrpr_6_lg gd_wrpr_12_sm gd_flex_column">

        <div class="gd_wrpr_outer gd_shape_tria_b gd_border_clr_white gd_m_b_auto gd_height_full">
            
            <div class="gd_wrpr_full gd_wrpr_inner gd_position_absolute gd_overflow_hidden secondary_font text_clr_7_light dark_bg">
                <div class="gd_position_absolute gd_txt_weight_400 gd_txt_size_80 gd_line_height_90 gd_display_table gd_left_90n rellax_action width_max_content" data-rellax-speed="-2">
                    WE PROVIDE BEST<br>
                    SERVICES FOR YOU.    
                </div>
            </div>

            <div class="gd_wrpr_full gd_h_wrpr gd_txt_align_left gd_txt_size_40 gd_line_height_40 gd_txt_weight_400 gd_txt_clr_white gd_p_lr_50 gd_p_t_60 gd_p_b_15 gd_p_lr_25_xl gd_p_lr_15_lg gd_p_t_45_xl gd_p_b_10_xl gd_p_t_25_lg gd_border_b_1 gd_txt_size_35_xl gd_line_height_35_xl gd_txt_size_30_lg gd_line_height_30_lg gd_txt_size_25_sm gd_line_height_25_sm gd_txt_size_22_xs gd_line_height_22_xs border_clr_dark secondary_font">
                <div class="gd_h_main">WE PROVIDE BEST</div>
                <div class="gd_h_sub gd_letter_space_0">SERVICES FOR YOU.</div>
            </div>
            <div class="gd_wrpr_full  gd_p_lr_50 gd_p_t_15 gd_p_b_45 gd_p_lr_25_xl gd_p_lr_15_lg gd_p_t_10_xl gd_p_b_35_xl gd_p_b_25_lg">
                <div class="gd_para gd_para gd_txt_size_14 gd_line_height_22 gd_txt_weight_400 gd_txt_size_13_lg gd_line_height_20_lg gd_txt_clr_white gd_m_b_30 gd_m_b_20_lg gd_m_b_15_sm primary_font">
                    With 24x7 reliable servicing options and our uncompromising service quality of parts and people help reduce downtime of machines. Our service solutions also help the customer guarantee excellent production & also maintaining machine condition.
                </div>
                <a href="services.php" class="gd_link gd_wrpr_auto gd_txt_size_11 gd_txt_weight_400 gd_p_tb_10 gd_p_lr_20 gd_bg_clr_white text_clr_0 primary_bg_hover">VIEW MORE</a>
            </div>
            <span class="gd_shape custom_shape_position"></span>
        </div>
        <div class="gd_wrpr_outer gd_p_lr_40 gd_p_tb_35 gd_overflow_hidden gd_p_lr_20_xl gd_p_lr_15_lg hover_img_effect">
            <div class="gd_wrpr_full gd_wrpr_inner gd_position_absolute gd_left_0 gd_top_0 gd_bg_img">
                <img src="images/index/provide_call_bg.jpg" alt="" class="hover_img">
            </div>
            <div class="gd_wrpr_auto gd_h_wrpr gd_txt_size_14 gd_line_height_20 gd_txt_weight_400 gd_txt_size_13_lg gd_txt_size_12_sm text_clr_4 secondary_font">
                <a href="tel:+917356666456" class="gl_link_fullwidth"></a>
                <div class="gd_h_sub gd_line_height_20 gd_letter_space_0">CUSTOMER SUPPORT</div>
                <div class="gd_h_main gd_txt_size_24 gd_line_height_30 gd_txt_size_20_lg gd_txt_size_18_sm text_clr_6">+91 735 6666 456</div>
            </div>
        </div>

    </div>
    
    <div class="gd_wrpr_8 gd_wrpr_7_xl gd_wrpr_6_lg gd_wrpr_12_sm">
        <div class="gd_wrpr_inner">

            <div class="gd_widget gd_widget_2 gd_overflow_hidden hover_img_effect">
                <div class="gd_bg_img gd_wrpr_inner custom_height_provide_img">
                    <img src="images/index/provide_main_img.jpg" alt="" class="hover_img">
                </div>
                <div class="gd_overlay">
                    <div class="gd_wrpr_full gd_p_20 gd_p_lr_15_lg primary_font secondary_bg_clr_light2">
                        <div class="gd_wrpr_full gd_wrpr_outer gd_txt_size_20 gd_line_height_20 gd_txt_weight_400 gd_txt_align_left gd_txt_clr_white gd_m_b_15 gd_txt_size_18_lg gd_line_height_18_lg gd_m_b_10_lg secondary_font">SERVICE REQUEST</div>
                        <div class="gd_wrpr_full gd_wrpr_outer gd_element_vcenter">
                            <div class="gd_wrpr gd_para gd_txt_align_left gd_para gd_txt_size_12 gd_line_height_17 gd_txt_weight_400 gd_txt_size_11_lg gd_line_height_16_lg gd_txt_clr_white primary_font flex_tip custom_service_req_para_scroll">
                                We have been improving design on VSI for years as per our customer requirements. The customer satisfaction is our objective on product development. Our research and development activities have resulted in a well-shaped autogenous crusher. The EP series VSI
                            </div>
                            <div class="gd_wrpr_auto gd_m_lr_40 gd_p_tb_5 gd_p_lr_20 gd_bg_clr_white gd_txt_size_11 gd_line_height_11 gd_txt_size_10_lg gd_line_height_10_lg gd_m_lr_15_lg cutom_link_shape primary_bg_hover">
                                <a href="services.php" class="gl_link_fullwidth"></a>
                                GO
                            </div>

                        </div>
                    </div>
                    <!-- <a href="javascript:void(0)" class="gd_link  gd_txt_clr_white gd_p_15">
                        <div class="gd_h_wrpr">
                            <div class="gd_h_main">Smart</div>
                            <span class="gd_h_sub">Homes</span>
                        </div>
                        <div class="gd_icon">
                            <i class="gd_icon_arrow_right_t gd_txt_size_12"></i>
                        </div>
                    </a> -->
                </div>
            </div>
        
        </div>
    </div>





</section>