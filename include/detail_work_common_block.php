
    <section class="gd_section">
        <div class="gd_wrpr_outer bg_clr_white_fade">
            
            <!--theme1-COMMON_WRAPPER_1-Wrapper open-->
            <div class="gd_wrpr_full structurebox">
                <div class="gd_wrpr_outer">

                    <!-- <div class="gd_wrpr_3 gd_wrpr_4_lg gd_wrpr_6_sm gd_p_5 ">
                        <div class="gd_wrpr_inner banner_container gd_overflow_hidden">
                            <a href="images/detail/work_images/1.jpg" class="venobox gl_link_fullwidth vbox-item" data-gall="gall-img "></a>
                            <div class="gd_wrpr_full gd_bg_img">
                                <img class="banner_bg" src="images/detail/work_images/1.jpg">
                            </div>
                        </div>
                    </div> -->

                    <div class="gd_wrpr_3 gd_wrpr_4_lg gd_wrpr_6_sm">
                        <div class="gd_wrpr_inner">

                            <div class="gd_wrpr_full">
                                <div class="gd_wrpr_inner gd_border_1 gd_border_clr_white gd_p_tb_15 gd_p_lr_20 gd_p_15_lg gd_p_b_5_xs secondary_bg_shade_hover banner_container">
                                    <a href="images/detail/work_images/1.jpg" class="venobox gl_link_fullwidth vbox-item" data-gall="gall-img "></a>

                                    <!-- 298x278 manipulation 338x315 ratio -->
                                    <div class="gd_wrpr_full gd_bg_img gd_m_b_25 gd_m_b_15_lg gd_overflow_hidden">
                                        <img src="images/detail/work_images/1.jpg" alt="" class="banner_bg">
                                    </div>
                                    <div class="gd_wrpr_full gd_m_b_15 gd_element_center gd_txt_size_14 gd_line_height_14 gd_txt_weight_700 gd_txt_size_13_lg gd_line_height_13_lg gd_txt_size_12_xs gd_line_height_12_xs">
                                        CONSTRUCTION OF 046 WK<br>
                                        COMMERCIAL APARTMENT
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                    <div class="gd_wrpr_3 gd_wrpr_4_lg gd_wrpr_6_sm">
                        <div class="gd_wrpr_inner">

                            <div class="gd_wrpr_full">
                                <div class="gd_wrpr_inner gd_border_1 gd_border_clr_white gd_p_tb_15 gd_p_lr_20 gd_p_15_lg gd_p_b_5_xs secondary_bg_shade_hover banner_container">
                                    <a href="images/detail/work_images/2.jpg" class="venobox gl_link_fullwidth vbox-item" data-gall="gall-img "></a>
                                    <div class="gd_wrpr_full gd_bg_img gd_m_b_25 gd_m_b_15_lg gd_overflow_hidden">
                                        <img src="images/detail/work_images/2.jpg" alt="" class="banner_bg">
                                    </div>
                                    <div class="gd_wrpr_full gd_m_b_15 gd_element_center gd_txt_size_14 gd_line_height_14 gd_txt_weight_700 gd_txt_size_13_lg gd_line_height_13_lg gd_txt_size_12_xs gd_line_height_12_xs">
                                        CONSTRUCTION OF 046 WK<br>
                                        COMMERCIAL APARTMENT
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                    <div class="gd_wrpr_3 gd_wrpr_4_lg gd_wrpr_6_sm">
                        <div class="gd_wrpr_inner">

                            <div class="gd_wrpr_full">
                                <div class="gd_wrpr_inner gd_border_1 gd_border_clr_white gd_p_tb_15 gd_p_lr_20 gd_p_15_lg gd_p_b_5_xs secondary_bg_shade_hover banner_container">
                                    <a href="images/detail/work_images/3.jpg" class="venobox gl_link_fullwidth vbox-item" data-gall="gall-img "></a>
                                    <div class="gd_wrpr_full gd_bg_img gd_m_b_25 gd_m_b_15_lg gd_overflow_hidden">
                                        <img src="images/detail/work_images/3.jpg" alt="" class="banner_bg">
                                    </div>
                                    <div class="gd_wrpr_full gd_m_b_15 gd_element_center gd_txt_size_14 gd_line_height_14 gd_txt_weight_700 gd_txt_size_13_lg gd_line_height_13_lg gd_txt_size_12_xs gd_line_height_12_xs">
                                        CONSTRUCTION OF 046 WK<br>
                                        COMMERCIAL APARTMENT
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                    <div class="gd_wrpr_3 gd_wrpr_4_lg gd_wrpr_6_sm">
                        <div class="gd_wrpr_inner">

                            <div class="gd_wrpr_full">
                                <div class="gd_wrpr_inner gd_border_1 gd_border_clr_white gd_p_tb_15 gd_p_lr_20 gd_p_15_lg gd_p_b_5_xs secondary_bg_shade_hover banner_container">
                                    <a href="images/detail/work_images/4.jpg" class="venobox gl_link_fullwidth vbox-item" data-gall="gall-img "></a>
                                    <div class="gd_wrpr_full gd_bg_img gd_m_b_25 gd_m_b_15_lg gd_overflow_hidden">
                                        <img src="images/detail/work_images/4.jpg" alt="" class="banner_bg">
                                    </div>
                                    <div class="gd_wrpr_full gd_m_b_15 gd_element_center gd_txt_size_14 gd_line_height_14 gd_txt_weight_700 gd_txt_size_13_lg gd_line_height_13_lg gd_txt_size_12_xs gd_line_height_12_xs">
                                        CONSTRUCTION OF 046 WK<br>
                                        COMMERCIAL APARTMENT
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                    <div class="gd_wrpr_3 gd_wrpr_4_lg gd_wrpr_6_sm">
                        <div class="gd_wrpr_inner">

                            <div class="gd_wrpr_full">
                                <div class="gd_wrpr_inner gd_border_1 gd_border_clr_white gd_p_tb_15 gd_p_lr_20 gd_p_15_lg gd_p_b_5_xs secondary_bg_shade_hover banner_container">
                                    <a href="images/detail/work_images/5.jpg" class="venobox gl_link_fullwidth vbox-item" data-gall="gall-img "></a>
                                    <div class="gd_wrpr_full gd_bg_img gd_m_b_25 gd_m_b_15_lg gd_overflow_hidden">
                                        <img src="images/detail/work_images/5.jpg" alt="" class="banner_bg">
                                    </div>
                                    <div class="gd_wrpr_full gd_m_b_15 gd_element_center gd_txt_size_14 gd_line_height_14 gd_txt_weight_700 gd_txt_size_13_lg gd_line_height_13_lg gd_txt_size_12_xs gd_line_height_12_xs">
                                        CONSTRUCTION OF 046 WK<br>
                                        COMMERCIAL APARTMENT
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                    <div class="gd_wrpr_3 gd_wrpr_4_lg gd_wrpr_6_sm">
                        <div class="gd_wrpr_inner">

                            <div class="gd_wrpr_full">
                                <div class="gd_wrpr_inner gd_border_1 gd_border_clr_white gd_p_tb_15 gd_p_lr_20 gd_p_15_lg gd_p_b_5_xs secondary_bg_shade_hover banner_container">
                                    <a href="images/detail/work_images/6.jpg" class="venobox gl_link_fullwidth vbox-item" data-gall="gall-img "></a>
                                    <div class="gd_wrpr_full gd_bg_img gd_m_b_25 gd_m_b_15_lg gd_overflow_hidden">
                                        <img src="images/detail/work_images/6.jpg" alt="" class="banner_bg">
                                    </div>
                                    <div class="gd_wrpr_full gd_m_b_15 gd_element_center gd_txt_size_14 gd_line_height_14 gd_txt_weight_700 gd_txt_size_13_lg gd_line_height_13_lg gd_txt_size_12_xs gd_line_height_12_xs">
                                        CONSTRUCTION OF 046 WK<br>
                                        COMMERCIAL APARTMENT
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                    <div class="gd_wrpr_3 gd_wrpr_4_lg gd_wrpr_6_sm">
                        <div class="gd_wrpr_inner">

                            <div class="gd_wrpr_full">
                                <div class="gd_wrpr_inner gd_border_1 gd_border_clr_white gd_p_tb_15 gd_p_lr_20 gd_p_15_lg gd_p_b_5_xs secondary_bg_shade_hover banner_container">
                                    <a href="images/detail/work_images/7.jpg" class="venobox gl_link_fullwidth vbox-item" data-gall="gall-img "></a>
                                    <div class="gd_wrpr_full gd_bg_img gd_m_b_25 gd_m_b_15_lg gd_overflow_hidden">
                                        <img src="images/detail/work_images/7.jpg" alt="" class="banner_bg">
                                    </div>
                                    <div class="gd_wrpr_full gd_m_b_15 gd_element_center gd_txt_size_14 gd_line_height_14 gd_txt_weight_700 gd_txt_size_13_lg gd_line_height_13_lg gd_txt_size_12_xs gd_line_height_12_xs">
                                        CONSTRUCTION OF 046 WK<br>
                                        COMMERCIAL APARTMENT
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                    <div class="gd_wrpr_3 gd_wrpr_4_lg gd_wrpr_6_sm">
                        <div class="gd_wrpr_inner">

                            <div class="gd_wrpr_full">
                                <div class="gd_wrpr_inner gd_border_1 gd_border_clr_white gd_p_tb_15 gd_p_lr_20 gd_p_15_lg gd_p_b_5_xs secondary_bg_shade_hover banner_container">
                                    <a href="images/detail/work_images/8.jpg" class="venobox gl_link_fullwidth vbox-item" data-gall="gall-img "></a>
                                    <div class="gd_wrpr_full gd_bg_img gd_m_b_25 gd_m_b_15_lg gd_overflow_hidden">
                                        <img src="images/detail/work_images/8.jpg" alt="" class="banner_bg">
                                    </div>
                                    <div class="gd_wrpr_full gd_m_b_15 gd_element_center gd_txt_size_14 gd_line_height_14 gd_txt_weight_700 gd_txt_size_13_lg gd_line_height_13_lg gd_txt_size_12_xs gd_line_height_12_xs">
                                        CONSTRUCTION OF 046 WK<br>
                                        COMMERCIAL APARTMENT
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>


                </div>
            </div>


        </div>
    </section>