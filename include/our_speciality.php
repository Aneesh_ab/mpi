<section class="gd_section">
    <div class="gd_wrpr_full gd_wrpr_inner gd_position_absolute gd_overflow_hidden secondary_font text_clr_7_light">
        <div class="gd_wrpr_auto gd_position_absolute gd_txt_weight_400 bg_font_text gd_display_table gd_left_100n rellax_action" data-rellax-speed="-4">
            OUR SPECIALITIES        
        </div>
    </div>

    <div class="gd_wrpr_outer gd_p_lr_50  gd_p_tb_60 gd_p_lr_25_xl gd_p_lr_15_lg gd_p_tb_45_xl gd_p_tb_25_lg">
        
        <div class="gd_wrpr_5 gd_wrpr_12_xl gd_m_b_35_xl" data-aos="fade-up">
            <div class="gd_wrpr_outer gd_p_r_80 gd_p_r_0_xl">
                <div class="gd_wrpr_full gd_txt_size_40 gd_line_height_40 gd_txt_weight_400 gd_m_b_30 gd_txt_size_35_xl gd_line_height_35_xl gd_txt_size_30_lg gd_line_height_30_lg gd_txt_size_25_xs gd_txt_line_height_25_xs gd_m_b_25_xl gd_m_b_20_lg  gd_m_b_15_xs secondary_font text_clr_7">
                    OUR SPECIALITIES
                </div>   
                <div class="gd_para gd_txt_size_14 gd_line_height_20 gd_txt_weight_400 gd_m_b_30 gd_txt_size_13_lg gd_txt_size_13_xs gd_line_height_18_lg gd_m_b_25_xl gd_m_b_20_lg gd_m_b_15_xs text_clr_4">
                    Hailstone Innovations is made up of a global pool of highly dedicated professionals working closely together to ensure our customer’s success. People with outstanding expertise, commitment and drive to make profitable advancements, reach new heights in service and deliver real results. This empowers us with the unique insight to create effective solutions that increases the productivity and environmental quality of our customers operations. We believe that the measure of our worth is in the result we deliver to our customers.
                </div>
                <a href="javascript:void(0)" class="gd_link gd_wrpr_auto gd_txt_size_11 gd_txt_weight_400 gd_p_tb_10 gd_p_lr_20 gd_txt_clr_white secondary_bg_clr primary_bg_hover">VIEW MORE</a>         
            </div>
        </div>

        <div class="gd_wrpr_7 gd_wrpr_12_xl" data-aos-delay="500" data-aos="fade-up">
            <div class="gd_wrpr_outer">

                <div class="gd_bg_clr_white gd_border_1 gd_p_15 gd_p_b_65 gd_p_b_50_xl gd_p_b_25_xs border_clr_ash1 common_split_3 box_hover_effect primary_hover_txt_clr gray_scale_hover txt_clr_7_hover">
                    <a href="javascript:void(0)" class="gl_link_fullwidth"></a>
                    <div class="gd_wrpr_full gd_txt_size_55 gd_line_height_55 gd_txt_weight_400 gd_m_b_30 gd_txt_size_45_xl gd_line_height_45_xl gd_txt_size_35_lg gd_line_height_35_lg gd_txt_size_30_xs gd_line_height_30_xs gd_m_b_15_xs third_font text_clr_12 hover_txt_block">
                        01
                    </div>
                    <div class="gd_wrpr_full gd_txt_size_85 gd_line_height_85 gd_element_center gd_txt_align_center gd_m_b_30 gd_txt_size_70_xl gd_line_height_70_xl gd_txt_size_65_lg gd_txt_size_65_lg gd_m_b_15_xs text_clr_11">
                        <!-- <span class="gd_icon_hand_shake"></span> -->
                        <img src="images/index/speciality1.svg" alt="">
                    </div>
                    <div class="gd_wrpr_full gd_txt_size_25 gd_line_height_25 gd_txt_align_center gd_txt_size_20_lg gd_line_height_20_lg gd_txt_size_18_xs gd_line_height_18_xs text_clr_11 secondary_font txt_hover_clr_7">
                        TECHNOLOGY
                    </div>
                </div>
                <div class="gd_bg_clr_white gd_border_1 gd_p_15 gd_p_b_65 gd_p_b_50_xl gd_p_b_25_xs border_clr_ash1 common_split_3 box_hover_effect primary_hover_txt_clr gray_scale_hover txt_clr_7_hover">
                    <a href="javascript:void(0)" class="gl_link_fullwidth"></a>
                    <div class="gd_wrpr_full gd_txt_size_55 gd_line_height_55 gd_txt_weight_400 gd_m_b_30 gd_txt_size_45_xl gd_line_height_45_xl gd_txt_size_35_lg gd_line_height_35_lg gd_txt_size_30_xs gd_line_height_30_xs gd_m_b_15_xs third_font text_clr_12 hover_txt_block">
                        02
                    </div>
                    <div class="gd_wrpr_full gd_txt_size_85 gd_line_height_85 gd_element_center gd_txt_align_center gd_m_b_30 gd_txt_size_70_xl gd_line_height_70_xl gd_txt_size_65_lg gd_txt_size_65_lg gd_m_b_15_xs text_clr_11">
                        <!-- <span class="gd_icon_hand_shake"></span> -->
                        <img src="images/index/speciality2.svg" alt="">
                    </div>
                    <div class="gd_wrpr_full gd_txt_size_25 gd_line_height_25 gd_txt_align_center gd_txt_size_20_lg gd_line_height_20_lg gd_txt_size_18_xs gd_line_height_18_xs text_clr_11 secondary_font txt_hover_clr_7">
                        INFRASTRUCTURE
                    </div>
                </div>
                <div class="gd_bg_clr_white gd_border_1 gd_p_15 gd_p_b_65 gd_p_b_50_xl gd_p_b_25_xs border_clr_ash1 common_split_3 box_hover_effect primary_hover_txt_clr gray_scale_hover txt_clr_7_hover">
                    <a href="javascript:void(0)" class="gl_link_fullwidth"></a>
                    <div class="gd_wrpr_full gd_txt_size_55 gd_line_height_55 gd_txt_weight_400 gd_m_b_30 gd_txt_size_45_xl gd_line_height_45_xl gd_txt_size_35_lg gd_line_height_35_lg gd_txt_size_30_xs gd_line_height_30_xs gd_m_b_15_xs third_font text_clr_12 hover_txt_block">
                        03
                    </div>
                    <div class="gd_wrpr_full gd_txt_size_85 gd_line_height_85 gd_element_center gd_txt_align_center gd_m_b_30 gd_txt_size_70_xl gd_line_height_70_xl gd_txt_size_65_lg gd_txt_size_65_lg gd_m_b_15_xs text_clr_11">
                        <!-- <span class="gd_icon_hand_shake"></span> -->
                        <img src="images/index/speciality3.svg" alt="">
                    </div>
                    <div class="gd_wrpr_full gd_txt_size_25 gd_line_height_25 gd_txt_align_center gd_txt_size_20_lg gd_line_height_20_lg gd_txt_size_18_xs gd_line_height_18_xs text_clr_11 secondary_font txt_hover_clr_7">
                        QUALITY
                    </div>
                </div>


                <!-- <div class="gd_wrpr_4 gd_m_r_30">
                    <div class="gd_wrpr_inner gd_bg_clr_white gd_border_1 gd_p_15 gd_p_b_65 border_clr_ash1">
                        <div class="gd_wrpr_full gd_txt_size_55 gd_line_height_55 gd_txt_weight_400 gd_m_b_30 gd_txt_size_45_xl gd_line_height_45_xl gd_txt_size_35_lg gd_line_height_35_lg gd_txt_size_30_xs gd_line_height_30_xs gd_m_b_15_xs third_font text_clr_12 hover_txt_block">
                            01
                        </div>
                        <div class="gd_wrpr_full gd_txt_size_85 gd_line_height_85 gd_element_center gd_txt_align_center gd_m_b_30 gd_txt_size_70_xl gd_line_height_70_xl gd_txt_size_65_lg gd_txt_size_65_lg gd_m_b_15_xs text_clr_11">
                            <span class="gd_icon_hand_shake"></span>
                            <img src="images/index/speciality1.svg" alt="">
                        </div>
                        <div class="gd_wrpr_full gd_txt_size_25 gd_line_height_25 gd_txt_align_center gd_txt_size_20_lg gd_line_height_20_lg gd_txt_size_18_xs gd_line_height_18_xs text_clr_11 secondary_font txt_hover_clr_7">
                            TECHNOLOGY
                        </div>
                    </div>
                </div>
                <div class="gd_wrpr_4 gd_m_r_30">
                    <div class="gd_wrpr_inner gd_bg_clr_white gd_border_1 gd_p_15 gd_p_b_65 border_clr_ash1">
                        <div class="gd_wrpr_full gd_txt_size_55 gd_line_height_55 gd_txt_weight_400 gd_m_b_30 gd_txt_size_45_xl gd_line_height_45_xl gd_txt_size_35_lg gd_line_height_35_lg gd_txt_size_30_xs gd_line_height_30_xs gd_m_b_15_xs third_font text_clr_12 hover_txt_block">
                            01
                        </div>
                        <div class="gd_wrpr_full gd_txt_size_85 gd_line_height_85 gd_element_center gd_txt_align_center gd_m_b_30 gd_txt_size_70_xl gd_line_height_70_xl gd_txt_size_65_lg gd_txt_size_65_lg gd_m_b_15_xs text_clr_11">
                            <span class="gd_icon_hand_shake"></span>
                            <img src="images/index/speciality1.svg" alt="">
                        </div>
                        <div class="gd_wrpr_full gd_txt_size_25 gd_line_height_25 gd_txt_align_center gd_txt_size_20_lg gd_line_height_20_lg gd_txt_size_18_xs gd_line_height_18_xs text_clr_11 secondary_font txt_hover_clr_7">
                            TECHNOLOGY
                        </div>
                    </div>
                </div>
                <div class="gd_wrpr_4 gd_m_r_30">
                    <div class="gd_wrpr_inner gd_bg_clr_white gd_border_1 gd_p_15 gd_p_b_65 border_clr_ash1">
                        <div class="gd_wrpr_full gd_txt_size_55 gd_line_height_55 gd_txt_weight_400 gd_m_b_30 gd_txt_size_45_xl gd_line_height_45_xl gd_txt_size_35_lg gd_line_height_35_lg gd_txt_size_30_xs gd_line_height_30_xs gd_m_b_15_xs third_font text_clr_12 hover_txt_block">
                            01
                        </div>
                        <div class="gd_wrpr_full gd_txt_size_85 gd_line_height_85 gd_element_center gd_txt_align_center gd_m_b_30 gd_txt_size_70_xl gd_line_height_70_xl gd_txt_size_65_lg gd_txt_size_65_lg gd_m_b_15_xs text_clr_11">
                            <span class="gd_icon_hand_shake"></span>
                            <img src="images/index/speciality1.svg" alt="">
                        </div>
                        <div class="gd_wrpr_full gd_txt_size_25 gd_line_height_25 gd_txt_align_center gd_txt_size_20_lg gd_line_height_20_lg gd_txt_size_18_xs gd_line_height_18_xs text_clr_11 secondary_font txt_hover_clr_7">
                            TECHNOLOGY
                        </div>
                    </div>
                </div> -->

                
            </div>
        </div>

    </div>
</section>