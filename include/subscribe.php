<section class="gd_section">
    <div class="gd_wrpr_inner gd_position_absolute gd_top_0 gd_left_0 gd_bg_img">
        <img src="images/index/subscribe_bg.jpg" alt="">
    </div>
    <div class="gd_wrpr_outer gd_p_lr_50  gd_p_tb_60 gd_p_lr_25_xl gd_p_lr_15_lg gd_p_tb_45_xl gd_p_tb_25_lg secondary_bg_clr_light">
        <div class="gd_wrpr_4 gd_wrpr_12_xl gd_m_b_20_xl">
            <div class="gd_wrpr_inner">
                <div class="gd_h_wrpr gd_wrpr_full gd_txt_clr_white gd_txt_size_20 gd_line_height_20 gd_txt_weight_400 gd_p_r_50 gd_p_r_20_lg gd_p_r_0_xs gd_txt_size_16_xs secondary_font">
                    <div class="gd_h_main gd_m_b_10 gd_txt_align_left">DON'T WANT TO SKIP AN UPDATE OR A POST?</div>
                    <div class="gd_h_sub gd_h_sub_md gd_txt_align_left primary_font">Get the latest updates from MPI Construction!</div>
                </div>
            </div>
        </div>
        <div class="gd_wrpr_6 gd_wrpr_8_xl gd_wrpr_12_xs gd_m_b_15_xs">
            <div class="gd_wrpr_inner">
                <form action="" class="gd_wrpr_full">
                    <div class="gd_wrpr_inner">
                        <div class="gd_wrpr_auto gd_m_r_10 flex_tip">
                            <input type="text" class="gd_wrpr_full gd_border_0 gd_txt_size_14 gd_txt_weight_400 gd_line_height_14 gd_p_15 gd_border_radius_5 gd_p_tb_15_xl gd_p_tb_15_xs text_clr_7 gd_txt_size_12_xs primary_font" placeholder="Enter your Email Address">                    
                            <label for="" class="error">Invalid</label>
                        </div>
                        <div class="gd_wrpr_auto">
                            <button type="button" class="gd_btn gd_border_radius_5 text_clr_0 gd_txt_size_14 gd_txt_weight_400 gd_line_height_14 gd_p_tb_15 gd_p_lr_65 gd_p_tb_15_xl gd_p_lr_50_lg  gd_p_lr_30_sm gd_p_lr_20_xs gd_p_tb_15_xs gd_txt_size_12_xs secondary_bg_clr_light1 primary_font primary_bg_hover">SUBMIT</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="gd_wrpr_2 gd_wrpr_4_xl gd_wrpr_12_xs">
            <div class="gd_wrpr_inner gd_p_l_60 gd_flex_column gd_p_l_40_lg gd_p_l_0_xs custom_subscribe">
                <div class="gd_wrpr_full gd_txt_size_14 gd_txt_weight_400 gd_line_height_14 gd_txt_clr_white gd_m_b_10 gd_txt_size_12_xs primary_font">
                    Stay Connected:
                </div>
                <div class="gd_wrpr_full gd_listgroup gd_listgroup_block">
                    <div class="gd_listitem gd_m_r_25 gd_m_r_15_lg">
                        <a href="javascript:void(0)" target="_blank" class="gd_link gd_txt_size_20 gd_line_height_20 gd_txt_clr_white gd_txt_size_16_xs">
                           <span class="gd_icon_facebook_f"></span>
                        </a>
                    </div>
                    <div class="gd_listitem gd_m_r_25 gd_m_r_15_lg">
                        <a href="javascript:void(0)" target="_blank" class="gd_link gd_txt_size_20 gd_line_height_20 gd_txt_clr_white gd_txt_size_16_xs">
                           <span class="gd_icon_youtube_t"></span>
                        </a>
                    </div>
                    <div class="gd_listitem gd_m_r_25 gd_m_r_15_lg">
                        <a href="javascript:void(0)" target="_blank" class="gd_link gd_txt_size_20 gd_line_height_20 gd_txt_clr_white gd_txt_size_16_xs">
                           <span class="gd_icon_instagram"></span>
                        </a>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>