<section class="gd_section">

    <div class="gd_wrpr_full gd_wrpr_inner gd_right_0 gd_position_absolute gd_height_full gd_wrpr_12_sm">
        <div class="gd_wrpr_inner gd_bg_img rellax_action style_02_parallax" data-rellax-speed="-2">
            <img src="images/index/virtual_design.jpg" alt="">
        </div>
        <!-- <div class="gd_overlay primary_bg_clr_light"></div> -->
    </div>

    <div class="gd_wrpr_outer gd_p_lr_50  gd_p_tb_100 gd_p_lr_25_xl gd_p_lr_15_lg gd_p_tb_45_xl gd_p_tb_100_lg gd_p_lr_100_lg gd_p_tb_80_xs gd_p_lr_15_xs ">
        <div class="gd_wrpr_7 gd_m_l_auto gd_border_5 gd_border_clr_ash gd_wrpr_8_xl gd_wrpr_12_lg ">
            <div class="gd_wrpr_inner gd_p_tb_50 gd_p_l_100 gd_p_r_55 gd_bg_clr_white gd_p_tb_30_xl gd_p_lr_50_xl gd_p_tb_25_lg gd_p_lr_15_lg top_btm_lines gd_justify_content_center_md" >
                <div class="gd_wrpr_full gd_display_flex gd_justify_content_end gd_flex_wrap gd_align_items_center_lg gd_justify_content_center_md">
                <div class="gd_wrpr_full gd_h_wrpr gd_txt_align_left gd_txt_size_80 gd_line_height_80 gd_txt_weight_700 gd_txt_clr_black gd_txt_size_45_xl gd_line_height_45_xl gd_txt_size_35_lg gd_line_height_35_lg gd_txt_size_25_md gd_line_height_25_md gd_txt_size_20_xs gd_line_height_20_xs gd_m_b_25 gd_m_b_15_xl primary_font custom_text_shape">
                    <div class="gd_h_main">
                        VIRTUAL DESIGN <br>
                        & CONSTRUCTION
                    </div>
                </div>
                <div class="gd_bg_img postion_element_img_01 gd_m_b_20">
                    <img src="images/index/01.jpg" alt="">
                </div>
                </div>
                <div class="gd_txt_size_16 gd_line_height_25 gd_txt_size_14_lg gd_line_height_20_lg gd_txt_weight_400 gd_txt_size_13_lg gd_txt_size_13_xs gd_line_height_18_lg text_clr_0 gd_m_b_15 gd_txt_align_center_md primary_font" >
                    MPI offers the expert services of an in-house Building Information Modeling (BIM) team to benefit every phase of the project life cycle.  We provide a total BIM design solution that improves collaboration and communication between all stakeholders, resulting in better quality of build, higher speed and productivity and more predictable and cost-effective solutions. 
                </div>
                <a href="javascript:void(0)" class="gd_link primary_font gd_txt_size_11 gd_line_height_11 text_clr_0 btn_style_01 secondary_bg_clr gd_p_tb_10 gd_p_lr_20 primary_bg_hover">VIEW MORE</a>

            </div>
        </div>
    </div>
</section>









