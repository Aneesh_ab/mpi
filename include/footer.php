




<section class="gd_wrpr_full ">
    <div class="gd_wrpr_outer gd_p_lr_50  gd_p_tb_30 primary_bg_clr_1 gd_p_lr_25_xl gd_p_lr_15_lg gd_p_tb_45_xl gd_p_tb_25_lg">
        <div class="gd_wrpr_4 gd_wrpr_12_md">
            <div class="gd_wrpr_inner gd_flex_column gd_align_items_start gd_m_b_45_md">
                <div class="gd_wrpr_full gd_m_b_30 gd_m_b_20_xl  gd_m_b_15_lg">
                    <a href="index.php" class="gd_link gd_wrpr_auto footer_logo logo_width gd_bg_clr_white">
                       <img src="images/header/logo.svg" alt="">
                    </a>
                </div>
                <!-- <div class="gd_wrpr_full gd_txt_size_16 gd_txt_weight_400 gd_line_height_16 gd_m_b_15 gd_txt_size_15_xl gd_m_b_20_xl gd_txt_clr_white">About MPI</div> -->
                <div class="gd_para gd_txt_size_12 gd_line_height_20 gd_txt_clr_white gd_txt_weight_400 gd_p_r_90 gd_m_b_30 gd_p_r_50_xl  gd_p_r_25_lg gd_m_b_15_xl gd_p_r_0_md primary_font">
                    
                    <p>Since MPI’s inception in 2015 as a British Omani company, we have established our reputation as the civil and construction engineering partner of choice. MPI focuses on delivering value and eliminating waste at every step of the construction process.</p>
                </div>
                <a href="javascript:void(0)" class="gd_link gd_wrpr_auto gd_p_tb_10 gd_p_lr_35  gd_border_clr_white bg_clr_light_01 gd_txt_clr_white gd_txt_size_12 gd_line_height_12 gd_txt_weight_400 gd_txt_size_10_md gd_line_height_10_md secondary_bg_hover">
                    ABOUT US
                </a>
            </div>
        </div>

        <div class="gd_wrpr_5 gd_wrpr_12_md gd_wrpr_12_xs">
            <div class="gd_wrpr_outer gd_m_b_20_xs gd_txt_clr_white primary_font gd_m_b_20_md">
                
                <div class="gd_wrpr_6 gd_wrpr_outer gd_wrpr_12_xs gd_m_b_15_xs">
                    <div class="gd_wrpr_full gd_txt_size_16 gd_txt_weight_400 gd_line_height_16 gd_m_b_15 gd_txt_size_15_xl gd_wrpr_full">Quick Links</div>
                    <ul class=" gd_listgroup gd_listgroup_block gd_flex_column gd_align_items_start">
                                    <li class="gd_listitem gd_listitem_block gd_m_b_15">
                                        <div class="gd_wrpr_full gd_wrpr_outer gd_element_vcenter gd_txt_size_9 gd_txt_weight_400">
                                            <span class="gd_icon_arrow2_right gd_wrpr_auto gd_m_r_10"></span>
                                            <a href="index.php" class="gd_link gd_wrpr_auto gd_txt_size_12 gd_line_height_12 gd_txt_weight_400">
                                                Home
                                            </a>
                                        </div>
                                    </li>
                                    <li class="gd_listitem gd_listitem_block gd_m_b_15">
                                        <div class="gd_wrpr_full gd_wrpr_outer gd_element_vcenter gd_txt_size_9 gd_txt_weight_400">
                                            <span class="gd_icon_arrow2_right gd_wrpr_auto gd_m_r_10"></span>
                                            <a href="javascript:void(0)" class="gd_link gd_wrpr_auto gd_txt_size_12 gd_line_height_12 gd_txt_weight_400">
                                               Services
                                            </a>
                                        </div>
                                    </li>
                                    <li class="gd_listitem gd_listitem_block gd_m_b_15">
                                        <div class="gd_wrpr_full gd_wrpr_outer gd_element_vcenter gd_txt_size_9 gd_txt_weight_400">
                                            <span class="gd_icon_arrow2_right gd_wrpr_auto gd_m_r_10"></span>
                                            <a href="javascript:void(0)" class="gd_link gd_wrpr_auto gd_txt_size_12 gd_line_height_12 gd_txt_weight_400">
                                                About us
                                            </a>
                                        </div>
                                    </li>
                                    <li class="gd_listitem gd_listitem_block gd_m_b_15">
                                        <div class="gd_wrpr_full gd_wrpr_outer gd_element_vcenter gd_txt_size_9 gd_txt_weight_400">
                                            <span class="gd_icon_arrow2_right gd_wrpr_auto gd_m_r_10"></span>
                                            <a href="javascript:void(0)" class="gd_link gd_wrpr_auto gd_txt_size_12 gd_line_height_12 gd_txt_weight_400">
                                                Contact us
                                            </a>
                                        </div>
                                    </li>
                                    <li class="gd_listitem gd_listitem_block gd_m_b_15">
                                        <div class="gd_wrpr_full gd_wrpr_outer gd_element_vcenter gd_txt_size_9 gd_txt_weight_400">
                                            <span class="gd_icon_arrow2_right gd_wrpr_auto gd_m_r_10"></span>
                                            <a href="javascript:void(0)" class="gd_link gd_wrpr_auto gd_txt_size_12 gd_line_height_12 gd_txt_weight_400">
                                                Employee Login
                                            </a>
                                        </div>
                                    </li>
                                </ul>
                            </div>

                <div class="gd_wrpr_6 gd_wrpr_outer gd_wrpr_12_xs"    >
                    <div class="gd_wrpr_full gd_txt_size_16 gd_txt_weight_400 gd_line_height_16 gd_m_b_15 gd_txt_size_15_xl gd_m_b_20_xl">Services</div>
                    <ul class=" gd_listgroup gd_listgroup_block gd_flex_column gd_align_items_start">

                                     <li class="gd_listitem gd_listitem_block gd_m_b_15">
                                        <div class="gd_wrpr_full gd_wrpr_outer gd_element_vcenter gd_txt_size_9 gd_txt_weight_400">
                                            <span class="gd_icon_arrow2_right gd_wrpr_auto gd_m_r_10"></span>
                                            <a href="javascript:void(0)" class="gd_link gd_wrpr_auto gd_txt_size_12 gd_line_height_12 gd_txt_weight_400">
                                                Projects

                                            </a>
                                        </div>
                                    </li>
                                     
                                    <li class="gd_listitem gd_listitem_block gd_m_b_15">
                                        <div class="gd_wrpr_full gd_wrpr_outer gd_element_vcenter gd_txt_size_9 gd_txt_weight_400">
                                            <span class="gd_icon_arrow2_right gd_wrpr_auto gd_m_r_10"></span>
                                            <a href="javascript:void(0)" class="gd_link gd_wrpr_auto gd_txt_size_12 gd_line_height_12 gd_txt_weight_400">
                                                LGS
                                            </a>
                                        </div>
                                    </li>
                                    <li class="gd_listitem gd_listitem_block gd_m_b_15">
                                        <div class="gd_wrpr_full gd_wrpr_outer gd_element_vcenter gd_txt_size_9 gd_txt_weight_400">
                                            <span class="gd_icon_arrow2_right gd_wrpr_auto gd_m_r_10"></span>
                                            <a href="javascript:void(0)" class="gd_link gd_wrpr_auto gd_txt_size_12 gd_line_height_12 gd_txt_weight_400">
                                                Design & Build
                                            </a>
                                        </div>
                                    </li>
                                    
                                    <li class="gd_listitem gd_listitem_block gd_m_b_15">
                                        <div class="gd_wrpr_full gd_wrpr_outer gd_element_vcenter gd_txt_size_9 gd_txt_weight_400">
                                            <span class="gd_icon_arrow2_right gd_wrpr_auto gd_m_r_10"></span>
                                            <a href="javascript:void(0)" class="gd_link gd_wrpr_auto gd_txt_size_12 gd_line_height_12 gd_txt_weight_400">
                                                Virtual Design & Construction

                                            </a>
                                        </div>
                                    </li>
                                    <li class="gd_listitem gd_listitem_block gd_m_b_15">
                                        <div class="gd_wrpr_full gd_wrpr_outer gd_element_vcenter gd_txt_size_9 gd_txt_weight_400">
                                            <span class="gd_icon_arrow2_right gd_wrpr_auto gd_m_r_10"></span>
                                            <a href="javascript:void(0)" class="gd_link gd_wrpr_auto gd_txt_size_12 gd_line_height_12 gd_txt_weight_400">
                                                Integrating MEP

                                            </a>
                                        </div>
                                    </li>
                                </ul></div> 
            </div>
        </div>

        <div class="gd_wrpr_3 gd_wrpr_12_md gd_wrpr_12_xs">
            <div class="gd_wrpr_outer gd_txt_clr_white primary_font">
                <div class="gd_wrpr_full gd_txt_size_16 gd_txt_weight_400 gd_line_height_16 gd_m_b_15 gd_txt_size_15_xl gd_m_b_20_xl">Contact Us</div>
                <div class="gd_wrpr_full">
                    <div class="gd_wrpr_outer gd_m_b_25 gd_m_b_20_xl">
                        <a class="gl_link_fullwidth" href="javascript:void(0)"></a>
                        <div class="gd_wrpr_auto gd_txt_size_18 gd_m_r_10">
                            <span class="gd_icon_location"></span>
                        </div>
                        <div class="gd_wrpr gd_txt_size_12 gd_txt_weight_400 gd_line_height_26 gd_txt_clr_white gd_element_vcenter flex_tip">
                            7th Floor, Super Plaza Building,<br>
                            340, Way 4805, Azaiba,<br>
                            Muscat, Sultanate of Oman
                        </div>
                    </div>                            
                    <div class="gd_wrpr_outer gd_m_b_25 gd_m_b_20_xl">
                        <div class="gd_wrpr_auto gd_txt_size_18 gd_m_r_10">
                            <span class="gd_icon_baselinecall"></span>
                        </div>
                        <div class="gd_wrpr gd_txt_size_12 gd_txt_weight_400 gd_line_height_26 gd_txt_clr_white gd_element_vcenter flex_tip">
                            <a href="tel:+968 2412 8989" class="gd_link">+968 2412 8989 </a> /  <a href="tel:2412 8833" class="gd_link">2412 8833</a>
                        </div>
                    </div>                            
                    <div class="gd_wrpr_outer">
                        <div class="gd_wrpr_auto gd_txt_size_18 gd_m_r_10">
                            <span class="gd_icon_mail"></span>
                        </div>
                        <div class=" gd_listgroup gd_listgroup_block gd_wrpr gd_txt_size_12 gd_txt_weight_400 gd_line_height_12 gd_txt_clr_white flex_tip gd_flex_column">
                            <div class="gd_listitem gd_listitem_block gd_wrpr_full gd_m_b_15 custom_link_color">
                                <a href="mailto:info@mpioman.com" class="gd_link">info@mpioman.com <span>[general]</span></a>
                            </div>
                            <div class="gd_listitem gd_listitem_block gd_wrpr_full gd_m_b_15 custom_link_color">
                                <a href="mailto:tendering@mpioman.com" class="gd_link">tendering@mpioman.com <span>[tendering]</span></a>
                            </div>
                            <!-- <a href="mailto:hr@mpioman.com" class="gd_link">hr@mpioman.com [job seekers]</a><br> -->
                        </div>
                    </div>                            
                </div>
            </div>
        </div>





    </div>

    <div class="gd_wrpr_outer gd_element_vcenter gd_p_lr_50 gd_p_tb_20 gd_p_lr_25_xl gd_p_lr_15_lg gd_p_tb_15_lg gd_txt_size_12 gd_txt_size_10_xs text_clr_10 copyright_bg primary_font gd_justify_content_center_xs">
        © 2020  MPI CONSTRUCTION All rights reserved.
    </div>
</section>


 <!-- Website developed by  <a href="http://www.godlandit.com/" target="blank" class="gd_link gd_m_l_10">GL infotech</a> -->












<!-- after footer components -->

<div class="gd_backto_top gd_backto_top_1 gd_txt_clr_white border_clr_1 backto_top_size gd_txt_size_11 gd_bottom_80 box_shadow_1 secondary_bg_clr_light" data-animate-time="1000" data-scroll-view="true" data-scroll-position="10">
    <span class="gd_icon_arrow2_top_t"></span>
    <span class="gd_wrpr_auto gd_position_absolute gd_bottom_15n txt_clr_gray_02">Top</span>
</div>

<div class="gd_bg_clr_white gd_flex_column_lg leftnav gd_p_b_50" data-hangburger="wrapper"></div>

<div class="quicklink_resp_wrpr gd_element_hcenter" data-quicklink-resp="wrapper"></div>
<div class="gd_display_none">
    <?php include 'include/socialmedia.php'; ?>
</div>

