<section class="gd_section">
	<div class="gd_wrpr_outer">
		<div class="gd_wrpr_6 gd_wrpr_12_sm">
			<div class="gd_wrpr_inner gd_bg_clr_white gd_p_l_50 gd_p_r_100 gd_p_t_80 gd_p_b_50 text_clr_0 gd_border_t_3 border_clr_01 gd_p_15_lg gd_flex_column gd_overflow_hidden">
				<div class="gd_wrpr_full gd_txt_align_left gd_m_b_35 gd_txt_size_50 gd_line_height_50 gd_txt_size_40_xl gd_line_height_40_xl gd_txt_size_30_lg gd_line_height_30_lg gd_txt_size_20_md gd_line_height_20_md primary_font gd_txt_weight_700 gd_m_b_15_lg">QHSE COMMITMENT</div>
				<div class="gd_txt_size_16 gd_line_height_25 gd_txt_size_14_lg gd_line_height_20_lg text_clr_0 primary_font gd_txt_align_left gd_section_4 gd_m_b_60 gd_wrpr_full gd_m_b_15_lg gd_txt_size_13_lg gd_line_height_18_lg ">
					MPI is committed to meeting the needs and expectations of its clients by implementing an effective and well-managed Quality, Health, Safety and Environment Management System.  <br><br>
					HSE is an integral part of our operations and we are dedicated to maintaining the highest safety standards on every job site with our senior management providing strong and visible HSE leadership and commitment. 



				</div>
				<div class="bg_txt_parallax_common rellax_action style_parallax_01" data-rellax-speed="-4">QHSE</div>
				<!-- <div class="gd_link primary_font gd_txt_size_11 gd_line_height_11 text_clr_0 btn_style_01 secondary_bg_clr gd_p_tb_10 gd_p_lr_20 gd_position_absolute gd_bottom_15n gd_position_relative_lg gd_bottom_0_lg primary_bg_hover">VIEW MORE</div> -->
                <a href="javascript:void(0)" class="gd_link primary_font gd_txt_size_11 gd_line_height_11 text_clr_0 btn_style_01 secondary_bg_clr gd_p_tb_10 gd_p_lr_20 primary_bg_hover">LEARN MORE</a>
			</div>
		</div>
		<div class=" custome_position_spcial_wrpr01 custom_height ">
                <img src="images/what_new/01.jpg" class="gd_wrpr_full" alt="">
            </div>
	</div>
</section>