<div class="coustom_overflow_wrpr">
    <div class="owl-carousel gd_slider gd_p_b_50 gd_m_b_35_lg gd_m_b_0_md gd_p_tb_15_lg"
        data-slide-autoplay="true"
        data-slide-margin="0"
        data-slide-loop="true"
        data-slide-nav="false"
        data-slide-dots="false"
        data-slide-items="3"
        data-slide-autoplaytimeout="5000"
        data-slide-responsive-567="{items:3}"
        data-slide-responsive-0="{items:1}">
        <div class="item gd_widget gd_widget_1 gd_p_5 gd_wrpr_inner gd_p_20 gd_p_10_lg gd_wrpr_full hover_trigger_widget">
            <div class="d_wrpr_full gd_border_1 border_clr_gray_01 gd_m_b_15 gd_img_block ">
                <img class="" src="images/product/1.jpg" alt="" class="gd_img">
            </div>
            <div class="gd_wrpr_full gd_txt_size_14 gd_line_height_20 gd_m_b_15 gd_txt_align_center gd_txt_weight_700 primary_font txt_clr_7">CONE CRUSHER</div>
            <div class="gd_overlay overlay_bg_01 gd_txt_clr_white gd_element_hcenter gd_flex_column gd_p_25 hover_overlay gd_p_10_lg">
                <div class="gd_txt_size_16 gd_line_height_16 gd_txt_clr_white primary_font gd_txt_weight_700 gd_m_b_20 gd_m_b_15_lg gd_wrpr_full gd_txt_align_center gd_txt_size_14_lg gd_line_height_16_lg gd_m_b_10_lg">Vertical Shaft Impactor</div>
                <div class="gd_txt_size_14 gd_line_height_20 gd_txt_clr_white primary_font gd_txt_align_left gd_section_4 gd_m_b_15 gd_wrpr_full gd_m_b_15_lg gd_txt_size_13_lg gd_line_height_18_lg gd_wrpr_full gd_txt_align_center gd_txt_size_12_lg gd_line_height_16_lg para_height01 gd_txt_size_12_md gd_line_height_md_16">
                    We have been improving design on VSI for years as per our customer requirements. The customer satisfaction is our objective on product development. Our research and development activities have resulted in a well-shaped autogenous crusher. The EP series VSI crusher is a 3rd or 4th stage crusher. Our research and development activities have resulted in a well-shaped autogenous crusher. The EP series VSI crusher is a 3rd or 4th stage crusher.Our research and development activities have resulted in a well-shaped autogenous crusher. The EP series VSI crusher is a 3rd or 4th stage crusher.
                </div>
                <a href="javascript:void(0)" class="gd_link gd_wrpr_full gd_txt_size_11 gd_line_height_11 gd_p_15 gd_bg_clr_white gd_border_1 border_clr_blue gd_border_radius_5 gd_p_10_lg gd_txt_align_center txt_clr_blue">View Detials</a>
            </div>
        </div>


        <div class="item gd_widget gd_widget_1 gd_p_5 gd_wrpr_inner gd_p_20 gd_p_10_lg gd_wrpr_full hover_trigger_widget">
            <div class="d_wrpr_full gd_border_1 border_clr_gray_01 gd_m_b_15 gd_img_block ">
                <img class="" src="images/product/3.jpg" alt="" class="gd_img">
            </div>
            <div class="gd_wrpr_full gd_txt_size_14 gd_line_height_20 gd_m_b_15 gd_txt_align_center gd_txt_weight_700 primary_font txt_clr_7">CONE CRUSHER</div>
            <div class="gd_overlay overlay_bg_01 gd_txt_clr_white gd_element_hcenter gd_flex_column gd_p_25 hover_overlay gd_p_10_lg">
                <div class="gd_txt_size_16 gd_line_height_16 gd_txt_clr_white primary_font gd_txt_weight_700 gd_m_b_20 gd_m_b_15_lg gd_wrpr_full gd_txt_align_center gd_txt_size_14_lg gd_line_height_16_lg gd_m_b_10_lg">Vertical Shaft Impactor</div>
                <div class="gd_txt_size_14 gd_line_height_20 gd_txt_clr_white primary_font gd_txt_align_left gd_section_4 gd_m_b_15 gd_wrpr_full gd_m_b_15_lg gd_txt_size_13_lg gd_line_height_18_lg gd_wrpr_full gd_txt_align_center gd_txt_size_12_lg gd_line_height_16_lg para_height01 gd_txt_size_12_md gd_line_height_md_16">
                    We have been improving design on VSI for years as per our customer requirements. The customer satisfaction is our objective on product development. Our research and development activities have resulted in a well-shaped autogenous crusher. The EP series VSI crusher is a 3rd or 4th stage crusher. Our research and development activities have resulted in a well-shaped autogenous crusher. The EP series VSI crusher is a 3rd or 4th stage crusher.Our research and development activities have resulted in a well-shaped autogenous crusher. The EP series VSI crusher is a 3rd or 4th stage crusher.
                </div>
                <a href="javascript:void(0)" class="gd_link gd_wrpr_full gd_txt_size_11 gd_line_height_11 gd_p_15 gd_bg_clr_white gd_border_1 border_clr_blue gd_border_radius_5 gd_p_10_lg gd_txt_align_center txt_clr_blue">View Detials</a>
            </div>
        </div>



        <div class="item gd_widget gd_widget_1 gd_p_5 gd_wrpr_inner gd_p_20 gd_p_10_lg gd_wrpr_full hover_trigger_widget">
            <div class="d_wrpr_full gd_border_1 border_clr_gray_01 gd_m_b_15 gd_img_block ">
                <img class="" src="images/product/2.jpg" alt="" class="gd_img">
            </div>
            <div class="gd_wrpr_full gd_txt_size_14 gd_line_height_20 gd_m_b_15 gd_txt_align_center gd_txt_weight_700 primary_font txt_clr_7">JAW CRUSHER</div>
            <div class="gd_overlay overlay_bg_01 gd_txt_clr_white gd_element_hcenter gd_flex_column gd_p_25 hover_overlay gd_p_10_lg">
                <div class="gd_txt_size_16 gd_line_height_16 gd_txt_clr_white primary_font gd_txt_weight_700 gd_m_b_20 gd_m_b_15_lg gd_wrpr_full gd_txt_align_center gd_txt_size_14_lg gd_line_height_16_lg gd_m_b_10_lg">Vertical Shaft Impactor</div>
                <div class="gd_txt_size_14 gd_line_height_20 gd_txt_clr_white primary_font gd_txt_align_left gd_section_4 gd_m_b_15 gd_wrpr_full gd_m_b_15_lg gd_txt_size_13_lg gd_line_height_18_lg gd_wrpr_full gd_txt_align_center gd_txt_size_12_lg gd_line_height_16_lg para_height01 gd_txt_size_12_md gd_line_height_md_16">
                    We have been improving design on VSI for years as per our customer requirements. The customer satisfaction is our objective on product development. Our research and development activities have resulted in a well-shaped autogenous crusher. The EP series VSI crusher is a 3rd or 4th stage crusher. Our research and development activities have resulted in a well-shaped autogenous crusher. The EP series VSI crusher is a 3rd or 4th stage crusher.Our research and development activities have resulted in a well-shaped autogenous crusher. The EP series VSI crusher is a 3rd or 4th stage crusher.
                </div>
                <a href="javascript:void(0)" class="gd_link gd_wrpr_full gd_txt_size_11 gd_line_height_11 gd_p_15 gd_bg_clr_white gd_border_1 border_clr_blue gd_border_radius_5 gd_p_10_lg gd_txt_align_center txt_clr_blue">View Detials</a>
            </div>
        </div>


        <div class="item gd_widget gd_widget_1 gd_p_5 gd_wrpr_inner gd_p_20 gd_p_10_lg gd_wrpr_full hover_trigger_widget">
            <div class="d_wrpr_full gd_border_1 border_clr_gray_01 gd_m_b_15 gd_img_block ">
                <img class="" src="images/product/4.jpg" alt="" class="gd_img">
            </div>
            <div class="gd_wrpr_full gd_txt_size_14 gd_line_height_20 gd_m_b_15 gd_txt_align_center gd_txt_weight_700 primary_font txt_clr_7">GRIZZLY FEEDER</div>
            <div class="gd_overlay overlay_bg_01 gd_txt_clr_white gd_element_hcenter gd_flex_column gd_p_25 hover_overlay gd_p_10_lg">
                <div class="gd_txt_size_16 gd_line_height_16 gd_txt_clr_white primary_font gd_txt_weight_700 gd_m_b_20 gd_m_b_15_lg gd_wrpr_full gd_txt_align_center gd_txt_size_14_lg gd_line_height_16_lg gd_m_b_10_lg">Vertical Shaft Impactor</div>
                <div class="gd_txt_size_14 gd_line_height_20 gd_txt_clr_white primary_font gd_txt_align_left gd_section_4 gd_m_b_15 gd_wrpr_full gd_m_b_15_lg gd_txt_size_13_lg gd_line_height_18_lg gd_wrpr_full gd_txt_align_center gd_txt_size_12_lg gd_line_height_16_lg para_height01 gd_txt_size_12_md gd_line_height_md_16">
                    We have been improving design on VSI for years as per our customer requirements. The customer satisfaction is our objective on product development. Our research and development activities have resulted in a well-shaped autogenous crusher. The EP series VSI crusher is a 3rd or 4th stage crusher. Our research and development activities have resulted in a well-shaped autogenous crusher. The EP series VSI crusher is a 3rd or 4th stage crusher.Our research and development activities have resulted in a well-shaped autogenous crusher. The EP series VSI crusher is a 3rd or 4th stage crusher.
                </div>
                <a href="javascript:void(0)" class="gd_link gd_wrpr_full gd_txt_size_11 gd_line_height_11 gd_p_15 gd_bg_clr_white gd_border_1 border_clr_blue gd_border_radius_5 gd_p_10_lg gd_txt_align_center txt_clr_blue">View Detials</a>
            </div>
        </div>


        <div class="item gd_widget gd_widget_1 gd_p_5 gd_wrpr_inner gd_p_20 gd_p_10_lg gd_wrpr_full hover_trigger_widget">
            <div class="d_wrpr_full gd_border_1 border_clr_gray_01 gd_m_b_15 gd_img_block ">
                <img class="" src="images/product/1.jpg" alt="" class="gd_img">
            </div>
            <div class="gd_wrpr_full gd_txt_size_14 gd_line_height_20 gd_m_b_15 gd_txt_align_center gd_txt_weight_700 primary_font txt_clr_7">CONE CRUSHER</div>
            <div class="gd_overlay overlay_bg_01 gd_txt_clr_white gd_element_hcenter gd_flex_column gd_p_25 hover_overlay gd_p_10_lg">
                <div class="gd_txt_size_16 gd_line_height_16 gd_txt_clr_white primary_font gd_txt_weight_700 gd_m_b_20 gd_m_b_15_lg gd_wrpr_full gd_txt_align_center gd_txt_size_14_lg gd_line_height_16_lg gd_m_b_10_lg">Vertical Shaft Impactor</div>
                <div class="gd_txt_size_14 gd_line_height_20 gd_txt_clr_white primary_font gd_txt_align_left gd_section_4 gd_m_b_15 gd_wrpr_full gd_m_b_15_lg gd_txt_size_13_lg gd_line_height_18_lg gd_wrpr_full gd_txt_align_center gd_txt_size_12_lg gd_line_height_16_lg para_height01 gd_txt_size_12_md gd_line_height_md_16">
                    We have been improving design on VSI for years as per our customer requirements. The customer satisfaction is our objective on product development. Our research and development activities have resulted in a well-shaped autogenous crusher. The EP series VSI crusher is a 3rd or 4th stage crusher. Our research and development activities have resulted in a well-shaped autogenous crusher. The EP series VSI crusher is a 3rd or 4th stage crusher.Our research and development activities have resulted in a well-shaped autogenous crusher. The EP series VSI crusher is a 3rd or 4th stage crusher.
                </div>
                <a href="javascript:void(0)" class="gd_link gd_wrpr_full gd_txt_size_11 gd_line_height_11 gd_p_15 gd_bg_clr_white gd_border_1 border_clr_blue gd_border_radius_5 gd_p_10_lg gd_txt_align_center txt_clr_blue">View Detials</a>
            </div>
        </div>






        <div class="item gd_widget gd_widget_1 gd_p_5 gd_wrpr_inner gd_p_20 gd_p_10_lg gd_wrpr_full hover_trigger_widget">
            <div class="d_wrpr_full gd_border_1 border_clr_gray_01 gd_m_b_15 gd_img_block ">
                <img class="" src="images/product/5.jpg" alt="" class="gd_img">
            </div>
            <div class="gd_wrpr_full gd_txt_size_14 gd_line_height_20 gd_m_b_15 gd_txt_align_center gd_txt_weight_700 primary_font txt_clr_7 gd_txt_size_13_lg gd_line_height_13_lg">PAN FEEDER</div>
            <div class="gd_overlay overlay_bg_01 gd_txt_clr_white gd_element_hcenter gd_flex_column gd_p_25 hover_overlay gd_p_10_lg">
                <div class="gd_txt_size_16 gd_line_height_16 gd_txt_clr_white primary_font gd_txt_weight_700 gd_m_b_20 gd_m_b_15_lg gd_wrpr_full gd_txt_align_center gd_txt_size_14_lg gd_line_height_16_lg gd_m_b_10_lg">Vertical Shaft Impactor</div>
                <div class="gd_txt_size_14 gd_line_height_20 gd_txt_clr_white primary_font gd_txt_align_left gd_section_4 gd_m_b_15 gd_wrpr_full gd_m_b_15_lg gd_txt_size_13_lg gd_line_height_18_lg gd_wrpr_full gd_txt_align_center gd_txt_size_12_lg gd_line_height_16_lg para_height01 gd_txt_size_12_md gd_line_height_md_16">
                    We have been improving design on VSI for years as per our customer requirements. The customer satisfaction is our objective on product development. Our research and development activities have resulted in a well-shaped autogenous crusher. The EP series VSI crusher is a 3rd or 4th stage crusher. Our research and development activities have resulted in a well-shaped autogenous crusher. The EP series VSI crusher is a 3rd or 4th stage crusher.Our research and development activities have resulted in a well-shaped autogenous crusher. The EP series VSI crusher is a 3rd or 4th stage crusher.
                </div>
                <a href="javascript:void(0)" class="gd_link gd_wrpr_full gd_txt_size_11 gd_line_height_11 gd_p_15 gd_bg_clr_white gd_border_1 border_clr_blue gd_border_radius_5 gd_p_10_lg gd_txt_align_center txt_clr_blue">View Detials</a>
            </div>
        </div>


        <div class="item gd_widget gd_widget_1 gd_p_5 gd_wrpr_inner gd_p_20 gd_p_10_lg gd_wrpr_full hover_trigger_widget">
            <div class="gd_wrpr_full gd_border_1 border_clr_gray_01 gd_m_b_15 gd_img_block ">
                <img class="" src="images/product/4.jpg" alt="" class="gd_img">
            </div>
            <div class="gd_wrpr_full gd_txt_size_14 gd_line_height_20 gd_m_b_15 gd_txt_align_center gd_txt_weight_700 primary_font txt_clr_7 gd_txt_size_13_lg gd_line_height_13_lg">GRIZZLY FEEDER</div>
            <div class="gd_overlay overlay_bg_01 gd_txt_clr_white gd_element_hcenter gd_flex_column gd_p_25 hover_overlay gd_p_10_lg">
                <div class="gd_txt_size_16 gd_line_height_16 gd_txt_clr_white primary_font gd_txt_weight_700 gd_m_b_20 gd_m_b_15_lg gd_wrpr_full gd_txt_align_center gd_txt_size_14_lg gd_line_height_16_lg gd_m_b_10_lg gd_txt_size_14_lg gd_line_height_16_lg gd_m_b_10_lg">Vertical Shaft Impactor</div>
                <div class="gd_txt_size_14 gd_line_height_20 gd_txt_clr_white primary_font gd_txt_align_left gd_section_4 gd_m_b_15 gd_wrpr_full gd_m_b_15_lg gd_txt_size_13_lg gd_line_height_18_lg gd_wrpr_full gd_txt_align_center gd_txt_size_12_lg gd_line_height_16_lg para_height01 gd_txt_size_12_md gd_line_height_md_16 gd_txt_size_12_md gd_line_height_md_16">
                    We have been improving design on VSI for years as per our customer requirements. The customer satisfaction is our objective on product development. Our research and development activities have resulted in a well-shaped autogenous crusher. The EP series VSI crusher is a 3rd or 4th stage crusher. Our research and development activities have resulted in a well-shaped autogenous crusher. The EP series VSI crusher is a 3rd or 4th stage crusher.Our research and development activities have resulted in a well-shaped autogenous crusher. The EP series VSI crusher is a 3rd or 4th stage crusher.
                </div>
                <a href="javascript:void(0)" class="gd_link gd_wrpr_full gd_txt_size_11 gd_line_height_11 gd_p_15 gd_bg_clr_white gd_border_1 border_clr_blue gd_border_radius_5 gd_p_10_lg gd_txt_align_center txt_clr_blue">View Detials</a>
            </div>
        </div>


        


        
    </div>
    <div class="gd_slider_control_wrpr gd_slider_control_b gd_slider_control_c">
        <div class="gd_slider_control gd_slider_control_left gd_txt_size_25 gd_line_height_25 icon_color_01">
            <span class="gd_icon_arrow1_left_t"></span>
        </div>
        <div class="gd_slider_control gd_slider_control_right gd_txt_size_25 gd_line_height_25 icon_color_01">
            <span class="gd_icon_arrow1_right_t"></span>
        </div>
    </div>
</div>