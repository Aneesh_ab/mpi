<header class="gd_section gd_bg_clr_white header_fixing gd_zindex_100">
    <div class="gd_wrpr_outer gd_border_b_3 gd_txt_clr_lightpink">
        <div class="gd_wrpr_auto gd_p_tb_20 gd_p_lr_15 gd_element_center gd_cursor_pointer hangburger resp_append" data-hangburger="trigger" data-append-class="hangburger_wrpr">
            <span class="hangburger_line"></span>
            <span class="hangburger_line"></span>
            <span class="hangburger_line"></span>
        </div>

        <div class="gd_wrpr_auto gd_p_tb_10 gd_link gd_m_r_10 clone gd_m_r_5_xl gd_m_r_auto_lg" data-hangburger-append="1"  data-hangburger-clone="logo_wrpr_hamb">
            <a href="index.php" class="gd_link gd_wrpr_auto gd_element_hcenter_lg gd_justify_content_end_lg logo_width">
               <img src="images/header/logo.svg" alt="">
            </a>
            <!-- <a href="index.php" class="gl_link_fullwidth"></a> -->
            <!-- <div class="gd_wrpr_auto gd_element_hcenter_lg gd_justify_content_end_lg">
                
            </div> -->
        </div>

        <ul class="gd_wrpr gd_listgroup gd_element_vcenter gd_justify_content_end gd_m_r_30 gd_txt_size_13 primary_font gd_txt_weight_700 gd_position_static gd_txt_size_11_xl gd_flex_column_lg gd_hide_lg gd_listgroup_with_full_items_lg gd_txt_weight_400_lg gd_m_r_10_xl clone cutom_menu_size" data-hangburger-append="2" data-hangburger-clone="megamenu_cloning gd_wrpr_flex_auto">
            <li class="gd_listitem gd_p_lr_15 gd_position_static gd_element_vcenter gd_height_full gd_p_lr_10_xl gd_p_lr_0_lg gd_height_auto_lg gd_p_15_lg gd_position_relative_lg gd_display_block_lg">
                <div class="gd_wrpr_auto gd_wrpr_full_lg gd_position_static gd_wrpr_outer menu_title_wrpr">
                    <a href="index.php" class="gd_link gd_wrpr_auto gd_justify_content_between gd_text_icon meagamenu_head">
                        <div class="gd_wrpr_auto gd_element_vcenter text_clr_5 secondary_clr_hover">HOME</div>
                    </a>
                </div>
            </li>
            <li class="gd_listitem gd_p_lr_15 gd_position_static gd_element_vcenter gd_height_full gd_p_lr_10_xl gd_p_lr_0_lg gd_height_auto_lg gd_p_15_lg gd_position_relative_lg gd_display_block_lg megamenu_trigger">
                <div class="gd_wrpr_auto gd_wrpr_full_lg gd_position_static gd_wrpr_outer menu_title_wrpr">
                    <a href="services.php" class="gd_link gd_wrpr_auto gd_justify_content_between gd_text_icon meagamenu_head">
                        <div class="gd_wrpr_auto gd_element_vcenter text_clr_5 secondary_clr_hover">SERVICES</div>
                    </a>
                    <span class="gd_icon_down_f gd_m_l_5 leftnav_drop_click gd_element_vcenter_lg gd_justify_content_end_lg gd_position_absolute_lg gd_right_0_lg text_clr_7 gd_cursor_pointer"></span>
                    <div class="gd_wrpr_auto gd_position_absolute gd_bg_clr_white megamenu_inner">
                        <ul class="gd_wrpr_full gd_wrpr_outer gd_listgroup gd_flex_column gd_boxshadow">
                            <li>
                                <a href="projects.php" class="gd_wrpr_full gd_p_15 gd_link gd_element_vcenter text_clr_5 secondary_clr_hover">Projects </a>
                            </li>
                            <li>
                                <a href="lgs.php" class="gd_wrpr_full gd_p_15 gd_link gd_element_vcenter text_clr_5 secondary_clr_hover">LGS</a>
                            </li>
                             <li>
                                <a href="db.php" class="gd_wrpr_full gd_p_15 gd_link gd_element_vcenter text_clr_5 secondary_clr_hover">Design and build  </a>
                            </li>
                            <li>
                                <a href="vdc.php" class="gd_wrpr_full gd_p_15 gd_link gd_element_vcenter text_clr_5 secondary_clr_hover">Virtual design and construction</a>
                            </li>
                           
                            <li>
                                <a href="im.php" class="gd_wrpr_full gd_p_15 gd_link gd_element_vcenter text_clr_5 secondary_clr_hover">Integrating mep</a>
                            </li>


                        </ul>
                    </div>
                </div>
            </li>
             <li class="gd_listitem gd_p_lr_15 gd_position_static gd_element_vcenter gd_height_full gd_p_lr_10_xl gd_p_lr_0_lg gd_height_auto_lg gd_p_15_lg gd_position_relative_lg gd_display_block_lg megamenu_trigger">
                <div class="gd_wrpr_auto gd_wrpr_full_lg gd_position_static gd_wrpr_outer menu_title_wrpr">
                    <a href="about.php" class="gd_link gd_wrpr_auto gd_justify_content_between gd_text_icon meagamenu_head">
                        <div class="gd_wrpr_auto gd_element_vcenter text_clr_5 secondary_clr_hover">ABOUT US</div>
                    </a>
                    <span class="gd_icon_down_f gd_m_l_5 leftnav_drop_click gd_element_vcenter_lg gd_justify_content_end_lg gd_position_absolute_lg gd_right_0_lg text_clr_7 gd_cursor_pointer"></span>
                    <div class="gd_wrpr_auto gd_position_absolute gd_bg_clr_white megamenu_inner">
                        <ul class="gd_wrpr_full gd_wrpr_outer gd_listgroup gd_flex_column gd_boxshadow">
                            <li>
                                <a href="team.php" class="gd_wrpr_full gd_p_15 gd_link gd_element_vcenter text_clr_5 secondary_clr_hover">Meet the team</a>
                            </li>
                            <li>
                                <a href="mission_vision.php" class="gd_wrpr_full gd_p_15 gd_link gd_element_vcenter text_clr_5 secondary_clr_hover">Mission, Vision</a>
                            </li>
                            <li>
                                <a href="core_value.php" class="gd_wrpr_full gd_p_15 gd_link gd_element_vcenter text_clr_5 secondary_clr_hover">Core values</a>
                            </li>
                            <li>
                                <a href="javascript:void(0)" class="gd_wrpr_full gd_p_15 gd_link gd_element_vcenter text_clr_5 secondary_clr_hover">QHSE Commitment </a>
                            </li>
                            <li>
                                <a href="testimonials.php" class="gd_wrpr_full gd_p_15 gd_link gd_element_vcenter text_clr_5 secondary_clr_hover">Testimonial</a>
                            </li>


                        </ul>
                    </div>
                </div>
            </li>
            <li class="gd_listitem gd_p_lr_15 gd_position_static gd_element_vcenter gd_height_full gd_p_lr_10_xl gd_p_lr_0_lg gd_height_auto_lg gd_p_15_lg gd_position_relative_lg gd_display_block_lg">
                <div class="gd_wrpr_auto gd_wrpr_full_lg gd_position_static gd_wrpr_outer menu_title_wrpr">
                    <a href="contactus.php" class="gd_link gd_wrpr_auto gd_justify_content_between gd_text_icon meagamenu_head">
                        <div class="gd_wrpr_auto gd_element_vcenter text_clr_5 secondary_clr_hover">CONTACT US</div>
                    </a>
                </div>
            </li>
            <li class="gd_listitem gd_p_lr_15 gd_position_static gd_element_vcenter gd_height_full gd_p_lr_10_xl gd_p_lr_0_lg gd_height_auto_lg gd_p_15_lg gd_position_relative_lg gd_display_block_lg">
                <div class="gd_wrpr_auto gd_wrpr_full_lg gd_position_static gd_wrpr_outer menu_title_wrpr">
                    <a href="login.php" class="gd_link gd_wrpr_auto gd_justify_content_between gd_text_icon meagamenu_head">
                        <div class="gd_wrpr_auto gd_element_vcenter text_clr_5 secondary_clr_hover">EMPLOYEE LOGIN</div>
                    </a>
                </div>
            </li>
           
            
        </ul>
        
        <!-- <div class="gd_wrpr_auto gd_element_hcenter gd_hide_xs resp_append gd_zindex_50 gd_wrpr_full_xs clone" data-hangburger-append="4" data-hangburger-clone="tool_call">

            <div class="gd_link gd_element_center gd_m_r_30 gd_listitem gd_m_r_15_lg gd_m_r_0_xs gd_p_tb_5_xs  primary_clr_hover gd_m_r_15_xl quick_call text_color_new">
                <a href="tel:+96824128989" class="gl_link_fullwidth" target="_self"></a>
                <div class="gd_icon gd_m_r_10 gd_txt_size_14_xl secondary_clr custom_border_rad_50">
                    <i class="gd_icon_baselinecall_f"></i>
                </div>
                <div class="gd_h_wrpr gd_txt_size_14 gd_txt_weight_700 gd_txt_size_12_lg gd_txt_size_11_xs primary_font gd_txt_size_11_xl">
                    <span class="gd_h_sub gd_m_b_5 gd_txt_align_left">Contact support</span>
                    <div class="gd_h_main">+968 2412 8989</div>
                </div>
            </div>

        </div> -->                            
        <!-- <div class="gd_wrpr_auto gd_element_hcenter gd_hide_xs resp_append gd_zindex_50 gd_wrpr_full_xs clone" data-hangburger-append="5" data-hangburger-clone="tool_call">

            <div class="gd_link gd_element_center gd_m_r_30 gd_listitem gd_m_r_15_lg gd_m_r_0_xs gd_p_tb_5_xs  primary_clr_hover gd_m_r_15_xl quick_call text_color_new">
                <a href="mailto:info@mpioman.com" class="gl_link_fullwidth" target="_self"></a>
                <div class="gd_icon gd_m_r_10 gd_txt_size_14_xl secondary_clr custom_border_rad_50">
                    <i class="gd_icon_mail_t"></i>
                </div>
                <div class="gd_h_wrpr gd_txt_size_14 gd_txt_weight_700 gd_txt_size_12_lg gd_txt_size_11_xs primary_font gd_txt_size_11_xl">
                    <span class="gd_h_sub gd_m_b_5 gd_txt_align_left">Contact support</span>
                    <div class="gd_h_main">info@mpioman.com</div>
                </div>
            </div>

        </div> -->


        <!-- <a href="javascript:void(0)" class="gd_wrpr_auto gd_p_lr_30 gd_element_vcenter text_clr_0 gd_txt_size_14 gd_txt_weight_700 primary_bg_clr secondary_bg_hover primary_font gd_p_lr_15_xl gd_txt_size_10_xl gd_p_lr_25_lg gd_p_tb_20_lg clone cutom_blog" data-hangburger-append="3" data-hangburger-clone="hamb_blog">
            BLOG
        </a> -->


    </div>
</header>























 <!-- megamenu_trigger
 megamenu_trigger
 megamenu_trigger
 megamenu_trigger
 megamenu_trigger
 megamenu_trigger
 megamenu_trigger -->