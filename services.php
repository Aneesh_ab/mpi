<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">

    <title>MPI Construction </title>

    <?php include 'include/link.php';?>

</head>
<body>

    <?php include 'include/header.php';?>



    <section class="gd_section">
        
        <!--theme1-COMMON_WRAPPER_1-Wrapper open-->
        <div class="gd_wrpr_full structurebox">
            

        <div class="gd_wrpr_inner gd_overlay gd_overflow_hidden">
            <div class="gd_wrpr_full gd_wrpr_inner gd_bg_img">
                <!-- 1366x550 manipulation -->
                <img src="images/services/01.jpg" alt="" title="">
            </div>
        </div>
        <div class="gd_wrpr_outer">
            <div class="gd_wrpr_full primary_font  gd_txt_clr_white gd_height_full gd_element_hcenter gd_flex_column gd_justify_content_center gd_m_l_0_lg gd_p_15 gd_align_items_center gd_p_tb_100 gd_p_lr_50 gd_p_tb_50_xl gd_p_tb_35_md gd_p_tb_25_xs  gd_p_lr_15_lg black_overlay">
                <div class="gd_wrpr_full gd_txt_align_left gd_txt_size_60 color_secondory gd_line_height_60 gd_txt_size_50_lg gd_line_height_50_lg gd_txt_size_35_xs gd_line_height_35_xs text_amin_1 gd_txt_weight_700 gd_txt_align_center gd_p_tb_100 gd_p_tb_50_xl gd_p_tb_35_md gd_p_tb_25_xs">
                    SERVICES
                </div>
            </div>
            <div class="gd_overlay gd_wrpr_full gd_height_full line_animation_wrpr style_04">
                <div class="line_01"></div>
                <div class="line_02"></div>
                <div class="line_03"></div>
                <div class="line_04"></div>
                <div class="line_05"></div>
                <div class="line_06"></div>
                <div class="dote_01"></div>
                <div class="circle_01" id="scene"><div class="inner" data-depth="0.5"></div></div>
                <div class="dote_02"></div>
            </div>
        </div>
    


        </div>

    </section>



    <section class="gd_section">
        <div class="gd_wrpr_outer gd_p_tb_70 gd_p_lr_50 gd_p_tb_50_xl gd_p_lr_25_xl gd_p_tb_20_lg gd_p_lr_15_lg">
            
            <!--theme1-COMMON_WRAPPER_1-Wrapper open-->
            <div class="gd_wrpr_full structurebox">

                <div class="gd_wrpr_outer gd_wrpr_full gd_element_hcenter">
                    
                    <div class="gd_wrpr_4 gd_wrpr_6_xs gd_p_10 gd_p_5_xs">
                        <div class="gd_wrpr_inner height_fixed">


                            
                                <div class="gd_widget gd_widget_1">
                                    
                                   <!--theme1-COMMON_WRAPPER_2-Wrapper open-->
                                    <div class="gd_wrpr_full gd_bg_img">
                                       <img src="images/services/02.jpg" alt="" class="gd_bg_img"> 
                                    </div>

                                    <div class="gd_overlay color_secondory overlay_bg_fade overlay_anim gd_overflow_hidden">

                                        <a href="javasript:void(0)" class="gl_link_fullwidth"></a>

                                        
                                        <!-- YELLOW_OVERLAY_WRAPPER[Client Requirement] -->
                                        <div class="overlay">
                                        </div>
                                        
                                         <!--theme1-COMMON_WRAPPER_3-Wrapper open-->
                                        <div class="gd_h_wrpr gd_txt_size_30 gd_line_height_30 gd_txt_size_25_xl gd_border_7  gd_border_5_lg gd_txt_size_20_lg gd_line_height_20_lg gd_border_3_md gd_txt_size_15_md gd_line_height_16_md common_height gd_element_vcenter gd_element_hcenter gd_txt_weight_700">
                                            
                                                LIGHT GAUGE <br> 
                                                STEEL
                                            
                                           
                                        </div>


                                    </div>



                                </div>
                            

                        </div>
                    </div>



                    <div class="gd_wrpr_4 gd_wrpr_6_xs gd_p_10 gd_p_5_xs">
                        <div class="gd_wrpr_inner height_fixed">


                            
                                <div class="gd_widget gd_widget_1">

                                    <div class="gd_wrpr_full gd_bg_img">
                                       <img src="images/services/03.jpg" alt="" class="gd_bg_img"> 
                                    </div>

                                    <div class="gd_overlay color_secondory overlay_bg_fade overlay_anim gd_overflow_hidden">

                                        <a href="javasript:void(0)" class="gl_link_fullwidth"></a>


                                        <div class="overlay">
                                        </div>
                                        

                                        <div class="gd_h_wrpr gd_txt_size_30 gd_line_height_30 gd_txt_size_25_xl gd_border_7  gd_border_5_lg gd_txt_size_20_lg gd_line_height_20_lg gd_border_3_md gd_txt_size_15_md gd_line_height_16_md common_height gd_element_vcenter gd_element_hcenter gd_txt_weight_700">
                                            
                                                DESIGN & BUILD
                                            
                                           
                                        </div>
                                    </div>
                                </div>
                            

                        </div>
                    </div>


                    <div class="gd_wrpr_4 gd_wrpr_6_xs gd_p_10 gd_p_5_xs">
                        <div class="gd_wrpr_inner height_fixed">


                            
                                <div class="gd_widget gd_widget_1">


                                    <div class="gd_wrpr_full gd_bg_img">
                                       <img src="images/services/04.jpg" alt="" class="gd_bg_img"> 
                                    </div>
                                    

                                    <div class="gd_overlay color_secondory overlay_bg_fade overlay_anim gd_overflow_hidden">

                                        <a href="javasript:void(0)" class="gl_link_fullwidth"></a>


                                        <div class="overlay">
                                        </div>
                                        

                                        <div class="gd_h_wrpr gd_txt_size_30 gd_line_height_30 gd_txt_size_25_xl gd_border_7  gd_border_5_lg gd_txt_size_20_lg gd_line_height_20_lg gd_border_3_md gd_txt_size_15_md gd_line_height_16_md common_height gd_element_vcenter gd_element_hcenter gd_txt_weight_700">
                                            
                                               PROJECTS
                                          
                                           
                                        </div>
                                    </div>
                                </div>
                            

                        </div>
                    </div>


                    <div class="gd_wrpr_4 gd_wrpr_6_xs gd_p_10 gd_p_5_xs">
                        <div class="gd_wrpr_inner height_fixed">


                            
                                <div class="gd_widget gd_widget_1">

                                   <div class="gd_wrpr_full gd_bg_img">
                                       <img src="images/services/05.jpg" alt="" class="gd_bg_img"> 
                                    </div>

                                    <div class="gd_overlay color_secondory overlay_bg_fade overlay_anim gd_overflow_hidden">

                                        <a href="javasript:void(0)" class="gl_link_fullwidth"></a>


                                        <div class="overlay">
                                            
                                        </div>
                                        

                                        <div class="gd_h_wrpr gd_txt_size_30 gd_line_height_30 gd_txt_size_25_xl gd_border_7  gd_border_5_lg gd_txt_size_20_lg gd_line_height_20_lg gd_border_3_md gd_txt_size_15_md gd_line_height_16_md common_height gd_element_vcenter gd_element_hcenter gd_txt_weight_700">
                                            
                                               VIRTUAL <br>
                                                DESIGN AND <br>
                                                CONSTRUCTION
                                            
                                           
                                        </div>
                                    </div>
                                </div>
                            

                        </div>
                    </div>


                    <div class="gd_wrpr_4 gd_wrpr_6_xs gd_p_10 gd_p_5_xs">
                        <div class="gd_wrpr_inner height_fixed">


                            
                                <div class="gd_widget gd_widget_1">

                                    <div class="gd_wrpr_full gd_bg_img">
                                       <img src="images/services/06.jpg" alt="" class="gd_bg_img"> 
                                    </div>

                                    <div class="gd_overlay color_secondory overlay_bg_fade overlay_anim gd_overflow_hidden">

                                        <a href="javasript:void(0)" class="gl_link_fullwidth"></a>


                                        <div class="overlay">
                                            
                                        </div>
                                        

                                        <div class="gd_h_wrpr gd_txt_size_30 gd_line_height_30 gd_border_7  gd_border_5_lg gd_txt_size_20_lg gd_line_height_20_lg gd_border_3_md gd_txt_size_15_sm gd_line_height_16_sm common_height gd_element_vcenter gd_element_hcenter gd_txt_weight_700">
                                            
                                                INTEGRATING MEP
                                            
                                           
                                        </div>
                                    </div>
                                </div>
                            

                        </div>
                    </div>


                    


                    


                    



                    
                </div>


            </div>
            

        </div>        
    </section>




    








    <?php /* /;?>
    
    <?php include 'include/banner_new.php'; ?>

    <?php include 'include/what.php'; ?>

    <?php include 'include/virtual_design.php';?>

    <?php include 'include/what_new.php';?>

    <?php include 'include/our_acheivement.php';?>
    
    <?php /* */;?>
    



    <?php include 'include/footer.php';?>

    <?php include 'include/script.php';?>

</body>
</html>