<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">

    <title>MPI Construction </title>

    <?php include 'include/link.php';?>

</head>
<body>

    <?php include 'include/header.php';?>



    <section class="gd_section">

        
        <!--theme1-COMMON_WRAPPER_1-Wrapper open-->
        <div class="gd_wrpr_full structurebox">
            
        <div class="gd_wrpr_inner gd_overlay gd_overflow_hidden">
            <div class="gd_wrpr_full gd_wrpr_inner gd_bg_img">
                <!-- 1366x550 manipulation -->
                <img src="images/banner/common_banner_bg_04.jpg">
            </div>
        </div>
        <div class="gd_wrpr_outer">
            <div class="gd_wrpr_full primary_font  gd_txt_clr_white gd_height_full gd_element_hcenter gd_flex_column gd_justify_content_center gd_m_l_0_lg gd_p_15 gd_align_items_center gd_p_tb_100 gd_p_lr_50 gd_p_tb_50_xl gd_p_tb_35_md gd_p_tb_25_xs  gd_p_lr_15_lg black_overlay">
                <h1 class="gd_wrpr_full gd_txt_align_left gd_txt_size_60 color_secondory gd_line_height_60 gd_txt_size_50_lg gd_line_height_50_lg gd_txt_size_35_xs gd_line_height_35_xs text_amin_1 gd_txt_weight_700 gd_txt_align_center gd_p_tb_100 gd_p_tb_50_xl gd_p_tb_35_md gd_p_tb_25_xs">
                    TESTIMONIAL
                </h1>
            </div>
            <div class="gd_overlay gd_wrpr_full gd_height_full line_animation_wrpr style_04">
                <div class="line_01"></div>
                <div class="line_02"></div>
                <div class="line_03"></div>
                <div class="line_04"></div>
                <div class="line_05"></div>
                <div class="line_06"></div>
                <div class="dote_01"></div>
                <div class="circle_01" id="scene"><div class="inner" data-depth="0.5"></div></div>
                <div class="dote_02"></div>
            </div>
        </div>
    
        </div>
    </section>



    <section class="gd_section">
        <div class="gd_wrpr_outer gd_p_tb_70 gd_p_lr_50 gd_p_tb_50_xl gd_p_lr_25_xl gd_p_tb_20_lg gd_p_lr_15_lg bg_clr_fade_yellow">


            
            
            <!--theme1-COMMON_WRAPPER_1-Wrapper open-->
            <div class="gd_wrpr_full structurebox">
                <div class="gd_wrpr_outer gd_wrpr_full">
                    
                    <div class="gd_wrpr_full">



                <div class="gd_wrpr_full gd_txt_size_20 gd_line_height_20 gd_txt_weight_700 primary_font gd_m_b_15 gd_txt_size_18_lg gd_line_height_18_lg text_clr_0 ">Mr Tim Murray</div>
            

                <div class="gd_wrpr_full gd_txt_size_20 gd_line_height_20 gd_txt_weight_400 primary_font gd_m_b_30 gd_txt_size_18_lg gd_line_height_18_lg text_clr_0 txt_italic_01">
                    CEO - Murray Architects
                </div>

                <div class="custom_line_01 gd_m_b_30"></div>

                <div class="gd_para gd_txt_size_16 gd_line_height_25 gd_txt_size_14_lg gd_line_height_20_lg txt_clr_0 primary_font gd_txt_align_left gd_section_4 gd_m_b_30 gd_wrpr_full gd_m_b_15_lg gd_txt_size_13_lg gd_line_height_18_lg text_clr_0">
                    Oman-based civil and construction engineering company, MPI Construction (MPI), is taking a number of measures to protect their workers on a construction site and in the offices to combat the threat of COVID-19.Oman-based civil and construction engineering company, MPI Construction (MPI), is taking a number of measures to protect their workers on a construction site and in the offices to combat the threat of COVID-19.Oman-based civil and construction engineering company, MPI Construction (MPI), is taking a number of measures to protect their workers on a construction site and in the offices to combat the threat of COVID-19.Oman-based civil and construction engineering <br><br>
                    company, MPI Construction (MPI), is taking a number of measures to protect their workers on a construction site and in the offices to combat the threat of COVID-19.Oman-based civil and construction engineering company, MPI Construction (MPI), is taking a number of measures to protect their workers on a construction site and in the offices to combat the threat of COVID-19. 

                    Oman-based civil and construction engineering company, MPI Construction (MPI), is taking a number of measures to protect their workers on a construction site and in the offices to combat the threat of COVID-19.Oman-based civil and construction engineering company, MPI Construction (MPI), is taking a number of measures to protect their workers on a construction site and in the offices to combat the threat of COVID-19.Oman-based civil and construction engineering company, MPI Construction (MPI), is taking a number of measures to protect their workers on a construction site and in the offices to combat the threat of COVID-19.Oman-based civil and construction engineering
                    company, MPI Construction (MPI), is taking a number of measures to protect their workers on a construction site and in the offices to combat the threat of COVID-19.
                </div>
        
                        


                    </div>

                </div>
            </div>
            
            

        </div>        
    </section>





    <section class="gd_section">
        <div class="gd_wrpr_outer gd_p_tb_70 gd_p_lr_50 gd_p_tb_50_xl gd_p_lr_25_xl gd_p_tb_20_lg gd_p_lr_15_lg">


            
            
            <!--theme1-COMMON_WRAPPER_1-Wrapper open-->
            <div class="gd_wrpr_full structurebox">
                <div class="gd_wrpr_outer gd_wrpr_full">
                    
                    <div class="gd_wrpr_full gd_m_b_30">


                        
                <div class="gd_wrpr_full gd_txt_size_20 gd_line_height_20 gd_txt_weight_700 primary_font gd_m_b_15 gd_txt_size_18_lg gd_line_height_18_lg text_clr_0 ">Mr John Cena</div>
            

                <div class="gd_wrpr_full gd_txt_size_20 gd_line_height_20 gd_txt_weight_400 primary_font gd_m_b_30 gd_txt_size_18_lg gd_line_height_18_lg text_clr_0 txt_italic_01">
                   CEO - Indus 
                </div>

                <div class="custom_line_01 gd_m_b_30"></div>

                <div class="gd_para gd_txt_size_16 gd_line_height_25 gd_txt_size_14_lg gd_line_height_20_lg txt_clr_0 primary_font gd_txt_align_left gd_section_4 gd_m_b_30 gd_wrpr_full gd_m_b_15_lg gd_txt_size_13_lg gd_line_height_18_lg text_clr_0">
                    Oman-based civil and construction engineering company, MPI Construction (MPI), is taking a number of measures to protect their workers on a construction site and in the offices to combat the threat of COVID-19.Oman-based civil and construction engineering company, MPI Construction (MPI), is taking a number of measures to protect their workers on a construction site and in the offices to combat the threat of COVID-19.Oman-based civil and construction engineering company, MPI Construction (MPI), is taking a number of measures to protect

                </div>
        
                        


                    </div>




                    <div class="gd_wrpr_full gd_m_b_30">


                        
                <div class="gd_wrpr_full gd_txt_size_20 gd_line_height_20 gd_txt_weight_700 primary_font gd_m_b_15 gd_txt_size_18_lg gd_line_height_18_lg text_clr_0 ">Mr Nakul Verma</div>
            

                <div class="gd_wrpr_full gd_txt_size_20 gd_line_height_20 gd_txt_weight_400 primary_font gd_m_b_30 gd_txt_size_18_lg gd_line_height_18_lg text_clr_0 txt_italic_01">
                    MD - Zubair Construction
                </div>

                <div class="custom_line_01 gd_m_b_30"></div>

                <div class="gd_para gd_txt_size_16 gd_line_height_25 gd_txt_size_14_lg gd_line_height_20_lg txt_clr_0 primary_font gd_txt_align_left gd_section_4 gd_m_b_30 gd_wrpr_full gd_m_b_15_lg gd_txt_size_13_lg gd_line_height_18_lg text_clr_0">
                    Oman-based civil and construction engineering company, MPI Construction (MPI), is taking a number of measures to protect their workers on a construction site and in the offices to combat the threat of COVID-19.Oman-based civil and construction engineering company, MPI
                </div>
        
                        


                    </div>




                    <div class="gd_wrpr_full gd_m_b_30">


                        
                <div class="gd_wrpr_full gd_txt_size_20 gd_line_height_20 gd_txt_weight_700 primary_font gd_m_b_15 gd_txt_size_18_lg gd_line_height_18_lg text_clr_0 ">Mr Kim Yong</div>
            

                <div class="gd_wrpr_full gd_txt_size_20 gd_line_height_20 gd_txt_weight_400 primary_font gd_m_b_30 gd_txt_size_18_lg gd_line_height_18_lg text_clr_0 txt_italic_01">
                    CEO - Yong
                </div>

                <div class="custom_line_01 gd_m_b_30"></div>

                <div class="gd_para gd_txt_size_16 gd_line_height_25 gd_txt_size_14_lg gd_line_height_20_lg txt_clr_0 primary_font gd_txt_align_left gd_section_4 gd_m_b_30 gd_wrpr_full gd_m_b_15_lg gd_txt_size_13_lg gd_line_height_18_lg text_clr_0">
                    Oman-based civil and construction engineering company, MPI Construction (MPI), is taking a number of measures to protect their workers on a construction site and in the offices to combat the threat of COVID-19.Oman-based civil and construction engineering company, MPI Construction (MPI), is taking a number of measures to protect their workers on a construction site and in the offices to combat the threat of COVID-19.Oman-based civil and construction engineering company, MPI Construction (MPI), is taking a number of measures to protect their workers on a construction site and in the offices to combat the threat of COVID-19.Oman-based civil and construction engineering
company, MPI Construction (MPI), is taking a number of measures to protect their workers on a construction site and in the offices to
                </div>
        
                        


                    </div>

                </div>
            </div>
            
            

        </div>        
    </section>



    




    








    <?php /* /;?>
    
    <?php include 'include/banner_new.php'; ?>

    <?php include 'include/what.php'; ?>

    <?php include 'include/virtual_design.php';?>

    <?php include 'include/what_new.php';?>

    <?php include 'include/our_acheivement.php';?>
    
    <?php /* */;?>
    



    <?php include 'include/footer.php';?>

    <?php include 'include/script.php';?>

</body>
</html>