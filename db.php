<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">

    <title>MPI Construction </title>

    <?php include 'include/link.php';?>

</head>
<body>

    <?php include 'include/header.php';?>



    <section class="gd_section">


        
        <!--theme1-COMMON_WRAPPER_1-Wrapper open-->
        <div class="gd_wrpr_full structurebox">
            
        <div class="gd_wrpr_inner gd_overlay gd_overflow_hidden">
            <div class="gd_wrpr_full gd_wrpr_inner gd_bg_img">
                <!-- 1366x550 manipulation -->
                <img src="images/banner/common_banner_bg1.jpg">
            </div>
        </div>
        <div class="gd_wrpr_outer">
            <div class="gd_wrpr_full primary_font  gd_txt_clr_white gd_height_full gd_element_hcenter gd_flex_column gd_justify_content_center gd_m_l_0_lg gd_p_15 gd_align_items_center gd_p_tb_100 gd_p_lr_50 gd_p_tb_50_xl gd_p_tb_35_md gd_p_tb_25_xs  gd_p_lr_15_lg black_overlay">
                <h1 class="gd_wrpr_full gd_txt_align_left gd_txt_size_60 color_secondory gd_line_height_60 gd_txt_size_50_lg gd_line_height_50_lg gd_txt_size_35_xs gd_line_height_35_xs text_amin_1 gd_txt_weight_700 gd_txt_align_center gd_p_tb_100 gd_p_tb_50_xl gd_p_tb_35_md gd_p_tb_25_xs">
                    DESIGN & BUILD
                </h1>
            </div>
            <div class="gd_overlay gd_wrpr_full gd_height_full line_animation_wrpr style_04">
                <div class="line_01"></div>
                <div class="line_02"></div>
                <div class="line_03"></div>
                <div class="line_04"></div>
                <div class="line_05"></div>
                <div class="line_06"></div>
                <div class="dote_01"></div>
                <div class="circle_01" id="scene"><div class="inner" data-depth="0.5"></div></div>
                <div class="dote_02"></div>
            </div>
        </div>
    
        </div>
    </section>



    <section class="gd_section">
        <div class="gd_wrpr_outer gd_p_tb_70 gd_p_lr_50 gd_p_tb_50_xl gd_p_lr_25_xl gd_p_tb_20_lg gd_p_lr_15_lg">
            
            <!--theme1-COMMON_WRAPPER_1-Wrapper open-->
            <div class="gd_wrpr_full structurebox">
                <div class="gd_wrpr_outer">

                     <!--theme1-COMMON_WRAPPER_4-Wrapper open-->
                    <div class="gd_para gd_txt_size_16 gd_line_height_25 gd_txt_size_14_lg gd_line_height_20_lg txt_clr_0 primary_font gd_txt_align_left gd_section_4 gd_m_b_30 gd_wrpr_full gd_m_b_15_lg gd_txt_size_13_lg gd_line_height_18_lg text_clr_0">
                        MPI with our design partner, offers a Design and Build solution to our clients, our preferred model for meeting the needs of both commercial and residential clients.  We provide a complete service, encompassing all the design and construction aspects of a build. Our Professional delivery ensures
                    </div>

                     <!--theme1-COMMON_WRAPPER_5-Wrapper open-->
                    <!-- <a href="javascript:void(0)" class="gd_link primary_font gd_txt_size_18 gd_line_height_18 gd_txt_weight_500 text_clr_0 gd_p_tb_15 gd_p_lr_20 gd_p_tb_10_xs gd_txt_size_16_lg gd_line_height_16_lg gd_txt_size_15_sm gd_line_height_15_sm gd_m_b_50 gd_m_b_35_xl gd_m_b_25_lg gd_m_b_20_xs btn_style_02 secondary_bg_clr primary_bg_hover">Click Here to download brochure</a> -->

                     <!--theme1-COMMON_WRAPPER_6-Wrapper open-->
                    <!-- <div class="gd_wrpr_full gd_txt_size_30 gd_line_height_30 gd_txt_weight_500 gd_m_b_30 gd_m_b_25_xl gd_m_b_20_lg gd_m_b_15_xs gd_txt_size_25_lg gd_line_height_28_lg gd_txt_size_20_sm gd_line_height_25_sm gd_txt_size_18_xs gd_line_height_22_xs primary_font txt_clr_4">
                        The advantages of using LGS as compared to traditional RCC structures are:
                    </div> -->


                    <!--theme1-COMMON_WRAPPER_1-Wrapper open-->
                    <div class="gd_wrpr_full structurebox">

                        <ul class="gd_wrpr_full gd_listgroup gd_listgroup_block gd_flex_column gd_align_items_start gd_m_b_25 gd_m_b_15_lg">
                        <li class="gd_listitem gd_listitem_block gd_m_b_25 gd_m_b_15_lg">
                            <div class="gd_wrpr_full gd_wrpr_outer gd_element_vcenter gd_txt_size_25 gd_line_height_25 gd_txt_weight_600 gd_txt_size_20_xl gd_line_height_22_xl gd_txt_size_18_lg gd_line_height_25_lg gd_txt_size_16_sm gd_line_height_22_sm gd_txt_size_14_xs gd_line_height_20_xs text_clr_7">
                                <div class="gd_wrpr_auto gd_m_r_30 gd_element_vcenter gd_m_r_15_xl gd_m_r_10_xs">
                                    <img src="images/detail/detail_list_mark.svg" alt="">
                                </div>
                                <a href="index.php" class="gd_link gd_wrpr_auto flex_tip">
                                    Single point of Responsibility
                                </a>
                            </div>
                        </li>
                        <li class="gd_listitem gd_listitem_block gd_m_b_25 gd_m_b_15_lg">
                            <div class="gd_wrpr_full gd_wrpr_outer gd_element_vcenter gd_txt_size_25 gd_line_height_25 gd_txt_weight_600 gd_txt_size_20_xl gd_line_height_22_xl gd_txt_size_18_lg gd_line_height_25_lg gd_txt_size_16_sm gd_line_height_22_sm gd_txt_size_14_xs gd_line_height_20_xs text_clr_7">
                                <div class="gd_wrpr_auto gd_m_r_30 gd_element_vcenter gd_m_r_15_xl gd_m_r_10_xs">
                                    <img src="images/detail/detail_list_mark.svg" alt="">
                                </div>
                                <a href="index.php" class="gd_link gd_wrpr_auto flex_tip">
                                    Total Accountability
                                </a>
                            </div>
                        </li>
                        <li class="gd_listitem gd_listitem_block gd_m_b_25 gd_m_b_15_lg">
                            <div class="gd_wrpr_full gd_wrpr_outer gd_element_vcenter gd_txt_size_25 gd_line_height_25 gd_txt_weight_600 gd_txt_size_20_xl gd_line_height_22_xl gd_txt_size_18_lg gd_line_height_25_lg gd_txt_size_16_sm gd_line_height_22_sm gd_txt_size_14_xs gd_line_height_20_xs text_clr_7">
                                <div class="gd_wrpr_auto gd_m_r_30 gd_element_vcenter gd_m_r_15_xl gd_m_r_10_xs">
                                    <img src="images/detail/detail_list_mark.svg" alt="">
                                </div>
                                <a href="index.php" class="gd_link gd_wrpr_auto flex_tip">
                                    Fast and cost-effective Project Delivery
                                </a>
                            </div>
                        </li>
                        <li class="gd_listitem gd_listitem_block gd_m_b_25 gd_m_b_15_lg">
                            <div class="gd_wrpr_full gd_wrpr_outer gd_element_vcenter gd_txt_size_25 gd_line_height_25 gd_txt_weight_600 gd_txt_size_20_xl gd_line_height_22_xl gd_txt_size_18_lg gd_line_height_25_lg gd_txt_size_16_sm gd_line_height_22_sm gd_txt_size_14_xs gd_line_height_20_xs text_clr_7">
                                <div class="gd_wrpr_auto gd_m_r_30 gd_element_vcenter gd_m_r_15_xl gd_m_r_10_xs">
                                    <img src="images/detail/detail_list_mark.svg" alt="">
                                </div>
                                <a href="index.php" class="gd_link gd_wrpr_auto flex_tip">
                                   Collaboration with client to promote a One-Team culture
                                </a>
                            </div>
                        </li>
                        <li class="gd_listitem gd_listitem_block gd_m_b_25 gd_m_b_15_lg">
                            <div class="gd_wrpr_full gd_wrpr_outer gd_element_vcenter gd_txt_size_25 gd_line_height_25 gd_txt_weight_600 gd_txt_size_20_xl gd_line_height_22_xl gd_txt_size_18_lg gd_line_height_25_lg gd_txt_size_16_sm gd_line_height_22_sm gd_txt_size_14_xs gd_line_height_20_xs text_clr_7">
                                <div class="gd_wrpr_auto gd_m_r_30 gd_element_vcenter gd_m_r_15_xl gd_m_r_10_xs">
                                    <img src="images/detail/detail_list_mark.svg" alt="">
                                </div>
                                <a href="index.php" class="gd_link gd_wrpr_auto flex_tip">
                                   Open and Transparent Communication
                                </a>
                            </div>
                        </li>
                        <li class="gd_listitem gd_listitem_block gd_m_b_25 gd_m_b_15_lg">
                            <div class="gd_wrpr_full gd_wrpr_outer gd_element_vcenter gd_txt_size_25 gd_line_height_25 gd_txt_weight_600 gd_txt_size_20_xl gd_line_height_22_xl gd_txt_size_18_lg gd_line_height_25_lg gd_txt_size_16_sm gd_line_height_22_sm gd_txt_size_14_xs gd_line_height_20_xs text_clr_7">
                                <div class="gd_wrpr_auto gd_m_r_30 gd_element_vcenter gd_m_r_15_xl gd_m_r_10_xs">
                                    <img src="images/detail/detail_list_mark.svg" alt="">
                                </div>
                                <a href="index.php" class="gd_link gd_wrpr_auto flex_tip">
                                   Elimination of Adversarial conditions
                                </a>
                            </div>
                        </li>
                    </ul>

                    </div>

                   <!--theme1-COMMON_WRAPPER_1-Wrapper open-->
                    <!-- <div class="gd_wrpr_full structurebox">


                        <div class="gd_wrpr_outer">
                            <div class="gd_wrpr_6 gd_wrpr_12_sm">
                                <div class="gd_wrpr_outer">
                                    <div class="gd_wrpr_full gd_widget gd_widget_1 hover_img_effect">
                                        <div class="gd_wrpr_full gd_bg_img">
                                            <img src="images/detail/description_video_bg.jpg" alt="" class="hover_img">
                                        </div>
                                        <div class="gd_overlay gd_txt_clr_white">
                                            <div class="gd_wrpr_full gd_wrpr_inner gd_element_center gd_txt_size_100 gd_line_height_100 gd_txt_size_80_lg gd_line_height_80_lg gd_txt_size_60_sm gd_line_height_60_sm gd_txt_size_50_xs gd_line_height_50_xs black_overlay">
                                                <a href="javascript:void(0)" class="gd_link">
                                                    <span class="gd_icon_video"></span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="gd_wrpr_6 gd_wrpr_12_sm">
                                <div class="gd_wrpr_inner gd_p_l_60 gd_p_t_15 gd_p_b_65 gd_p_l_45_xl gd_p_t_b_15_xl gd_p_lr_15_md gd_p_tb_0_md gd_p_t_15_sm gd_p_b_5_sm gd_p_lr_0_sm">
                                    <div class="gd_wrpr_full gd_txt_size_50 gd_line_height_50 gd_txt_weight_900 gd_m_b_65 gd_txt_size_40_xl gd_line_height_40_xl gd_m_b_30_xl gd_m_b_25_lg gd_txt_size_30_md gd_line_height_30_md gd_m_b_15_md gd_txt_size_25_xs gd_line_height_25_xs txt_clr_4 primary_font">
                                        LIPSUM GENERATOR:<br>
                                        ALL THE FACTS
                                    </div>
                                    <div class="gd_para gd_txt_size_16 gd_line_height_25 gd_txt_size_14_lg gd_line_height_20_lg txt_clr_0 primary_font gd_txt_align_left gd_section_4 gd_m_b_30 gd_wrpr_full gd_m_b_15_lg gd_txt_size_13_lg gd_line_height_18_lg text_clr_0">
                                        Our BIM design solutions which are collaborative, detailed and user-friendly, supports our ethos of pursuing innovative thinking to provide greater value. 
                                        <br><br>
                                        As a forward-thinking company with a keen eye for detail, MPI is committed to adopting the latest in technology to bring home world class services and solutions to Oman. 
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> -->

                </div>
            </div>

        </div>        
    </section>



    <?php include 'include/detail_work_common_block.php'; ?>




    <?php /* /;?>
    
    <?php include 'include/banner_new.php'; ?>

    <?php include 'include/what.php'; ?>

    <?php include 'include/virtual_design.php';?>

    <?php include 'include/what_new.php';?>

    <?php include 'include/our_acheivement.php';?>
    
    <?php /* */;?>
    



    <?php include 'include/footer.php';?>

    <?php include 'include/script.php';?>

</body>
</html>