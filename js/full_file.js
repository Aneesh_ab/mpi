function require(jspath) {
    document.write('<script type="text/javascript" src="'+jspath+'"><\/script>');
}



require("js/owl.carousel.js");
require("js/widgets_slider.js");


require("js/aos.js");

require("js/bootstrap.bundle.min.js");
require("js/bootstrap-select.js");
require("js/common.js");
require("js/elements_forms_select.js");
require("js/form_input.js");

// require("js/widget_design.js");
require("js/popup_alert.js");
require("js/overlay.js");
require("js/widget_other_element.js");
require("js/jquery.mCustomScrollbar.js");

require("js/fha.js");
require("js/widget_design.js");
require("js/star_rating.min.js");

require("js/css_creation.js");
require("js/custom.js");
