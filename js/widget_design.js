/*------------ developed by joby front-end developer Gl infotech------------*/
/*-------------------------------11 Dec 2018--------------------------------*/


$(function() {
    if ($('.gd_counter_wrpr').length) {
        counter();
    }

    if ($('.gd_tab_active').length) {
        tab_creator();
    }

    if ($('.gd_collapse_wrpr').length) {
        collapse();
    }

    if ($('.gd_tab_collapse_wrpr').length) {
        tab_collapse();
    }

    if ($('.gd_backto_top').length) {
        backto_top();
    }

});



function counter() {

    $('body').on('click', '.gd_counter_minus', function() {
        var $input = $(this).siblings('input');
        var count = parseInt($input.val()) - 1;
        count = count < 0 ? 0 : count;
        $input.val(count);
        return false;
    }).on('click', '.gd_counter_plus', function() {
        var $input = $(this).siblings('input');
        $input.val(parseInt($input.val()) + 1);
        return false;
    }).on('blur', '.gd_counter', function() {
        var conter_val = $(this).val();
        conter_val = conter_val <= null ? 0 : conter_val;
        $(this).val(conter_val);
    }).on('keypress keyup blur', '.gd_counter', function(event) {
        $(this).val($(this).val().replace(/[^\d].+/, ""));
        if ((event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });
}



function collapse() {


    $('.gd_collapse_active .gd_collapse_body_inner').each(function() {
        var height = parseInt($(this).outerHeight());
        if ($(this).parent().parent('.gd_collapse').hasClass('active')) {
            $(this).parent().css({
                "height": height
            });
        }
    });

    $('body').on('click', '.gd_collapse_active .collapsed', function() {
        $(this).parent().toggleClass('active').siblings().removeClass('active');

        if ($(this).parents('.gd_collapse_active').length > 1) {
            $(this).parents('.gd_collapse_body').css({
                "height": 'auto'
            });
            var p_height = $(this).parents('.gd_collapse_body').outerHeight();
        }


        $(this).parent().siblings().children('.gd_collapse_body').css({
            "height": '0px'
        });
        var height = parseInt($(this).siblings().find('.gd_collapse_body_inner').outerHeight());

        $(this).siblings().css({
            "height": height
        });
        if (!$(this).parent().hasClass('active')) {
            $(this).siblings().css({
                "height": '0px'
            });
        }
    });
}



function tab_creator() {
    $('body').on('click', '.gd_tab_active .gd_tab_head', function() {
        var tab_head_parent_attr = $(this).parent().attr('data-tab-head-wrpr');
        var selected_head = $(this).attr('data-tab-head');

        $(this).addClass('active').siblings().removeClass('active');

        $('.gd_tab_body[data-tab-body-wrpr="' + tab_head_parent_attr + '"] .gd_tab_content').removeClass('active');
        $('.gd_tab_body[data-tab-body-wrpr="' + tab_head_parent_attr + '"] .gd_tab_content[data-tab-body="' + selected_head + '"]').addClass('active');
    });
}




function tab_collapse() {
    var window_w = $(window).width();
    $('body').on('click', '.gd_tab_active .gd_tab_head', function() {
        var tab_head_parent_attr = $(this).parent().attr('data-tab-collapse-wrpr');
        var selected_head = $(this).attr('data-tab-collapse-head');

        $(this).addClass('active').siblings().removeClass('active');

        $('[data-tab-collapse-body="' + tab_head_parent_attr + '"] .gd_collapse').removeClass('active');
        $('[data-tab-collapse-body="' + tab_head_parent_attr + '"] .gd_collapse[data-tab-collapse-body-inner="' + selected_head + '"]').addClass('active');
    });

    $('body .gd_tab_collapse_wrpr').each(function() {
        var resp_size = $(this).attr('data-tab-collapse-resp');
        resp_size = size_converstion_resp(resp_size);
        if (resp_size != null) {
            if (window_w <= resp_size) {
                $(this).addClass('resp');
            }
        } else {
            if (window_w <= 1024) {
                $(this).addClass('resp');
            }
        }
    });

    $('.gd_tab_collapse_wrpr [data-tab-collapse-head]').each(function() {
        var this_attr = $(this).attr('data-tab-collapse-head');
        var par_attr = $(this).parents('[data-tab-collapse-wrpr]').attr('data-tab-collapse-wrpr');

        var content = $(this).find('.resp_append_head').clone();

        $('[data-tab-collapse-body="' + par_attr + '"] [data-tab-collapse-body-inner="' + this_attr + '"] .gd_collapse_head').append(content);
    });
}






function backto_top() {

    var animate_scroll = $('.gd_backto_top').attr('data-scroll-view');
    var scroll_pos = $('.gd_backto_top').attr('data-scroll-position');
    var doc_height = $(document).outerHeight();


    if (animate_scroll && animate_scroll == "true") {
        if (!scroll_pos) {
            scroll_pos = 3;
        }

        scroll_pos = Math.round(doc_height / scroll_pos);

        $(window).scroll(function() {
            var scrolling = $(window).scrollTop();
            if (scrolling >= scroll_pos) {
                $('.gd_backto_top').addClass('active');
            } else {
                $('.gd_backto_top').removeClass('active');
            }

        });
    }



    $('body').on('click', '.gd_backto_top', function() {

        var animate_time = parseInt($(this).attr('data-animate-time'));


        if (!animate_time) {
            animate_time = 3000;
        }

        $('html, body').animate({
            scrollTop: 0
        }, animate_time);


        return false;
    });
}