/*------------ developed by joby front-end developer Gl infotech------------*/
/*-------------------------------11 Dec 2018--------------------------------*/




/*for the bootstarp carousel customization sections are added here*/
// $(function() {
//     if ($('.owl-carousel.gd_slider').length) {
//         owlslider();
//     }

// });



/*dynamically created the owl carousel*/
function owlslider() {
    var conunt = 0;
    $('.owl-carousel.gd_slider').each(function() {

        $(this).removeClass(function(index, css) {
            return (css.match(/(^|\s)gd_owlcarousel\S+/g) || []);
        });

        if ($(this).next('.gd_slider_control_wrpr').length) {
            var control_left = $(this).next('.gd_slider_control_wrpr').find('.gd_slider_control_left').wrap();
            var control_right = $(this).next('.gd_slider_control_wrpr').find(' .gd_slider_control_right').wrap();
        } else {
            var control_left = "<div class='gd_btn gd_bg_clr_white'>prev</div>";
            var control_right = "<div class='gd_btn gd_bg_clr_white'>next</div>";
        }

        var wrapper_classes = $(this).next('.gd_slider_control_wrpr').attr('class');
        $(this).next('.gd_slider_control_wrpr').remove();
        $(this).addClass('gd_owlcarousel_' + conunt);
        $(this).next('.gd_slider_indicators').addClass('gd_slider_indicators_' + conunt);


        var inner_margin = $(this).attr('data-ext-class');


        var C_items = $(this).attr('data-slide-items'),
            P_items = 1;
        if (C_items) {
            P_items = C_items;
        } else {
            P_items;
        }


        var C_margin = $(this).attr('data-slide-margin'),
            P_margin = 1;
        if (C_margin) {
            P_margin = parseInt(C_margin);
        } else {
            P_margin;
        }


        var C_loop = $(this).attr('data-slide-loop'),
            P_loop = false;
        if (C_loop) {
            if (C_loop == 'true') {
                P_loop = true;
            } else {
                P_loop;
            }
        } else {
            P_loop;
        }


        var C_center = $(this).attr('data-slide-center'),
            P_center = false;
        if (C_center) {
            if (C_center == 'true') {
                P_center = true;
            } else {
                P_center;
            }
        } else {
            P_center;
        }



        var C_mergeFit = $(this).attr('data-slide-mergeFit'),
            P_mergeFit = false;
        if (C_mergeFit) {
            if (C_mergeFit == 'true') {
                P_mergeFit = true;
            } else {
                P_mergeFit;
            }
        } else {
            P_mergeFit;
        }




        var C_autoWidth = $(this).attr('data-slide-autoWidth'),
            P_autoWidth = false;
        if (C_autoWidth) {
            if (C_autoWidth == 'true') {
                P_autoWidth = true;
            } else {
                P_autoWidth;
            }
        } else {
            P_autoWidth;
        }


        var C_startPosition = $(this).attr('data-slide-startPosition'),
            P_startPosition = 0;
        if (C_startPosition) {
            P_startPosition = C_startPosition;
        } else {
            P_startPosition;
        }



        var C_nav = $(this).attr('data-slide-nav'),
            P_nav = false;
        if (C_nav) {
            if (C_nav == 'true') {
                P_nav = true;
            } else {
                P_nav;
            }
        } else {
            P_nav;
        }

        var C_dots = $(this).attr('data-slide-dots'),
            P_dots = false;
        if (C_dots) {
            if (C_dots == 'true') {
                P_dots = true;
            } else {
                P_dots;
            }
        } else {
            P_dots;
        }


        var C_animateOut = $(this).attr('data-slide-animateOut'),
            P_animateOut = '';
        if (C_animateOut) {
            P_animateOut = C_animateOut;
        } else {
            P_animateOut;
        }



        var C_animateIn = $(this).attr('data-slide-animateIn'),
            P_animateIn = '';
        if (C_animateIn) {
            P_animateIn = C_animateIn;
        } else {
            P_animateIn;
        }


        var C_slideBy = $(this).attr('data-slide-slideBy'),
            P_slideBy = 1;
        if (C_slideBy) {
            P_slideBy = C_slideBy;
        } else {
            P_slideBy;
        }




        var C_dotsEach = $(this).attr('data-slide-dotsEach'),
            P_dotsEach = false;
        if (C_dotsEach) {
            if (C_dotsEach == 'true') {
                P_dotsEach = true;
            } else {
                P_dotsEach;
            }
        } else {
            P_dotsEach;
        }




        var C_lazyLoad = $(this).attr('data-slide-lazyLoad'),
            P_lazyLoad = false;
        if (C_lazyLoad) {
            if (C_lazyLoad == 'true') {
                P_lazyLoad = true;
            } else {
                P_lazyLoad;
            }
        } else {
            P_lazyLoad;
        }


        var C_autoplay = $(this).attr('data-slide-autoplay'),
            P_autoplay = false;
        if (C_autoplay) {
            if (C_autoplay == 'true') {
                P_autoplay = true;
            } else {
                P_autoplay;
            }
        } else {
            P_autoplay;
        }




        var C_autoplayTimeout = $(this).attr('data-slide-autoplayTimeout'),
            P_autoplayTimeout = 5000;
        if (C_autoplayTimeout) {
            P_autoplayTimeout = C_autoplayTimeout;
        } else {
            P_autoplayTimeout;
        }

        var C_autoplaySpeed = $(this).attr('data-slide-autoplaySpeed'),
            P_autoplaySpeed = 500;
        if (C_autoplaySpeed) {
            P_autoplaySpeed = C_autoplaySpeed;
        } else {
            P_autoplaySpeed;
        }


        var C_navSpeed = $(this).attr('data-slide-navSpeed'),
            P_navSpeed = 300;
        if (C_navSpeed) {
            P_navSpeed = C_navSpeed;
        } else {
            P_navSpeed;
        }

        var C_dotsSpeed = $(this).attr('data-slide-dotsSpeed'),
            P_dotsSpeed = 300;
        if (C_dotsSpeed) {
            P_dotsSpeed = C_dotsSpeed;
        } else {
            P_dotsSpeed;
        }


        var C_autoplayHoverPause = $(this).attr('data-slide-autoplayHoverPause'),
            P_autoplayHoverPause = false;
        if (C_autoplayHoverPause) {
            if (C_autoplayHoverPause == 'true') {
                P_autoplayHoverPause = true;

                autoplayHoverPause_f($(this), P_autoplayTimeout);
            } else {
                P_autoplayHoverPause;
            }
        } else {
            P_autoplayHoverPause;
        }





        var C_responsive = [0, 576, 992, 768, 1024, 1200];
        var i = 0,
            j = 0;
        var vars = {};
        var vars_ex = {};

        while (i < C_responsive.length) {


            if ($(this).attr('data-slide-responsive-' + C_responsive[i])) {

                vars['C_responsive_' + C_responsive[i]] = $(this).attr('data-slide-responsive-' + C_responsive[i]);
                vars['C_responsive_' + C_responsive[i]] = vars['C_responsive_' + C_responsive[i]].slice(1, -1);
                vars['C_responsive_' + C_responsive[i]] = JSON.stringify(vars['C_responsive_' + C_responsive[i]].split(','));
                vars['C_responsive_' + C_responsive[i]] = JSON.parse(vars['C_responsive_' + C_responsive[i]].split(','));


                for (j = 0; j < vars['C_responsive_' + C_responsive[i]].length; j++) {
                    var local = vars['C_responsive_' + C_responsive[i]][j].split(':');
                    vars_ex['C_responsive_' + C_responsive[i] + '_' + local[0]] = JSON.parse(local[1]);
                }

            }

            i++;
        }

        var owl_selector = $(".gd_owlcarousel_" + conunt);

        owl_selector.owlCarousel({
            items: P_items,
            margin: P_margin,
            loop: P_loop,
            center: P_center,
            mouseDrag: true,
            pullDrag: true,
            mergeFit: P_mergeFit,
            autoWidth: P_autoWidth,
            startPosition: P_startPosition,
            nav: P_nav,
            rewind: true,
            slideBy: P_slideBy,
            dots: P_dots,
            autoplay: P_autoplay,
            autoplayTimeout: P_autoplayTimeout,
            autoplaySpeed: P_autoplaySpeed,
            navSpeed: P_navSpeed,
            dotsSpeed: P_dotsSpeed,
            navText: [control_left, control_right],
            navContainerClass: wrapper_classes,
            dotsContainer: '.gd_slider_indicators_' + conunt,
            animateOut: P_animateOut,
            animateIn: P_animateIn,
            lazyLoad: P_lazyLoad,
            responsive: {
                0: {
                    items: vars_ex['C_responsive_0_items'],
                    margin: vars_ex['C_responsive_0_margin'],
                    loop: vars_ex['C_responsive_0_loop'],
                    nav: vars_ex['C_responsive_0_nav'],
                    dots: vars_ex['C_responsive_0_dots'],
                    autoplay: vars_ex['C_responsive_0_autoplay'],
                    autoplayTimeout: vars_ex['C_responsive_0_autoplayTimeout']
                },
                577: {
                    items: vars_ex['C_responsive_576_items'],
                    margin: vars_ex['C_responsive_576_margin'],
                    loop: vars_ex['C_responsive_576_loop'],
                    nav: vars_ex['C_responsive_576_nav'],
                    dots: vars_ex['C_responsive_576_dots'],
                    autoplay: vars_ex['C_responsive_576_autoplay'],
                    autoplayTimeout: vars_ex['C_responsive_576_autoplayTimeout']
                },
                769: {
                    items: vars_ex['C_responsive_768_items'],
                    margin: vars_ex['C_responsive_768_margin'],
                    loop: vars_ex['C_responsive_768_loop'],
                    nav: vars_ex['C_responsive_768_nav'],
                    dots: vars_ex['C_responsive_768_dots'],
                    autoplay: vars_ex['C_responsive_768_autoplay'],
                    autoplayTimeout: vars_ex['C_responsive_768_autoplayTimeout']
                },
                993: {
                    items: vars_ex['C_responsive_992_items'],
                    margin: vars_ex['C_responsive_992_margin'],
                    loop: vars_ex['C_responsive_992_loop'],
                    nav: vars_ex['C_responsive_992_nav'],
                    dots: vars_ex['C_responsive_992_dots'],
                    autoplay: vars_ex['C_responsive_992_autoplay'],
                    autoplayTimeout: vars_ex['C_responsive_992_autoplayTimeout']

                },
                1025: {
                    items: vars_ex['C_responsive_1024_items'],
                    margin: vars_ex['C_responsive_1024_margin'],
                    loop: vars_ex['C_responsive_1024_loop'],
                    nav: vars_ex['C_responsive_1024_nav'],
                    dots: vars_ex['C_responsive_1024_dots'],
                    autoplay: vars_ex['C_responsive_1024_autoplay'],
                    autoplayTimeout: vars_ex['C_responsive_1024_autoplayTimeout']
                },
                1201: {
                    items: vars_ex['C_responsive_1200_items'],
                    margin: vars_ex['C_responsive_1200_margin'],
                    loop: vars_ex['C_responsive_1200_loop'],
                    nav: vars_ex['C_responsive_1200_nav'],
                    dots: vars_ex['C_responsive_1200_dots'],
                    autoplay: vars_ex['C_responsive_1200_autoplay'],
                    autoplayTimeout: vars_ex['C_responsive_1200_autoplayTimeout']
                },
            }
        });


        var C_dot_class = $(this).next().attr('data-dot-class')
        if (C_dot_class) {
            $(this).next().addClass(C_dot_class);
        }

        $(this).find('.owl-stage-outer').addClass(inner_margin);
        $(this).next().find('.owl-dot').addClass('gd_slider_indicator_items');

        $('body').on('click', '.gd_slider_indicators_' + conunt + ' .gd_slider_indicator_items', function(e) {
            owl_selector.trigger("to.owl.carousel", [$(this).index(), P_dotsSpeed]);
        });

        conunt++;
    });
}



/*owl carousel hove pause*/
function autoplayHoverPause_f(item, P_autoplayTimeout) {
    item.on('mouseover', function() {
        item.trigger('stop.owl.autoplay');
    }).on('mouseleave', function() {
        item.trigger('play.owl.autoplay', [P_autoplayTimeout]);
    })
}