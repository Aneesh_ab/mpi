/*------------ developed by joby front-end developer Gl infotech------------*/
/*-------------------------------11 Dec 2018--------------------------------*/

// ellipsis
(function(a) {
    if (typeof define === "function" && define.amd) {
        define(["jquery"], a)
    } else {
        a(jQuery)
    }
}(function(d) {
    var c = "ellipsis",
        b = '<span style="white-space: nowrap;">',
        e = { lines: "auto", ellipClass: "ellip", responsive: false };

    function a(h, q) {
        var m = this,
            w = 0,
            g = [],
            k, p, i, f, j, n, s;
        m.$cont = d(h);
        m.opts = d.extend({}, e, q);

        function o() {
            m.text = m.$cont.text();
            m.opts.ellipLineClass = m.opts.ellipClass + "-line";
            m.$el = d('<span class="' + m.opts.ellipClass + '" />');
            m.$el.text(m.text);
            m.$cont.empty().append(m.$el);
            t()
        }

        function t() {
            if (typeof m.opts.lines === "number" && m.opts.lines < 2) {
                m.$el.addClass(m.opts.ellipLineClass);
                return
            }
            n = m.$cont.height();

            if (m.opts.lines === "auto" && m.$el.prop("scrollHeight") <= n) {
                return
            }

            if (!k) {
                return
            }
            s = d.trim(m.text).split(/\s+/);
            m.$el.html(b + s.join("</span> " + b) + "</span>");
            m.$el.find("span").each(k);

            if (p != null) { u(p) }
        }

        function u(x) {
            s[x] = '<span class="' + m.opts.ellipLineClass + '">' + s[x];
            s.push("</span>");
            m.$el.html(s.join(" "))
        }

        if (m.opts.lines === "auto") {
            var r = function(y, A) {
                var x = d(A),
                    z = x.position().top;
                j = j || x.height();
                if (z === f) { g[w].push(x) } else {
                    f = z;
                    w += 1;
                    g[w] = [x]
                }
                if (z + j > n) { p = y - g[w - 1].length; return false }
            };

            k = r
        }
        if (typeof m.opts.lines === "number" && m.opts.lines > 1) {
            var l = function(y, A) {
                var x = d(A),
                    z = x.position().top;
                if (z !== f) {
                    f = z;
                    w += 1
                }
                if (w === m.opts.lines) { p = y; return false }
            };
            k = l
        }

        if (m.opts.responsive) {
            var v = function() {
                g = [];
                w = 0;
                f = null;
                p = null;
                m.$el.html(m.text);
                clearTimeout(i);
                i = setTimeout(t, 100)
            };
            d(window).on("resize." + c, v)
        }
        o()
    }
    d.fn[c] = function(f) {
        return this.each(function() {
            try {
                d(this).data(c, (new a(this, f)))
            } catch (g) {
                if (window.console) {
                    console.error(c + ": " + g)
                }
            }
        })
    }
}));


$('.gd_ellipsis').ellipsis();
$('.gd_ellipsis_1').ellipsis({ lines: 1 });
$('.gd_ellipsis_2').ellipsis({ lines: 2 });
$('.gd_ellipsis_3').ellipsis({ lines: 3 });
$('.gd_ellipsis_4').ellipsis({ lines: 4 });
$('.gd_ellipsis_5').ellipsis({ lines: 5 });
$('.gd_ellipsis_responsive').ellipsis({ responsive: true });




$(function() {


    var break_points = { "xl": 1200, "lg": 1024, "md": 992, "sm": 768, "xs": 576 };
    var w_width = $(window).outerWidth();
    $.each(break_points, function(l, val) {
        for (var i = 1; i <= 5; i++) {
            if ($('.gd_ellipsis_' + i + '_' + l).length) {
                if (w_width <= val) {
                    $('.gd_ellipsis_' + i + '_' + l).ellipsis({ lines: i });
                }
            } else if ($('.gd_ellipsis_' + i + '_xxl').length) {
                if (w_width >= 1400) {
                    $('.gd_ellipsis_' + i + '_xxl').ellipsis({ lines: i });
                }
            }
        }
    });





    element_appends();
    element_clones();

});






function size_converstion_resp(triggering_size) {

    switch (triggering_size) {

        case 'xl':
            triggering_size = 1200;
            break;
        case 'lg':
            triggering_size = 1024;
            break;
        case 'md':
            triggering_size = 992;
            break;
        case 'sm':
            triggering_size = 768;
            break;
        case 'xs':
            triggering_size = 576;
            break;
    }
    return triggering_size;
}






function element_appends() {
    var window_w = $(window).width();

    $('.resp_append').each(function() {
        var this_class = $(this).attr('data-append-class');
        var appending_size = $(this).attr('data-append-size');

        appending_size_int = size_converstion_resp(appending_size);

        if (window_w <= appending_size_int) {
            $('body').find('.' + this_class).append($(this));
            $('body').find('.' + this_class + ' [data-append-class="' + this_class + '"]').addClass('flex_imp');
        }
    });
}


function element_clones() {
    var window_w = $(window).width();


    $('.resp_clone').each(function() {
        var this_class = $(this).attr('data-clone-class');
        // var cloning_size = $(this).attr('data-clone-size');
        var cloning_size = window_w;
        var clone_add_class = $(this).attr('data-clone-add-class');

        if (!cloning_size) {
            cloning_size_int = $(this).attr('data-clone-size');
        }

        cloning_size_int = size_converstion_resp(cloning_size);
        var cloned_element = $(this).clone();

        if (window_w <= cloning_size_int) {
            $('body').find('.' + this_class).html('');
            $('body').find('.' + this_class).append(cloned_element);
            $('body').find('.' + this_class).addClass(clone_add_class);
            $('body').find('.' + this_class + ' [data-clone-class="' + this_class + '"]').addClass('flex_imp');
        }
    });
}





function element_full_height(items) {
    var element_height = 0;
    var item_outer_height = parseInt(items.outerHeight());
    var item_outer_height = item_outer_height + parseInt(items.children().outerHeight());
    return item_outer_height;

}