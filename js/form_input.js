/*------------ developed by joby front-end developer Gl infotech------------*/
/*-------------------------------11 Dec 2018--------------------------------*/


$(function(){
	if ($('.gd_form_group_floating').length) {
		floating();
	}
});




// floating style
function floating(){
	$('body').on('focus','.gd_form_group_floating .gd_input',function(){
		$(this).parent('.gd_form_group_floating').addClass('floated');
	}).on('blur','.gd_form_group_floating .gd_input',function(){
		if ($(this).val() == '') {
			$(this).parent('.gd_form_group_floating').removeClass('floated');
		}
	});


	$('.gd_form_group_floating .gd_input').each(function(){
		if ($(this).val()) {
			$(this).parent('.gd_form_group_floating').removeClass('floated').addClass('floated');
		}
	});
}
