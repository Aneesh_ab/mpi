/*------------ developed by joby front-end developer Gl infotech------------*/
/*-------------------------------11 Dec 2018--------------------------------*/





$(function() {
    if ($('.gd_popup').length) {
        custom_popup();
    }
    if ($('.gd_alert').length) {
        custom_alert();
    }
});



function custom_popup() {
    $('body').on('click', '.gd_popup_btn', function() {

        var selected_attr = $(this).attr('data-trigger');
        $('.gd_popup[data-connection]').removeClass('active');
        $('.gd_popup[data-connection="' + selected_attr + '"]').addClass('active');

        overlay_active();

        overlay_clicks($('.gd_popup[data-connection="' + selected_attr + '"]'));
    }).on('click', '.gd_popup_closing', function() {

        $(this).parents('.gd_popup').removeClass('active');

        overlay_deactive();
    });
}



function custom_alert() {
    $('body').on('click', '[data-alert-trigger]', function(e) {

        e.preventDefault();
        $(this).toggleClass('active');
        var this_attr = $(this).attr('data-alert-trigger');
        var this_time = $('[data-alert-popup="' + this_attr + '"]').attr('data-alert-time');
        var this_switch = $(this).attr('data-alert-switch');
        var this_switch_time = $('[data-alert-popup="' + this_switch + '"]').attr('data-alert-time');

        if (this_time == null || this_switch_time == null) {
            this_time = 2000;
            this_switch_time = 2000;
        }


        if (this_switch) {
            if ($(this).hasClass('active')) {
                $('[data-alert-popup="' + this_attr + '"]').addClass('active');
                setTimeout(function() {
                    $('[data-alert-popup="' + this_attr + '"]').removeClass('active');
                }, this_time);
            } else {
                $('[data-alert-popup="' + this_switch + '"]').addClass('active');
                setTimeout(function() {
                    $('[data-alert-popup="' + this_switch + '"]').removeClass('active');
                }, this_switch_time);
            }

        } else {
            $('[data-alert-popup="' + this_attr + '"]').addClass('active');
            setTimeout(function() {
                $('[data-alert-popup="' + this_attr + '"]').removeClass('active');
            }, this_time);
        }
    });
}



// function alert_active(get_attr) {
//     var this_time = $('[data-alert-popup="' + get_attr + '"]').attr('data-alert-time');
//     if (this_time == null) {
//         this_time = 2000;
//     }
//     $('[data-alert-popup="' + get_attr + '"]').addClass('active');
//     setTimeout(function() {
//         $('[data-alert-popup="' + get_attr + '"]').removeClass('active');
//     }, this_time);
// }

// sample of alert 
var alert_parameters = {
    alert_content: 'this is the alert', //String
    alert_bg_clr: 'gd_bg_clr_success', //String
    // avilable background colors
    // 1.gd_bg_clr_danger
    // 2.gd_bg_clr_info
    // 3.gd_bg_clr_warning
    // 4.gd_bg_clr_success//-default
    alert_effect: 'alert_fade', //String
    // avilable effect
    // 1.alert_fade//-default
    alert_size: 'alert_small', //String+
    // available size
    // 1.alert_small
    // 2.alert_medium//-default
    // 3.alert_large
    alert_time: 4000, //Number
    // 3000-default
    alert_position: {
        // all are null default
        top: '', //Number/String
        right: '', //Number/String
        bottom: '', //Number/String
        left: '', //Number/String
        mesurement: '' //String
        // availabel mesurement
        // 1.px//-default
        // 2.%
        // 3.vh
        // 4.em
    },
    alert_user_classes: '', //String - pass the user defined classes separate with commas
    // gd_txt_clr_white & gd_txt_align_center are default
}


// alert_ative(alert_parameters);




function alert_ative(alert_parameters) {
    var array_keys = Object.keys(alert_parameters);
    var alert_slector = $('.gd_alert');
    var alert_content_fn = '';
    var alert_bg_clr_fn = '';
    var alert_bg_clr_avilable_fn = ['gd_bg_clr_danger', 'gd_bg_clr_info', 'gd_bg_clr_warning', 'gd_bg_clr_success'];
    var alert_effect_fn = '';
    var alert_effect_available_fn = ['alert_fade'];
    var alert_size_fn = '';
    var alert_size_availablr_fn = ['alert_small', 'alert_medium', 'alert_large'];
    var alert_time_fn = '';
    var alert_position_fn = '';
    var alert_user_classes_fn = '';
    var alert_overlay_fn = '';


    if ($.inArray("alert_content", array_keys) != '-1') {
        alert_content_fn = alert_parameters.alert_content;
        if (!alert_content_fn.trim()) {
            alert_content_fn = "alert message here";
        }
    } else {
        alert_content_fn = "alert message here";
    }




    if ($.inArray("alert_bg_clr", array_keys) != '-1') {
        alert_bg_clr_fn = alert_parameters.alert_bg_clr;
        if (!alert_bg_clr_fn.trim()) {
            alert_bg_clr_fn = "gd_bg_clr_success";
        } else {
            if ($.inArray(alert_bg_clr_fn, alert_bg_clr_avilable_fn) == '-1') {
                alert_bg_clr_fn = "gd_bg_clr_success";
            }
        }
    } else {
        alert_bg_clr_fn = "gd_bg_clr_success";
    }



    if ($.inArray("alert_effect", array_keys) != '-1') {
        alert_effect_fn = alert_parameters.alert_effect;
        if (!alert_effect_fn.trim()) {
            alert_effect_fn = "alert_fade";
        } else {
            if ($.inArray(alert_effect_fn, alert_effect_available_fn) == '-1') {
                alert_effect_fn = "alert_fade";
            }
        }
    } else {
        alert_effect_fn = "alert_fade";
    }



    if ($.inArray("alert_size", array_keys) != '-1') {
        alert_size_fn = alert_parameters.alert_size;
        if (!alert_size_fn.trim()) {
            alert_size_fn = "alert_medium";
        } else {
            if ($.inArray(alert_size_fn, alert_size_availablr_fn) == '-1') {
                alert_size_fn = "alert_medium";
            }
        }
    } else {
        alert_size_fn = "alert_medium";
    }



    if ($.inArray("alert_time", array_keys) != '-1') {
        alert_time_fn = parseInt(alert_parameters.alert_time);
        if ($.isNumeric(alert_time_fn)) {
            alert_time_fn = alert_time_fn;
        } else {
            alert_time_fn = 3000;
        }
    } else {
        alert_time_fn = 3000;
    }


    var alert_position_array_key_fn = '';
    var alert_position_top_fn = '',
        alert_position_right_fn = '',
        alert_position_bottom_fn = '',
        alert_position_left_fn = '',
        alert_position_mesurement_fn = '';
    var alert_position_mesurement_available_fn = ['px', '%', 'vh', 'em'];


    if ($.inArray("alert_position", array_keys) != '-1') {
        alert_position_fn = alert_parameters.alert_position;
        alert_position_array_key_fn = Object.keys(alert_parameters.alert_position);


        if ($.inArray("mesurement", alert_position_array_key_fn) != '-1') {
            alert_position_mesurement_fn = alert_parameters.alert_position.mesurement;

            if (!alert_position_mesurement_fn.trim()) {
                alert_position_mesurement_fn = 'px';
            } else {
                if ($.inArray(alert_position_mesurement_fn, alert_position_mesurement_available_fn) == '-1') {
                    alert_position_mesurement_fn = 'px';
                }
            }

        } else {
            alert_position_mesurement_fn = 'px';
        }




        if ($.inArray("top", alert_position_array_key_fn) != '-1') {
            alert_position_top_fn = alert_parameters.alert_position.top;

            if ($.isNumeric(alert_position_top_fn)) {
                alert_position_top_fn = alert_position_top_fn + '' + alert_position_mesurement_fn;
            } else if (alert_position_top_fn == 'auto') {
                alert_position_top_fn = alert_position_top_fn;
            } else {
                alert_position_top_fn = '';
            }

        } else {
            alert_position_top_fn = '';
        }




        if ($.inArray("right", alert_position_array_key_fn) != '-1') {
            alert_position_right_fn = alert_parameters.alert_position.right;

            if ($.isNumeric(alert_position_right_fn)) {
                alert_position_right_fn = alert_position_right_fn + '' + alert_position_mesurement_fn;
            } else if (alert_position_right_fn == 'auto') {
                alert_position_right_fn = alert_position_right_fn;
            } else {
                alert_position_right_fn = '';
            }

        } else {
            alert_position_right_fn = '';
        }


        if ($.inArray("bottom", alert_position_array_key_fn) != '-1') {
            alert_position_bottom_fn = alert_parameters.alert_position.bottom;

            if ($.isNumeric(alert_position_bottom_fn)) {
                alert_position_bottom_fn = alert_position_bottom_fn + '' + alert_position_mesurement_fn;
            } else if (alert_position_bottom_fn == 'auto') {
                alert_position_bottom_fn = alert_position_bottom_fn;
            } else {
                alert_position_bottom_fn = '';
            }

        } else {
            alert_position_bottom_fn = '';
        }

        if ($.inArray("left", alert_position_array_key_fn) != '-1') {
            alert_position_left_fn = alert_parameters.alert_position.left;

            if ($.isNumeric(alert_position_left_fn)) {
                alert_position_left_fn = alert_position_left_fn + '' + alert_position_mesurement_fn;
            } else if (alert_position_left_fn == 'auto') {
                alert_position_left_fn = alert_position_left_fn;
            } else {
                alert_position_left_fn = '';
            }

        } else {
            alert_position_left_fn = '';
        }

    } else {
        alert_position_top_fn = '';
        alert_position_right_fn = '';
        alert_position_bottom_fn = '';
        alert_position_left_fn = '';
        alert_position_mesurement_fn = '';
    }




    if ($.inArray("alert_user_classes", array_keys) != '-1') {
        alert_user_classes_fn = alert_parameters.alert_user_classes;
        if (!alert_user_classes_fn.trim()) {
            alert_user_classes_fn = 'gd_txt_clr_white gd_txt_align_center';
        }
    } else {
        alert_user_classes_fn = 'gd_txt_clr_white gd_txt_align_center';
    }




    alert_slector.text(alert_content_fn);
    var adding_classes = alert_bg_clr_fn + ' ' + alert_effect_fn + ' ' + alert_size_fn + ' ' + alert_user_classes_fn;
    alert_slector.addClass(adding_classes);
    alert_slector.css({
        'top': alert_position_top_fn,
        'right': alert_position_right_fn,
        'bottom': alert_position_bottom_fn,
        'left': alert_position_left_fn
    });


    alert_slector.addClass('active');

    setTimeout(function() {
        alert_slector.removeClass('active');
    }, alert_time_fn);
    setTimeout(function() {
        alert_slector.removeClass(adding_classes);
        alert_slector.removeAttr('style');
    }, alert_time_fn  + (alert_time_fn / 10));

}