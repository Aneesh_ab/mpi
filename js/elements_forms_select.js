/*------------ developed by joby front-end developer Gl infotech------------*/
/*-------------------------------11 Dec 2018--------------------------------*/



$(function() {
    if ($("[data-dropdown-effect='slide']").length) {
        gd_dropdown_slide();
    }
    if ($(".gd_dropdown_click").length) {
        gd_dropdown_click();
    }
    $('.selectpicker').selectpicker();
    gd_dropdown_resp();

});



function gd_dropdown_slide() {
    $('body').on('mouseenter', ".gd_dropdown_wrpr[data-dropdown-effect='slide']:not(.gd_dropdown_click)", function() {
        var element_height = element_full_height($(this).find('.gd_dropdown_menu_wrpr'));
        $(this).find(".gd_dropdown_menu_wrpr").css({ "max-height": element_height + "px", "height": element_height + "px" });
    }).on('mouseleave', ".gd_dropdown_wrpr[data-dropdown-effect='slide']:not(.gd_dropdown_click)", function() {
        $(this).find(".gd_dropdown_menu_wrpr").css({ "max-height": "0px", "height": "0px" });
    });
}




function gd_dropdown_click() {
    // $(".gd_dropdown_menu_wrpr").css({ "max-height": "0px", "height": "0px" });
    $('body').on('click', '.gd_dropdown_click .gd_dropdown_toggle', function(e) {
        $(this).parent('.gd_dropdown_click').toggleClass('active');

        var effect = $(this).parent('.gd_dropdown_click').attr("data-dropdown-effect");
        if (effect == "slide" && $(this).parent('.gd_dropdown_click').hasClass('active')) {
            var element_height = element_full_height($(this).parent('.gd_dropdown_click').find('.gd_dropdown_menu_wrpr'));
            if (typeof any_dropdown_changes != 'undefined' && $.isFunction(any_dropdown_changes)) {
                // for custom height changes
                element_height = any_dropdown_changes($(this).parent('.gd_dropdown_click').find('.gd_dropdown_menu_wrpr'));
            }
            $(this).parent('.gd_dropdown_click').find(".gd_dropdown_menu_wrpr").css({ "max-height": element_height + "px", "height": element_height + "px" });
        } else if (effect == "slide") {
            $(this).parent('.gd_dropdown_click').find(".gd_dropdown_menu_wrpr").css({ "max-height": "0px", "height": "0px" });
        }
    });
}


function gd_dropdown_resp() {

    $('body').on('click', '[data-dropdown-responsive] >:not(.gd_dropdown_toggle)', function(e) {
        e.stopPropagation();
    });

    $('body').on('click', '[data-dropdown-responsive]', function() {
        
        var resp_size = $(this).attr('data-dropdown-responsive');
        var resp_size_return = size_converstion_resp(resp_size);
        var window_width = $(window).width();

        $(this).parent().siblings().find('.gd_dropdown_click').removeClass('select_resp');



        if (resp_size == 'xxl') {
            if ((window_width >= 1200)) {
                var element_height = element_full_height($(this).find('.gd_dropdown_menu'));
                $(this).addClass('select_resp');
                $(this).find(".gd_dropdown_menu_wrpr").css({
                    "max-height": element_height + "px",
                    "height": element_height + "px"
                });
            }
        }

        if (window_width <= resp_size_return) {
            gd_dropdown_resp_call($(this));
        }

    }).on('click', '.select_resp[data-dropdown-responsive]', function() {
        var resp_size = $(this).attr('data-dropdown-responsive');
        var resp_size_return = size_converstion_resp(resp_size);

        var window_width = $(window).width();

        if (resp_size == 'xxl') {
            if ((window_width >= 1200)) {
                $(this).find(".gd_dropdown_menu_wrpr").css({ "max-height": "0px", "height": "0px" });
                $(this).removeClass('select_resp');
            }
        }
        if (window_width <= resp_size_return) {
            $(this).find(".gd_dropdown_menu_wrpr").css({ "max-height": "0px", "height": "0px" });
            $(this).removeClass('select_resp');
        }
    });
}




function gd_dropdown_resp_call(thid_item) {
    var element_height = element_full_height(thid_item.find('.gd_dropdown_menu_wrpr'));
    thid_item.addClass('select_resp');
    thid_item.find(".gd_dropdown_menu_wrpr").css({
        "max-height": element_height + "px",
        "height": element_height + "px"
    });
}