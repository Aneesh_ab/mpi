/*------------ developed by joby front-end developer Gl infotech------------*/
/*-------------------------------11 Dec 2018--------------------------------*/




$(function() {
    // hangburger
    if ($("[data-hangburger='trigger']").length) {
        hangburger();
    }

    if ($("[data-listing]").length) {
        listing_view();
    }


});






// hangburger
function hangburger() {

    var window_width = $(window).width();

    var nav = $("[data-hangburger='nav']");
    // var triggering_size = 'lg';
    var triggering_size = window_width;
    var triggering_size_int;

    if ($("[data-hangburger-triggering]").length) {
        triggering_size = $("[data-hangburger-triggering]").attr('data-hangburger-triggering');
    }


    triggering_size_int = size_converstion_resp(triggering_size);


    if (window_width <= triggering_size_int) {

        $('body').on('click', '[data-hangburger="trigger"]', function() {

            $("[data-hangburger='wrapper']").addClass('active');
            // $('.gd_body_overlay').addClass('active');

            overlay_active();
            overlay_clicks($("[data-hangburger='wrapper']")); /*from overlay js*/
            
        });

        if ($("[data-hangburger-append]").length) {
            order_appending();
        } else {
            $("[data-hangburger='wrapper']").append(nav);
        }
    }

}




function order_appending() {

    var val_storing = [];
    var val_storing_last = [];
    var window_width = $(window).width();
    var largest_append = 0;




    $("[data-hangburger-append]").each(function() {

        var clone_data = '';
        var largest_append_local = $(this).attr('data-hangburger-append');

        if ($(this).hasClass('clone')) {
            var adding_clas = $(this).attr('data-hangburger-clone');
            clone_data = $(this).clone().removeClass('clone').addClass(adding_clas);
            val_storing.push(clone_data);
        } else {
            val_storing.push($(this));
        }

        if (largest_append < largest_append_local) {
            largest_append = largest_append_local;
        }

    });


    for (var i = 0; i < largest_append; i++) {
        if (typeof val_storing[i] != "undefined") {
            var crct_val = val_storing[i].attr('data-hangburger-append');
            val_storing_last[crct_val - 1] = val_storing[i];
        }
    }

    val_storing_last = val_storing_last.filter(function(ele) {
        return ele !== undefined;
    });


    for (var i = 0; i < val_storing_last.length; i++) {

        var append_size = val_storing_last[i].attr('data-hangburger-append-size');
        // var addingClass = val_storing_last[i].attr('data-hangburger-adding-class');


        if (append_size == null) {
            // $("[data-hangburger='wrapper']").html('');

            var elemSelector = val_storing_last[i].attr('data-hangburger-append');

            if ($("[data-hangburger='wrapper']").find($("[data-hangburger-append='" + elemSelector + "']"))) {
                if ($("[data-hangburger='wrapper']").find('.mCustomScrollBox')) {

                    $("[data-hangburger='wrapper']").mCustomScrollbar("destroy");
                }
                $("[data-hangburger='wrapper']").find($("[data-hangburger-append='" + elemSelector + "']")).remove();
            }

            $("[data-hangburger='wrapper']").append(val_storing_last[i]);

            leftnavScrollSection();


        } else {
            append_size = size_converstion_resp(append_size);
            if (window_width <= append_size) {

                $("[data-hangburger='wrapper']").append(val_storing_last[i]);
            }
        }

    }

}


function leftnavScrollSection() {
    $("[data-hangburger='wrapper']").mCustomScrollbar({
        theme: "dark-thin",
        autoHideScrollbar: true
    });
}







function listing_view() {
    $('body').on('click', '[data-listing="grid"]', function() {
        $(this).addClass('active').siblings().removeClass('active');
        $('.gd_product_listing').addClass('gd_product_grid');
    }).on('click', '[data-listing="list"]', function() {
        $(this).addClass('active').siblings().removeClass('active');
        $('.gd_product_listing').removeClass('gd_product_grid');
    })
}