$(function() {

    var window_w = $(window).width();


    megamenu();
    megamenu_resp('megamenu_cloning');
    scroll_fixings();
    loading_popup('4');


    if ($('.filter_wrpr').length) {
        filter_clicks();
    }
    if ($('.search_wrpr').length) {
        search_functions();
    }

    if ($('[data-txt-effect]').length) {
        $('[data-txt-effect]').each(function() {
            var this_attr = $(this).attr('data-txt-effect');
            txt_show_effect(this_attr);
        });
    }

    // index page
    if ($('.custom_owl').length) {

        var get_a_quote_owl = $('.slider_one');
        var get_a_quote_owl_1 = $('.custom_owl');
        var get_currentItem = 0;
        var get_currentItem_1 = 0;

        left_active();

        if (!$('.slider_one').length) {
            get_a_quote_owl_1.on('translated.owl.carousel', function(event) {
                left_active();
            });
        } else {
            left_active();

            get_a_quote_owl.on('changed.owl.carousel', function(event) {
                get_currentItem = ((event.item.index) - (event.relatedTarget._clones.length / 2));
                get_a_quote_owl_1.trigger("to.owl.carousel", [get_currentItem]);
                left_active();
            });

            get_a_quote_owl_1.on('changed.owl.carousel', function(event) {
                get_currentItem_1 = ((event.item.index) - (event.relatedTarget._clones.length / 2));
                get_a_quote_owl.trigger("to.owl.carousel", [get_currentItem_1]);
            });
        }

        function left_active() {
            $('.custom_owl .owl-item.active').each(function(index) {
                if (index == 0) {
                    $($(this).addClass('active_ext'));
                    $($(this).siblings().removeClass('active_ext'));
                }
            });
        }
    }




    // detail page

    if ($('.zoom_img').length) {
        zoom_function();
    }


    if (window_w <= 1200) {
        if ($("[data-quote-resp-append]").length) {
            elements_resp_appending('quote');
        }
    }
    if (window_w <= 1024) {
        if ($("[data-filter-resp-append]").length) {
            elements_resp_appending('filter');
        }
        if ($("[data-sort-resp-append]").length) {
            elements_resp_appending('sort');
        }
        if ($("[data-subhead-resp-append]").length) {
            elements_resp_appending('subhead');
        }
        if ($("[data-quicklink-resp-append]").length) {
            elements_resp_appending('quicklink');
        }

        if ($("[data-compare-resp-append]").length) {
            elements_resp_appending('compare');
        }

        if ($("[data-faq-resp-append]").length) {
            elements_resp_appending('faq');
        }

        $(".filter_wrpr_dropdown .gd_dropdown_menu").mCustomScrollbar("destroy");


    } else {
        // scrollbar
        $(".filter_wrpr_dropdown .gd_dropdown_menu").mCustomScrollbar({
            theme: "dark-thin",
            autoHideScrollbar: true
        });
        $(".megamenu_left").mCustomScrollbar({
            theme: "dark-thin",
            autoHideScrollbar: true
        });
        $('.ongoing_hiding_txt').mCustomScrollbar({
            theme: "light-thin",
            autoHideScrollbar: true
        });

        $('.notification_wrpr').mCustomScrollbar({
            theme: "dark-thin",
            autoHideScrollbar: true
        });

        // function leftnavScrollSection() {
        //     $(".leftnav").mCustomScrollbar({
        //         theme: "light-thin",
        //         autoHideScrollbar: true
        //     });
        // }
    }


    $('.mega_search_result ').mCustomScrollbar({
        // theme: "light-thin",
        autoHideScrollbar: true
    });



    $(".megamenu_cloning .megamenu_left").mCustomScrollbar("destroy");

    if ($('.scroll_wrpr_detail').length) {
        click_to_scroll_section('detail');
        if (window_w <= 1024) {
            $('body').on('click', '.scroll_wrpr_detail li', function() {
                overlay_deactive();
                $('.scroll_wrpr_detail').parent().removeClass('active');
            });
        }
    }


    if ($('.scroll_wrpr_finance').length) {
        click_to_scroll_section('finance');
        // if (window_w <= 1024) {
        //     $('body').on('click', '.scroll_wrpr_finance li', function() {
        //         overlay_deactive();
        //         $('.scroll_wrpr_finance').parent().removeClass('active');
        //     });
        // }

    }

    if ($('input.gd_radio').length) {
        radio_button_check();
    }
    if ($('.my_cart_popup_trigger ').length) {
        addTocart();
    }

    if ($('.gd_form_group_floating').length) {
        // floated_focus();
    }


    // track order
    if ($('.track_progress_bar').length) {
        trackorder();
    }

    // comnpare page functions
    if ($('.compare_single_row .compare_item_head').length) {
        compare_page();
    }

    // chekout
    if ($('.checkout_full_cover_wrapr').length) {
        checkout();
        my_account_edit();
    }

    if ($('.scroll_bottom_fix').length) {
        bottom_fixings('scroll_bottom_fix');
    }

    star_ratings();

    if ($('.header_fixing').length) {
        header_height_to_margin();
        // header_height_to_margin(true);
    }


});





function megamenu() {

    $('[data-hangburger="nav"]').addClass('megamenu');

    $('body').on('mouseover', '.megamenu .megamenu_trigger', function() {
        var prev_secl = $(this).find('[data-meganav-wrpr-item].active').attr('data-meganav-wrpr-item');
        $(this).find('[data-meganav-wrpr-sub-item="' + prev_secl + '"]').addClass('active');
    });


    $('body').on('mouseover', '.megamenu [data-meganav-wrpr-item]', function() {
        var data_attr = $(this).attr('data-meganav-wrpr-item');
        var data_parent = $(this).parents('[data-meganav-wrpr]').attr('data-meganav-wrpr');

        $(this).parents('.megamenu_wrpr').find('[data-meganav-wrpr-item]').removeClass('active');
        $(this).addClass('active');

        // $(this).parents('[data-meganav-wrpr]').prevAll('[data-meganav-wrpr]').find('[data-meganav-wrpr-item]').removeClass('border_active');

        $('body').find('[data-meganav-wrpr-sub="' + data_parent + '"] [data-meganav-wrpr-sub-item]').removeClass('active');
        $('body').find('[data-meganav-wrpr-sub="' + data_parent + '"] [data-meganav-wrpr-sub-item="' + data_attr + '"]').addClass('active');

    }).on('mouseleave', '.megamenu .megamenu_trigger', function() {
        $(this).find('[data-meganav-wrpr-sub-item]').removeClass('active');
    });
};





function megamenu_resp(class_name) {

    var count = 0;
    // console.log('body '+class_name+' .megamenu_trigger');

    $('[data-hangburger="nav"].' + class_name + '').removeClass('megamenu');


    $('body .' + class_name + ' .megamenu_trigger').each(function() {
        var setting_height = $(this).find('.megamenu_left').outerHeight();

        $(this).attr('data-scroll-selector', count);
        $(this).addClass('nav_selector_' + count);


        if ($(this).hasClass('active')) {
            $(this).find('.megamenu_wrpr').css({
                "max-height": setting_height,
                "height": setting_height
            });
        }

        count++;
    });



    $('body').on('click', '.' + class_name + ' .megamenu_trigger .leftnav_drop_click', function(e) {
        var megamenu_h = $(this).parents('.megamenu_trigger').find('.megamenu_left').outerHeight();
        var attr_count = $(this).parents('.megamenu_trigger').attr('data-scroll-selector');
        var this_selector = $(this);

        $(this).parents('.megamenu_trigger').siblings().removeClass('active').find('.megamenu_wrpr').css({
            "max-height": "0px",
            "height": "0px"
        });


        if ($(this).parents('.megamenu_trigger').hasClass('active')) {
            $(this).parents('.megamenu_trigger').find('.megamenu_wrpr').css({
                "max-height": "0px",
                "height": "0px"
            });
            $(this).parents('.megamenu_trigger').removeClass('active');
        } else {

            // var focusElement = 'nav_selector_' + attr_count;

            // document.querySelector('.' + focusElement).scrollIntoView({
            //     behavior: 'smooth'
            // });


            $(this).parents('.megamenu_trigger').addClass('active');
            $(this).parents('.megamenu_trigger').find('.megamenu_wrpr').css({
                "max-height": megamenu_h,
                "height": megamenu_h
            });

            // setTimeout(function() {
            //     // $(this).parents('.megamenu_trigger').find('.megamenu_wrpr').css({
            //     //     "max-height": megamenu_h,
            //     //     "height": megamenu_h
            //     // });
            //     // console.log('test');
            // }, 0);


        }
    }).on('mouseleave', '.' + class_name + '.megamenu_trigger', function() {
        $(this).find('[data-meganav-wrpr-item]').removeClass('active');
    });





    // for login signup popup
    $('body').on('click', '[data-trigger="popup_1"]', function() {
        $('[data-hangburger="wrapper"]').removeClass('active');
    });
}






function scroll_fixings() {
    var window_w = $(window).width();
    var element_h = parseInt($('.header_fixing').outerHeight());

    // $('.header_fixing').next().next().css('margin-top', element_h + 'px');

    // filter_scroll_wrpr_top
    var fixed_item_top_1, fixed_item_top_2, fixed_item_top_3, fixed_item_top_4, element_h_1, element_h_2, element_h_3, element_h_4, hide_position;

    if ($(".filter_scroll_wrpr").length) {
        element_h_1 = $(".filter_scroll_wrpr").outerHeight();
        fixed_item_top_1 = ($(".filter_scroll_wrpr").offset().top) + element_h_1;
    }

    if ($(".detail_subhead").length) {
        element_h_2 = $(".detail_subhead").outerHeight();
        fixed_item_top_2 = ($(".detail_subhead").offset().top) + element_h_2;
    }

    if ($(".buynow_wrpr_fix").length) {
        element_h_3 = $(".buynow_wrpr_fix").outerHeight();
        fixed_item_top_3 = ($(".buynow_wrpr_fix").offset().top + 150) + element_h_3;
        hide_position_3 = parseInt($(document).outerHeight() / 5);
    }


    if ($(".finance_fix_wrpr").length) {
        element_h_4 = $(".finance_fix_wrpr").outerHeight();
        fixed_item_top_4 = ($(".finance_fix_wrpr").offset().top) + element_h_4;
    }


    window.onscroll = function() {

        if ($('.header_fixing').length && $('.header_fixing_wrpr').length) {
            header_fix($('.header_fixing'), $('.header_fixing_wrpr'));
        }

        if (window_w > 1024) {
            if ($("[data-filter-scroll-append]").length) {
                scroll_fixing_elements($(".filter_scroll_wrpr"), element_h_1, fixed_item_top_1, 'top');
                filter_scroll_appending($('.filtered_by_append'));
            }
            if ($(".detail_subhead").length) {
                scroll_fixing_elements($(".detail_subhead"), element_h_2, fixed_item_top_2, 'top');
            }

            if ($(".finance_fix_wrpr").length) {
                scroll_fixing_elements($(".finance_fix_wrpr"), element_h_4, fixed_item_top_4, 'top');
            }
        } else {
            if ($('.buynow_wrpr_fix').length) {
                scroll_fixing_elements($(".buynow_wrpr_fix"), element_h_3, fixed_item_top_3, 'bottom', hide_position_3);
            }
        }
    }
}



var lastScrollTop = 0;
var lastScrollTop1 = 0;

function header_fix(fix_item, fixing_content) {
    var element_h = parseInt(fix_item.outerHeight());
    var element_t = fixing_content.position().top;
    var scroll_pos = $(window).scrollTop();

    if (scroll_pos > lastScrollTop) {
        fix_item.addClass('sticky').css({
            "top": "-" + element_t + "px"
        });
    } else {
        fix_item.removeClass('sticky').css({
            "top": "0px"
        });
    }
    lastScrollTop = scroll_pos;
}



function scroll_fixing_elements(fix_item, element_h, fixed_item_top, position, hide_position, top_size) {

    var scroll_pos = $(window).scrollTop();
    var window_h = $(window).height();
    var doc_h = $(document).outerHeight();
    var scroll_full = Math.round(scroll_pos + window_h);
    var header_h = $('header').outerHeight();

    if (top_size != null) {
        top_size = top_size;
    } else {
        top_size = 0;
    }

    if (position == 'top') {

        if (scroll_pos > fixed_item_top) {

            if (scroll_pos > lastScrollTop1) {
                if (fix_item.hasClass('margin_top_setter')) {
                    fix_item.next().css('margin-top', (element_h + header_h));
                    fix_item.removeAttr('style');
                } else {
                    fix_item.next().css('margin-top', element_h);
                }

                fix_item.addClass('active active_pls').css({
                    "top": top_size
                });

            } else {
                fix_item.removeClass('active_pls');
            }

            lastScrollTop1 = scroll_pos;

        } else {

            fix_item.next().css('margin-top', 'auto');
            fix_item.removeClass('active active_pls').css({
                "top": "0px"
            });

            if (fix_item.hasClass('margin_top_setter')) {
                header_height_to_margin(true);
            }

        }

    } else if (position == 'bottom') {

        if (scroll_full > fixed_item_top) {
            fix_item.addClass('active active_pls').css({
                "bottom": "0px"
            });

            if (hide_position != null) {
                if (scroll_full > hide_position) {
                    fix_item.addClass('disabling');
                } else {
                    fix_item.removeClass('disabling');
                }
            }

        } else {

            fix_item.removeClass('active active_pls').css({
                "bottom": "0px"
            });
        }
    }
}



function filter_scroll_appending(fix_item) {
    var scroll_pos = $(window).scrollTop();
    var fix_item_top = parseInt(fix_item.offset().top);
    var header_h = parseInt($('.header_fixing').outerHeight());
    var val_storing = [];
    var val_storing_last = [];

    if (scroll_pos > fix_item_top) {

        $("[data-filter-scroll-append]").each(function() {
            var fixing_h = $(this).outerHeight();
            $(this).css("height", fixing_h);
            val_storing.push($(this));
        });

        for (var i = 0; i < val_storing.length; i++) {
            var crct_val = val_storing[i].attr('data-filter-scroll-append');
            val_storing_last[crct_val - 1] = val_storing[i].children();
        }

        for (var i = 0; i < val_storing_last.length; i++) {
            $(".filter_scroll_wrpr[data-filter-scroll='wrapper']  [data-filter-scroll-appending='" + (i + 1) + "']").append(val_storing_last[i]);
        }
    } else {
        $("[data-filter-scroll-appending]").each(function() {
            val_storing.push($(this));
        });

        for (var i = 0; i < val_storing.length; i++) {
            var crct_val = val_storing[i].attr('data-filter-scroll-appending');
            val_storing_last[crct_val - 1] = val_storing[i].children();
        }

        for (var i = 0; i < val_storing_last.length; i++) {
            $("[data-filter-scroll-append='" + (i + 1) + "']").append(val_storing_last[i]);
            $("[data-filter-scroll-append='" + (i + 1) + "']").attr('style', '');
        }
    }
}



function filter_clicks() {

    var window_width = $(window).width();

    $('body').on('click', '.filter_wrpr .gd_dropdown_toggle', function() {
        $(this).parents('.filter_wrpr').siblings().find('.gd_dropdown_click').removeClass('active');
        $(this).parents('.filter_wrpr').siblings().find('.gd_dropdown_menu_wrpr').css({
            'max-height': '0px',
            'height': '0px'
        });

        $(this).parents('.filter_wrpr').addClass('gd_zindex_300').siblings().removeClass('gd_zindex_300');

        var this_left = parseInt($(this).parents('.filter_wrpr').offset().left);

        // for the scroll view of filter
        $(this).parents('.filter_wrpr').find('.gd_dropdown_toggle').removeClass('set_dropdown_right');
        if ($('.filter_scroll_wrpr').hasClass('active_pls')) {
            if (this_left >= (window_width / 2)) {
                $(this).toggleClass('set_dropdown_right');
            }
        }

    });

    $('body').on('click', '.filter_wrpr .filter_close', function() {
        $(this).parents('.filter_wrpr').find('.gd_dropdown_click ').removeClass('active');
        $(this).parents('.filter_wrpr').find('.gd_dropdown_menu_wrpr').css({
            'max-height': '0px',
            'height': '0px'
        });
        $(this).parents('.filter_wrpr').removeClass('gd_zindex_300');
        $(this).parents('.filter_wrpr').find('.gd_dropdown_toggle').removeClass('set_dropdown_right');
    });

    $('body').on('click', '.filter_by_close', function() {
        $(this).parent().remove();
    });

}


// for select.js
function any_dropdown_changes(item) {
    var fixed_height = 300;

    var item_height = element_full_height(item);
    if (item_height > fixed_height) {
        return fixed_height;
    } else {
        return item_height;

    }
}





function elements_resp_appending(indentifier) {



    var val_storing = [];
    var val_storing_last = [];

    $("[data-" + indentifier + "-resp-append]").each(function() {
        val_storing.push($(this));
    });

    for (var i = 0; i < val_storing.length; i++) {
        var crct_val = val_storing[i].attr('data-' + indentifier + '-resp-append');
        val_storing_last[crct_val - 1] = val_storing[i]
    }

    for (var i = 0; i < val_storing_last.length; i++) {
        $("[data-" + indentifier + "-resp='wrapper']").append(val_storing_last[i]);
    }

    $('body').on('click', '.' + indentifier + '_click', function() {
        $("[data-" + indentifier + "-resp='wrapper']").addClass('active');
        overlay_active();
        overlay_clicks($("[data-" + indentifier + "-resp='wrapper']"));
    });
}






function zoom_carousel(carousel_calss_1, carousel_calss_2, indicator_click) {

    var first_carousel = $("." + carousel_calss_1);
    var second_carousel = $("." + carousel_calss_2);
    var first_carousel_item = 0;
    var second_carousel_item = 0;
    var tem_count = 0;


    first_carousel.on('translated.owl.carousel', function(event) {
        first_carousel_item = event.item.index;
        var this_img = $(this).find('.active img').attr('src');
        second_carousel.find('.item').removeClass('zoom_active');
        second_carousel.find('img[src="' + this_img + '"]').parents('.item').addClass('zoom_active');
        second_carousel.trigger("to.owl.carousel", [first_carousel_item]);
    });


    second_carousel.find('.item').each(function() {
        $(this).attr('data-item-count', tem_count);
        tem_count++;
    });

    if (indicator_click) {
        $('body').on('click', '.' + carousel_calss_2 + ' .item', function(e) {
            $(this).parent().siblings().find('.item').removeClass('zoom_active');
            $(this).addClass('zoom_active');
            var tem_attr = $(this).attr('data-item-count');
            first_carousel.trigger("to.owl.carousel", [tem_attr]);
        });
    }
}


function zoom_function() {

    var item_array = [];
    var item_array_1 = [];

    var items_length = $('.zoom_img .item').length;

    $('body').on('click', '.zoom_img .item, .detail_bottom_slider .item', function() {
        zoom_carousel_linking('detail_bottom_slider', 'detail_zoom_carousel');
        $('.zoom_clone').addClass('active');

    });

    $('body').on('click', '.zoom_close', function() {
        zoom_carousel_linking('detail_zoom_carousel_thumb', 'zoom_img');
        $('.zoom_clone').removeClass('active');
    });

    zoom_carousel('zoom_img', 'detail_bottom_slider', false);
    zoom_carousel('detail_zoom_carousel', 'detail_zoom_carousel_thumb', true);



    $('.zoom_img').owlCarousel({
        items: 1,
        video: true
    });

    $('.detail_bottom_slider').owlCarousel({
        items: 6,
        nav: true,
        margin: 20,
        navText: ["<span class='gd_icon_arrow2_left_t'></span>", "<span class='gd_icon_arrow2_right_t'></span>"],
        responsive: {
            0: {
                margin: 10
            },
            577: {
                margin: 20
            }
        }
    });


    $('.zoom_img .item').each(function() {
        var this_clone = $(this).clone();
        var this_img = $(this).find('img').attr('data-large-img');
        this_clone.find('img').attr('src', this_img);
        item_array.push(this_clone);
    });
    $('.detail_bottom_slider .item').each(function() {
        var this_clone = $(this).clone();
        var this_img = $(this).find('img').attr('data-large-img');
        this_clone.find('img').attr('src', this_img);
        item_array_1.push(this_clone);

    });

    for (var i = 0; i < items_length; i++) {
        $('.detail_zoom_carousel').append(item_array[i]);
        $('.detail_zoom_carousel_thumb').append(item_array_1[i]);
    }

    $('.detail_zoom_carousel').owlCarousel({
        items: 1,
        nav: true,
        navText: ["<span class='gd_icon_arrow2_left_t'></span>", "<span class='gd_icon_arrow2_right_t'></span>"],
        animateOut: "fadeOut",
        dots: true,
        responsive: {
            577: {
                dots: false
            }
        }
    });
    $('.detail_zoom_carousel_thumb').owlCarousel({
        items: 6,
        margin: 10
    });
}


function zoom_carousel_linking(carousel_thumb, carousel_selectors) {
    var selected_attr = $('.' + carousel_thumb).find('.item.zoom_active').attr('data-item-count');
    var carousel = $('.' + carousel_selectors);
    carousel.trigger("to.owl.carousel", [selected_attr]);
}




function click_to_scroll_section(indentifier) {
    // console.log(indentifier);
    $('body').on('click', '.scroll_wrpr_' + indentifier + ' [data-scroll-to-' + indentifier + ']', function() {
        $('body').find('[data-scroll-to-' + indentifier + ']').parent().removeClass('active');
        $(this).parent().addClass('active');
        var data_indenti = $(this).attr('data-scroll-to-' + indentifier);

        $('html, body').animate({
            scrollTop: $('[data-scroll-from-' + indentifier + '="' + data_indenti + '"]').offset().top - 200
        }, 1000);
    });
    $(window).scroll(function() {
        var scrolling = $(window).scrollTop();

        $('[data-scroll-from-' + indentifier + ']').each(function() {
            var this_item_top = Math.round($(this).offset().top);
            var this_item_height = Math.round(($(this).outerHeight()) + (this_item_top + $('header').outerHeight()));
            var data_indenti_val = $(this).attr('data-scroll-from-' + indentifier);


            if (scrolling + 250 >= this_item_top && scrolling + 250 < this_item_height) {
                $('body').find('[data-scroll-to-' + indentifier + ']').parent().removeClass('active');
                $('body').find('[data-scroll-to-' + indentifier + '="' + data_indenti_val + '"]').parent().addClass('active');
            }
        })
    });
}






// function search_functions() {
//     var window_w = $(window).width();



//     $('body').on('keyup', '.search_wrpr input, .search_wrpr_1 input, .search_wrpr_2 input', function() {
//         var content = $(this).val();

//         if (content == '') {
//             $(this).siblings('.search_close,.search_result').removeClass('active');
//         } else {
//             $(this).siblings('.search_close,.search_result').addClass('active');
//         }
//     }).on('click', '.search_wrpr, .search_wrpr_1, search_wrpr_2', function(e) {
//         e.stopPropagation();
//         $(this).find('input').focus();
//         $(this).addClass('active');
//     }).on('click', '.search_close', function() {
//         $(this).removeClass('active');
//         $(this).siblings('.search_result').removeClass('active');
//     });



//     if (window_w > 1024) {
//         $('body').on('mouseover', '.search_wrpr', function() {
//             $(this).find('input').focus();
//             $(this).addClass('active');
//         }).on('click', function() {
//             $('.search_wrpr,.search_result_1,search_wrpr_2').removeClass('active');
//         });

//         $(window).scroll(function() {
//             $('.search_result').removeClass('active');
//         });
//     } else {
//         $('body').on('click', '.search_back', function(e) {
//             e.stopPropagation();
//             $('.search_wrpr ,.search_wrpr_1,search_wrpr_2').removeClass('active');
//         });
//     }
// }


function search_functions() {

    var window_width = $(window).width();

    $('body').on('click', '.search_function_js:not("[data-mega-search-resp] .search_function_js")', function() {
        $(this).parents('.mega_search').addClass('active');
        $(this).parents('.mega_search').find('.mega_search_input_js').val('');
        $(this).parents('.mega_search').find('.mega_search_result').removeClass('active');
    }).on('click', '.mega_search_close_js', function(e) {
        e.stopPropagation();
        $(this).parents('.mega_search').removeClass('active');
        $(this).parents('.mega_search').find('.mega_search_input_js').val('');
        $(this).parents('.mega_search').find('.mega_search_result').removeClass('active');
    });

    $('body').on('keyup', '.mega_search_input_js', function() {

        var content = $(this).val();

        if (content == '') {
            $(this).parents('.mega_search').find('.mega_search_result').removeClass('active');
        } else {
            $(this).parents('.mega_search').find('.mega_search_result').addClass('active');
        }
    });




    $('.mega_search[data-mega-search-resp]').each(function() {
        var this_attr_selector = parseInt($(this).attr('data-mega-search-resp'));
        if (window_width <= this_attr_selector) {
            $(this).addClass('mega_search_resp');
        }
    });

    $('body').on('click', '.mega_search_resp .search_function_js,.mega_search_resp .mega_search_input_js', function() {
        $(this).parents('.mega_search').addClass('active');
        $(this).parents('.mega_search').find('.mega_search_input_js').val('');
        $(this).parents('.mega_search').find('.mega_search_result').removeClass('active');
    });




}







function txt_show_effect(selector) {
    $('body .' + selector + '_container').hover(function() {
        var text_h = $(this).find('.' + selector + '_txt').outerHeight();
        $(this).find('.' + selector + '_hiding_txt').css({
            "height": text_h
        });
    }, function() {
        $(this).find('.' + selector + '_hiding_txt').css({
            "height": "0px"
        });
    });

}





function radio_button_check() {
    $('body').on('change', 'input[type="radio"]', function() {
        var this_name = $(this).attr('name');
        $('input[name="' + this_name + '"]').parent().siblings().removeClass('radio_active');
        $('input[name="' + this_name + '"]:checked').parent().siblings().addClass('radio_active');
    });
}



function addTocart() {
    $('body').on('click', '.my_cart_popup_trigger', function() {

        var scroll_pos = $(window).scrollTop();
        if (scroll_pos != 0) {
            $("html, body").animate({
                scrollTop: (scroll_pos - 10)
            }, 300);
        }
        $('.mycart_popup_wrap').addClass('active');

        setTimeout(function() {
            $('.mycart_popup_wrap').removeClass('active');
        }, 3000);

    });

}


function floated_focus() {
    $('form .gd_form_group_floating:not(.gd_no_focus) input:first').trigger('focus');

    $('body').on('click', '[data-trigger]', function() {
        var this_attr = $(this).attr('data-trigger');
        $('body').find('[data-connection="' + this_attr + '"] .gd_form_group_floating input:first').trigger('focus');
    });
}


function loading_popup(number) {
    var url = window.location.pathname;
    var p_name = url.substring(url.lastIndexOf('/') + 1);


    if (p_name == 'index_1.php') {
        if ($('body [data-connection="popup_' + number + '"]')) {
            setTimeout(function() {
                $('body').find('[data-connection="popup_' + number + '"]').addClass('active');
                overlay_active();
                overlay_clicks($('[data-connection="popup_' + number + '"]'));
            }, 2000);
        }
    }
}





function trackorder() {
    var window_w = $(window).width();

    $('body').on('mouseover', '.track_progress_bar .round_block', function() {
        $(this).parent().siblings().removeClass('active');
        $(this).parent().addClass('active');

        var attr_selector = $(this).attr('data-progress');
        $('.track_order_wrpr').find('[data-progress-connection]').removeClass('active');
        $('.track_order_wrpr').find('[data-progress-connection="' + attr_selector + '"]').addClass("active");
    });


    if (window_w < 1024) {
        $('.track_order_head [data-progress-head]').each(function() {
            var this_attr = $(this).attr('data-progress-head');
            $('.track_progress_bar [data-progress="' + this_attr + '"]').siblings('.resp_details').append($(this));
        });

        $('.track_detail [data-progress-connection]').each(function() {
            var this_attr = $(this).attr('data-progress-connection');
            $('.track_progress_bar [data-progress="' + this_attr + '"]').siblings('.resp_details').append($(this));
        });


        $('body').on('click', '.track_progress_bar,.track_more', function() {
            $('.track_progress_bar').addClass('active');
            $('.track_progress_control').addClass('active');
        }).on('click', '.track_back', function() {
            $('.track_progress_bar').removeClass('active');
            $('.track_progress_control').removeClass('active');
        });

    }
}





function compare_page() {
    var compare_count = $('.compare_wrapper:first .compare_item').length;
    var window_wdth = $(window).width();

    if (compare_count == 2) {
        $('.compare_wrapper').each(function() {
            $(this).find('.compare_item').removeClass('gd_wrpr').addClass('gd_wrpr_6');
        });
        $('.compare_click').addClass('active');
    } else if (compare_count == 3) {
        $('.compare_wrapper').each(function() {
            $(this).find('.compare_item').removeClass('gd_wrpr').addClass('gd_wrpr_4');
        });
        $('.compare_click').removeClass('active');
    }




    var this_item_height = parseInt($('.compare_head_wraper').outerHeight());
    var this_item_height_full = parseInt($('.compare_head_wraper').offset().top + this_item_height);
    var top_size = $('header .header_fixing_wrpr').outerHeight();
    var total_h = $('.compare_all_wrpr').outerHeight() + this_item_height;


    $(window).scroll(function() {
        if (window_wdth > 1024) {
            scroll_fixing_elements($(".compare_head_wraper"), this_item_height, this_item_height_full, 'top', total_h);
        } else {
            scroll_fixing_elements($(".compare_head_wraper"), this_item_height, this_item_height_full, 'top', total_h, top_size);
        }
    })

}






function checkout() {
    $('.checked_input').each(function() {
        if ($(this).prop("checked")) {
            $(this).parents('.checkout_full_cover_wrapr').addClass('active');
        }
    });


    $('body').on('click', '.checkout_full_cover_wrapr:not(.edit) .checked_input', function() {
        $('.checkout_full_cover_wrapr').removeClass('active edit');
        $(this).parents('.checkout_full_cover_wrapr').addClass('active');
        // from widgtet_other.js
        collapse();
    }).on('click', '.checkout_edit', function() {
        $('.checkout_full_cover_wrapr').removeClass('edit');
        $(this).parents('.checkout_full_cover_wrapr').addClass('edit');
        // from widgtet_other.js
        collapse();
    }).on('click', '.form_cancel_btn', function() {
        $('.checkout_full_cover_wrapr').removeClass('edit');
        // from widgtet_other.js
        collapse();
    }).on('click', '.add_new_address', function() {
        $('.checkout_full_cover_wrapr').removeClass('active edit');
        $(this).parents('.checkout_full_cover_wrapr').addClass('active edit');
        // from widgtet_other.js
        collapse();
    });

}




function my_account_edit() {
    $('body').on('click', '.my_ac_edit_btn', function() {
        $('.checkout_full_cover_wrapr').removeClass('active edit');
        $(this).parents('.checkout_full_cover_wrapr').addClass('edit');
        // from widgtet_other.js
        collapse();
    }).on('click', '.set_default_wraper', function() {
        $(this).toggleClass('active');
        // from widgtet_other.js
        collapse();
    }).on('click', '.positioning_wraper', function(e) {
        e.stopPropagation();
    }).on('click', '.password_hideshow ', function() {
        var type_finder = $(this).siblings('input').attr('type');

        if (type_finder === 'text') {
            $(this).siblings('input').attr('type', 'password');
        } else {
            $(this).siblings('input').attr('type', 'text');
        }
    });
}



function bottom_fixings(indentifier) {

    var fix_item_height = $('.' + indentifier + '').outerHeight();
    var head_h = $('header').outerHeight();
    var fix_item_top = parseInt($('.' + indentifier + '').offset().top);


    $(window).scroll(function() {
        var scroll_amt = $(window).scrollTop();

        if (scroll_amt >= head_h) {
            $('.' + indentifier + '').addClass('active');

            // if (scroll_amt >= fix_item_top) {
            //     $('.'+indentifier+'').removeClass('active');
            // }

        } else {
            $('.' + indentifier + '').removeClass('active');
        }
    });
}




function star_ratings_old() {

    $('.rating_wrpr').each(function(e) {
        var selected_rating = $(this).find('.rating_input').attr('value');
        $(this).find('.rating_txt').html(selected_rating);
        var float_val = selected_rating.split(".");


        if (float_val.length > 1) {
            $(this).find('.rating_star .rating_star_block').each(function(e) {
                if (e < selected_rating - 1) {
                    $(this).addClass('clicked hover');
                }
                if (e < selected_rating) {
                    $(this).addClass('clicked hover_half')
                }
            });
        } else {
            $(this).find('.rating_star .rating_star_block').each(function(e) {
                if (e < selected_rating) {
                    $(this).addClass('clicked hover_half hover');
                }
            });
        }
    });



    var position = 0;

    $('body').on('mousemove', '.rating_wrpr .rating_star .rating_star_block', function(e) {

        var mouseSide;
        var element_left = parseInt((e.pageX - this.offsetLeft) - $(this).parents('.rating_star').offset().left);
        var this_width = parseInt($(this).outerWidth() + $(this).offset().left);
        var onStar = parseInt($(this).data('value'));

        $(this).parent().children('.rating_star_block').each(function(e) {
            if (e < onStar) {
                if (element_left < parseInt(($(this).outerWidth() - 5) / 2)) {
                    $(this).prev().addClass('hover');
                    $(this).addClass('hover_half');
                } else {
                    $(this).addClass('hover');
                }
            }
        });


        if (e.pageX < position) {
            var this_selector = $(this);
            $(this).parent().children('.rating_star_block').each(function(e) {
                if (e < onStar) {
                    if (element_left < parseInt(($(this).width()) / 2)) {
                        this_selector.removeClass('hover');
                        this_selector.nextAll().removeClass('hover_half hover');
                    }
                    if (element_left <= 0) {
                        this_selector.removeClass('hover_half');
                        this_selector.nextAll().removeClass('hover_half hover');
                    }
                }
            });
        }


        position = e.pageX;



    }).on('click', '.rating_wrpr .rating_star .rating_star_block', function(e) {
        var onStar = parseInt($(this).data('value'));
        var element_left = parseInt((e.pageX - this.offsetLeft) - $(this).parents('.rating_star').offset().left);

        if (element_left < $(this).width() / 2) {
            onStar = (onStar - 1) + .5;
        } else {
            onStar = onStar;
        }

        $(this).parent().children('.rating_star_block').each(function(e) {
            if (e < onStar) {
                $(this).addClass('clicked');
            } else {
                $(this).removeClass('clicked hover_half hover');
            }
        });
        $(this).parents('.rating_wrpr').find('.rating_txt').html(onStar);
        $(this).parents('.rating_wrpr').find('.rating_input').attr('value', onStar);

    }).on('mouseleave', '.rating_wrpr', function() {

        var selected_rating = $(this).find('.rating_input').attr('value');
        var float_val = selected_rating.split(".");

        $(this).find('.rating_star .rating_star_block').each(function(e) {
            $(this).removeClass('clicked hover_half hover');
        })

        if (float_val.length > 1) {
            $(this).find('.rating_star .rating_star_block').each(function(e) {
                if (e < selected_rating - 1) {
                    $(this).addClass('clicked hover');
                }
                if (e < selected_rating) {
                    $(this).addClass('clicked hover_half')
                }
            });
        } else {
            $(this).find('.rating_star .rating_star_block').each(function(e) {
                if (e < selected_rating) {
                    $(this).addClass('clicked hover_half hover');
                }
            });
        }
    });
}

function star_ratings() {

    var conter = 0;
    $('.gd_rating').each(function() {
        $(this).addClass('rating_' + conter);

        $('.rating_' + conter).rating({
            showCaption: false,
            size: 'sm',
            filledStar: '<i class="gd_icon_star_f"></i>',
            emptyStar: '<i class="gd_icon_star"></i>',
            containerClass: 'primary_clr',
            showClear: false,
        });
    });

    $('.gd_rating_xs').each(function() {
        $(this).addClass('rating_small_' + conter);

        $('.rating_small_' + conter).rating({
            showCaption: false,
            size: 'xs',
            filledStar: '<i class="gd_icon_star_f"></i>',
            emptyStar: '<i class="gd_icon_star"></i>',
            containerClass: 'primary_clr',
            showClear: false
        });
    });


    $('.gd_rating_1').rating({
        showCaption: false,
        size: 'xs',
        filledStar: '<i class="gd_icon_star_f"></i>',
        emptyStar: '<i class="gd_icon_star"></i>',
        containerClass: 'gd_txt_clr_success',
        showClear: false
    });


    $('.gd_rating_2').each(function() {
        $(this).rating({
            showCaption: false,
            size: 'xs',
            filledStar: '<i class="gd_icon_box_f"></i>',
            emptyStar: '<i class="gd_icon_box_t"></i>',
            containerClass: 'secondary_clr gd_txt_size_10',
            showClear: false
        });
    });
}


function header_height_to_margin(repeat_caller) {

    var element_h = parseInt($('.header_fixing').outerHeight());


    // if (repeat_caller) {
    //     $('.margin_top_setter').removeAttr('style');
    //     $('.header_fixing').addClass('active');
    //     $('.header_fixing').next().next().css('margin-top', element_h + 'px').addClass('margin_top_setter');
    // } else {
    //     $('.header_fixing').addClass('active');
    //     $('.header_fixing').next().next().css('margin-top', element_h + 'px').addClass('margin_top_setter');
    // }

    if (repeat_caller) {
        $('.margin_top_setter').removeAttr('style');
        $('.header_fixing').addClass('active');
        $('.header_fixing').next().css('margin-top', element_h + 'px').addClass('margin_top_setter');
    } else {
        $('.header_fixing').addClass('active');
        $('.header_fixing').next().css('margin-top', element_h + 'px').addClass('margin_top_setter');
    }
}






// ajin
$(document).ready(function() {

    /* // Get A Quote Featue Checkbox JS
     $('body').on('click', '.click_to_check_label', function() {
         $(this).parents('.hover_overlay_effect1').toggleClass('checked');
         if ($(this).parents('.hover_overlay_effect1').hasClass('checked')) {
             $(this).parents('.hover_overlay_effect1.checked').find('.gd_radio').attr('checked', 'checked');
             // console.log('has');
         } else {
             $(this).parents('.hover_overlay_effect1').find('.gd_radio').removeAttr('checked');
             // console.log('not');
         }
     });

     // Get A Quote Featue Radio JS
     $('body').on('click', '.click_to_radio_label', function() {
         $('.hover_overlay_effect2').removeClass('checked').find('.gd_radio').removeAttr('checked');
         $(this).parents('.hover_overlay_effect2').addClass('checked').find('.gd_radio').attr('checked', 'checked');
     })*/

    // Get A Quote Featue Radio JS 2
    $('body').on('click', '.click_to_radio_label2', function() {
        $('.hover_overlay_effect2').removeClass('checked').find('.gd_radio').removeAttr('checked');
        $(this).parents('.hover_overlay_effect2').addClass('checked').find('.gd_radio').attr('checked', 'checked');
    });





    // my ac section
    // martin
    // $('body').on('click', '#myButton', function() {
    //     $(this).parent().siblings().find('.myInput').removeAttr('readonly');
    //     $(this).parent().siblings().find('.myInput').filter(':visible:first').focus();
    //     $(this).hide();
    //     $(this).parent().find('#update_btn, #cancel_btn').show();
    //     $(this).parent().prev().find('.acct_form').addClass("active");

    //     $(this).parent().next().find('.check_pointer_wrap').addClass("active");

    // });

    // $('body').on('click', '#cancel_btn', function() {
    //     $(this).parent().siblings().find('.myInput').attr('readonly', true);
    //     $(this).parent().find('#update_btn, #cancel_btn').hide();
    //     $(this).parent().find('#myButton').show();
    //     $(this).parent().prev().find('.acct_form').removeClass("active");

    //     $(this).parent().next().find('.check_pointer_wrap').removeClass("active");
    // });

    // ajin

    $('body').on('click', '.check_delivery_btn', function() {
        $(this).toggleClass('checked');
        $(this).parent('.check_delivery_warp').toggleClass('active');
    });



    var sync1 = $('#step_text').owlCarousel();
    var sync2 = $('#step_img').owlCarousel();
    sync2.on('click', '.owl-next', function() {
        sync1.trigger('next.owl.carousel')
    });
    sync2.on('click', '.owl-prev', function() {
        sync1.trigger('prev.owl.carousel')
    });
    sync2.on('dragged.owl.carousel', function(event) {
        if (event.relatedTarget['_drag']['direction'] == 'left') {
            sync1.trigger('next.owl.carousel')
        } else {
            sync1.trigger('prev.owl.carousel')
        }
    });
    sync1.on('dragged.owl.carousel', function(event) {
        if (event.relatedTarget['_drag']['direction'] == 'left') {
            sync2.trigger('next.owl.carousel')
        } else {
            sync2.trigger('prev.owl.carousel')
        }
    });




    // Notification
    var win_w = $(window).width();
    if (win_w >= 1025) {
        $('body').on('click', '.notification_click', function(e) {
            e.preventDefault();
            e.stopPropagation();
            $(this).parent().find('.notification_poup').toggleClass('active');
        }).on('click', function() {
            $(this).parent().find('.notification_poup').removeClass('active');
        });
    }


    // Image Upload 
    $('body').on('change', '.upload_img_btn', function(e) {
        var fileName = e.target.files[0].name;
        $(this).parent().find('.upload_img_filename').text(fileName).addClass('active');

        // if (fileName != 0) {
        // } else {
        //     $(this).parent().find('.upload_img_filename').removeClass('active');
        // }

    });


    // Video PopUP
    var video_link;
    $('body').on('click', '.video_popup_trigger', function() {
        var this_elem = $(this);

        if ($(this).parents().hasClass('video_slider_wrpr')) {
            video_link = this_elem.parents('.video_slider_wrpr').find('.active img').attr('data-video-link');
        } else {
            video_link = this_elem.attr('data-video-link');
        }

        if ($('.video_popup_wrap').hasClass('active')) {
            $('.video_popup_iframe').attr('src', video_link);
        }

    });


    $('body').on('click', '.video_popup_wrap .gd_popup_closing , .gd_body_overlay', function() {
        $('.video_popup_iframe').attr('src', '');
    });





    // right fixing section clicked
    $('body').on('click', '.right_block_fixing', function() {
        $(this).parents('.fixing_block_custom').toggleClass('active')
    });


});